object FormCampoNMA: TFormCampoNMA
  Left = 0
  Top = 0
  Caption = 'Campo'
  ClientHeight = 155
  ClientWidth = 243
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lblNombre: TLabel
    Left = 24
    Top = 32
    Width = 42
    Height = 13
    Caption = 'NOMBRE'
  end
  object txtNombre: TEdit
    Left = 24
    Top = 64
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'Nombre'
  end
  object btnGuardar: TButton
    Left = 160
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Guardar'
    TabOrder = 1
    OnClick = btnGuardarClick
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    LoginPrompt = False
    Left = 207
    Top = 7
  end
end
