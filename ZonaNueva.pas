unit ZonaNueva;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  Vcl.ComCtrls, Vcl.StdCtrls;

type
  TUZonaNueva = class(TForm)
    StatusBar1: TStatusBar;
    Conexion: TFDConnection;
    Label1: TLabel;
    btnGuardar: TButton;
    txtZona: TEdit;
    procedure btnGuardarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UZonaNueva: TUZonaNueva;

implementation

{$R *.dfm}

procedure TUZonaNueva.btnGuardarClick(Sender: TObject);
begin
 try
   Conexion.ExecSQL('insert into zonas_clientes(zona_cliente_id,nombre,es_predet,oculto) values(-1,:n,''N'',''N'')',[txtZona.Text]);
   ShowMessage('Se guardo la zona del cliente correctamente.');
   ModalResult:=mrOk;
  // Self.Close;
 except
  ShowMessage('ERROR al guardar la zona nueva.');
  ModalResult:=mrCancel;
 end;
end;

end.
