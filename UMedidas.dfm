object Medidas: TMedidas
  Left = 0
  Top = 0
  Caption = 'Medida Nueva'
  ClientHeight = 129
  ClientWidth = 306
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lblNombre: TLabel
    Left = 24
    Top = 16
    Width = 41
    Height = 13
    Caption = 'Nombre:'
  end
  object Label1: TLabel
    Left = 168
    Top = 16
    Width = 50
    Height = 13
    Caption = 'Clave Sat:'
  end
  object txtNombre: TEdit
    Left = 24
    Top = 48
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object btnGuardar: TButton
    Left = 112
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Guardar'
    TabOrder = 1
    OnClick = btnGuardarClick
  end
  object txtSat: TEdit
    Left = 168
    Top = 48
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    LoginPrompt = False
    Left = 23
    Top = 79
  end
end
