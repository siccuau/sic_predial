unit UNuevoMunicipio;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, Data.Bind.EngExt, Vcl.Bind.DBEngExt,
  System.Rtti, System.Bindings.Outputs, Vcl.Bind.Editors, Data.Bind.Components,
  Data.DB, Data.Bind.DBScope, Vcl.StdCtrls, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFormNuevoMunicipio = class(TForm)
    lblNombre: TLabel;
    txtNombre: TEdit;
    btnGuardar: TButton;
    Conexion: TFDConnection;
    qryEstado: TFDQuery;
    cboEstado: TComboBox;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    lblEstado: TLabel;
    procedure btnGuardarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormNuevoMunicipio: TFormNuevoMunicipio;

implementation

{$R *.dfm}

procedure TFormNuevoMunicipio.btnGuardarClick(Sender: TObject);
begin
if txtNombre.Text = '' then  ShowMessage('Falta Nombre')
else
begin
try
Conexion.ExecSQL('INSERT INTO CIUDADES(CIUDAD_ID,NOMBRE,ESTADO_ID) VALUES(-1,'''+txtNombre.Text+''','+qryEstado.FieldByName('ESTADO_ID').AsString+')') ;
ShowMessage('Municipio agregado');
Close;
Except
ShowMessage('Hubo un error al agregar el municipio');
end;
end;
end;

procedure TFormNuevoMunicipio.FormShow(Sender: TObject);
begin
qryEstado.Open();
end;

end.
