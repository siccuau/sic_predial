﻿object FormCargoLotes: TFormCargoLotes
  Left = 0
  Top = 0
  Caption = 'Cargo Lotes'
  ClientHeight = 495
  ClientWidth = 1198
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblCliente: TLabel
    Left = 8
    Top = 8
    Width = 41
    Height = 13
    Caption = 'CLIENTE'
  end
  object lblAño: TLabel
    Left = 336
    Top = 8
    Width = 22
    Height = 13
    Caption = 'A'#209'O'
  end
  object lblConcepto: TLabel
    Left = 487
    Top = 8
    Width = 55
    Height = 13
    Caption = 'CONCEPTO'
  end
  object lblTipo: TLabel
    Left = 8
    Top = 56
    Width = 74
    Height = 13
    Caption = 'TIPO TERRENO'
  end
  object cboCliente: TComboBox
    Left = 8
    Top = 27
    Width = 305
    Height = 21
    TabOrder = 0
    OnChange = cboClienteChange
  end
  object txtAño: TEdit
    Left = 336
    Top = 27
    Width = 121
    Height = 21
    MaxLength = 4
    NumbersOnly = True
    TabOrder = 1
    Visible = False
    OnChange = txtAñoChange
  end
  object cboConcepto: TComboBox
    Left = 487
    Top = 27
    Width = 145
    Height = 21
    PopupMenu = popmnuConcepto
    TabOrder = 2
  end
  object sgTerreno: TStringGrid
    Left = 8
    Top = 102
    Width = 1177
    Height = 529
    ColCount = 8
    FixedCols = 0
    RowCount = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnSelectCell = sgTerrenoSelectCell
  end
  object btnCargos: TButton
    Left = 712
    Top = 25
    Width = 75
    Height = 25
    Caption = 'Crear Cargos'
    TabOrder = 4
    OnClick = btnCargosClick
  end
  object cboTipoTerreno: TComboBox
    Left = 8
    Top = 75
    Width = 305
    Height = 21
    TabOrder = 5
    Text = 'Lote'
    OnChange = cboTipoTerrenoChange
    Items.Strings = (
      'Lote'
      'Agricola de Temporal'
      'Agricola de Riego'
      'Casa'
      'Corredor')
  end
  object btnCalcular: TButton
    Left = 638
    Top = 25
    Width = 75
    Height = 25
    Caption = 'Calcular'
    TabOrder = 6
    Visible = False
    WordWrap = True
    OnClick = btnCalcularClick
  end
  object dtpYear: TDateTimePicker
    Left = 336
    Top = 27
    Width = 121
    Height = 21
    Date = 44097.000000000000000000
    Format = 'yyyy'
    Time = 0.573060266200627700
    DateMode = dmUpDown
    TabOrder = 7
    OnChange = dtpYearChange
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 476
    Width = 1198
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object qryCliente: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from clientes')
    Left = 1155
    Top = 312
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    LoginPrompt = False
    Left = 1163
    Top = 62
  end
  object qryConceptos: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select * from conceptos_cc where naturaleza='#39'C'#39' order by fecha_h' +
        'ora_creacion desc')
    Left = 1162
    Top = 376
  end
  object qryTerrenos: TFDQuery
    Connection = Conexion
    Left = 1160
    Top = 8
  end
  object BindSourceDB1: TBindSourceDB
    DataSet = qryCliente
    ScopeMappings = <>
    Left = 1152
    Top = 248
  end
  object BindingsList1: TBindingsList
    Methods = <>
    OutputConverters = <>
    Left = 1164
    Top = 117
    object LinkListControlToField1: TLinkListControlToField
      Category = 'Quick Bindings'
      DataSource = BindSourceDB1
      FieldName = 'NOMBRE'
      Control = cboCliente
      FillExpressions = <>
      FillHeaderExpressions = <>
      FillBreakGroups = <>
    end
    object LinkListControlToField2: TLinkListControlToField
      Category = 'Quick Bindings'
      DataSource = BindSourceDB2
      FieldName = 'NOMBRE'
      Control = cboConcepto
      FillExpressions = <>
      FillHeaderExpressions = <>
      FillBreakGroups = <>
    end
  end
  object BindSourceDB2: TBindSourceDB
    DataSet = qryConceptos
    ScopeMappings = <>
    Left = 1152
    Top = 176
  end
  object popmnuConcepto: TPopupMenu
    Left = 1168
    Top = 440
    object Nuevo1: TMenuItem
      Caption = 'Nuevo'
      OnClick = Nuevo1Click
    end
  end
  object qryValores: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from SIC_PREDIAL_VALORES')
    Left = 824
    Top = 16
  end
end
