object Principal: TPrincipal
  Left = 0
  Top = 0
  Caption = 'Terrenos'
  ClientHeight = 742
  ClientWidth = 1573
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 71
    Height = 25
    Caption = 'Cliente:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 16
    Top = 56
    Width = 152
    Height = 25
    Caption = 'Tipo de terreno:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 568
    Top = 16
    Width = 180
    Height = 25
    Caption = 'Fecha de Creacion:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 534
    Top = 51
    Width = 214
    Height = 25
    Caption = 'Fecha de Modificacion:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 705
    Top = 677
    Width = 43
    Height = 18
    Caption = 'Total:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 871
    Top = 677
    Width = 21
    Height = 18
    Caption = 'M'#178
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 1025
    Top = 677
    Width = 42
    Height = 18
    Caption = 'Acres'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object cbTipo: TComboBox
    Left = 16
    Top = 91
    Width = 193
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Text = 'Lote'
    OnChange = cbTipoChange
    Items.Strings = (
      'Lote'
      'Agricola de Temporal'
      'Agricola de Riego'
      'Casa'
      'Corredor')
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 723
    Width = 1573
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object sgTerreno: TStringGrid
    Left = 8
    Top = 136
    Width = 1557
    Height = 529
    ColCount = 11
    FixedCols = 0
    RowCount = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    PopupMenu = PopupMenu2
    TabOrder = 2
    OnDrawCell = sgTerrenoDrawCell
    OnKeyDown = sgTerrenoKeyDown
    OnSelectCell = sgTerrenoSelectCell
  end
  object btnGuardar: TButton
    Left = 504
    Top = 671
    Width = 97
    Height = 34
    Caption = 'Guardar'
    TabOrder = 3
    OnClick = btnGuardarClick
  end
  object cbCliente: TDBLookupComboBox
    Left = 93
    Top = 16
    Width = 420
    Height = 27
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyField = 'CLIENTE_ID'
    ListField = 'NOMBRE'
    ListSource = dsClientes
    ParentFont = False
    TabOrder = 4
    OnCloseUp = cbClienteCloseUp
  end
  object txtFechaCreacion: TEdit
    Left = 754
    Top = 18
    Width = 231
    Height = 27
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Text = 'Edit1'
  end
  object txtFechaModificacion: TEdit
    Left = 754
    Top = 53
    Width = 231
    Height = 27
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    Text = 'Edit1'
  end
  object txtAcres: TEdit
    Left = 938
    Top = 671
    Width = 81
    Height = 27
    Alignment = taRightJustify
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
  end
  object txtMetros: TEdit
    Left = 762
    Top = 671
    Width = 103
    Height = 27
    Alignment = taRightJustify
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
  end
  object btnImpuestos: TButton
    Left = 534
    Top = 84
    Width = 122
    Height = 34
    Caption = 'Otros Impuestos'
    TabOrder = 9
    OnClick = btnImpuestosClick
  end
  object cbCampo: TDBLookupComboBox
    Left = 232
    Top = 49
    Width = 145
    Height = 21
    KeyField = 'ID_CAMPOS'
    ListField = 'NOMBRE'
    ListFieldIndex = 1
    ListSource = dsCampos
    TabOrder = 10
    Visible = False
    OnCloseUp = cbCampoCloseUp
    OnKeyDown = cbCampoKeyDown
  end
  object cbMunicipio: TComboBox
    Left = 352
    Top = 90
    Width = 145
    Height = 21
    TabOrder = 11
    Text = 'Cuauht'#233'moc'
    Visible = False
    OnCloseUp = cbMunicipioCloseUp
    OnKeyDown = cbMunicipioKeyDown
    Items.Strings = (
      'Cuauht'#233'moc'
      'Cusihuiriachi')
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'Protocol=TCPIP'
      'Server=localhost'
      'CharacterSet=ISO8859_1'
      'RoleName=USUARIO_MICROSIP'
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=C:\Microsip datos\ABRAHAM THIESSEN.FDB'
      'DriverID=FB')
    UpdateOptions.AssignedValues = [uvAutoCommitUpdates]
    UpdateOptions.AutoCommitUpdates = True
    LoginPrompt = False
    Left = 485
    Top = 208
  end
  object qryClientes: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from clientes')
    Left = 464
    Top = 40
  end
  object dsClientes: TDataSource
    DataSet = qryClientes
    Left = 392
    Top = 72
  end
  object qryTerrenos: TFDQuery
    Connection = Conexion
    Left = 328
    Top = 80
  end
  object PopupMenu1: TPopupMenu
    Left = 568
    Top = 80
    object NuevaZona1: TMenuItem
      Caption = 'Nueva Zona'
      OnClick = NuevaZona1Click
    end
  end
  object qryCampos: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from sic_predial_campos')
    Left = 592
    Top = 32
  end
  object BindSourceDB1: TBindSourceDB
    DataSet = qryCampos
    ScopeMappings = <>
    Left = 360
    Top = 280
  end
  object BindingsList1: TBindingsList
    Methods = <>
    OutputConverters = <>
    Left = 20
    Top = 5
  end
  object rpConsolidado: TfrxReport
    Version = '6.7.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44085.402654224500000000
    ReportOptions.LastChange = 44124.382153472220000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 136
    Top = 464
    Datasets = <
      item
        DataSet = dssConsolidado
        DataSetName = 'DatasetConsolidado'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 355.600000000000000000
      PaperHeight = 215.900000000000000000
      PaperSize = 5
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object Shape24: TfrxShapeView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 64.252010000000000000
        Width = 944.882272990000000000
        Height = 18.758696690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Shape2: TfrxShapeView
        AllowVectorExport = True
        Left = 90.693230120000000000
        Top = 26.373338010000000000
        Width = 1080.945352990000000000
        Height = 18.758696690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Shape5: TfrxShapeView
        AllowVectorExport = True
        Left = 90.693230120000000000
        Top = 45.132034710000000000
        Width = 136.062852990000000000
        Height = 18.758696690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Shape6: TfrxShapeView
        AllowVectorExport = True
        Left = 90.693230120000000000
        Top = 63.890731400000000000
        Width = 136.062852990000000000
        Height = 18.758696690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Shape9: TfrxShapeView
        AllowVectorExport = True
        Left = 226.862933400000000000
        Top = 63.890731400000000000
        Width = 612.283632990000000000
        Height = 18.758696690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo1: TfrxMemoView
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 26.456710000000000000
        Width = 1080.945352990000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Manitoba Kolonie Cd. Cuauht'#233'moc, Chihuahua. Mx.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape13: TfrxShapeView
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 124.724490000000000000
        Width = 1080.945352990000000000
        Height = 427.086614170000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo42: TfrxMemoView
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 47.354360000000000000
        Width = 136.063048270000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Gebortsdatum')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo49: TfrxMemoView
        AllowVectorExport = True
        Left = 905.307670000000000000
        Top = 66.252010000000000000
        Width = 45.354328270000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          'Tel.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape11: TfrxShapeView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 45.354360000000000000
        Width = 944.882272990000000000
        Height = 18.758696690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo46: TfrxMemoView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 47.354360000000000000
        Width = 612.283828270000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Name von den Eigent'#252'mer')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape7: TfrxShapeView
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 83.149660000000000000
        Width = 136.062852990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo2: TfrxMemoView
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 98.267780000000000000
        Width = 136.063048270000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Datum')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape12: TfrxShapeView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 83.149660000000000000
        Width = 56.692722990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo3: TfrxMemoView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 98.267780000000000000
        Width = 56.692918270000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Campo')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape14: TfrxShapeView
        AllowVectorExport = True
        Left = 283.464750000000000000
        Top = 83.149660000000000000
        Width = 173.858152990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo4: TfrxMemoView
        AllowVectorExport = True
        Left = 283.464750000000000000
        Top = 83.149660000000000000
        Width = 173.858348270000000000
        Height = 41.574830000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Calle/ Av./ Fracc./'
          'Residencial/ Corredor C.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape15: TfrxShapeView
        AllowVectorExport = True
        Left = 457.323130000000000000
        Top = 83.149660000000000000
        Width = 90.708520890000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo6: TfrxMemoView
        AllowVectorExport = True
        Left = 457.323130000000000000
        Top = 98.267780000000000000
        Width = 90.708688270000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Municipio')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape16: TfrxShapeView
        AllowVectorExport = True
        Left = 548.031850000000000000
        Top = 83.149660000000000000
        Width = 86.928962990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo7: TfrxMemoView
        AllowVectorExport = True
        Left = 548.031850000000000000
        Top = 83.149660000000000000
        Width = 86.929158270000000000
        Height = 41.574830000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Lote/ Temporal/'
          'Riego')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape17: TfrxShapeView
        AllowVectorExport = True
        Left = 634.961040000000000000
        Top = 83.149660000000000000
        Width = 143.621912990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo8: TfrxMemoView
        AllowVectorExport = True
        Left = 634.961040000000000000
        Top = 83.149660000000000000
        Width = 143.622108270000000000
        Height = 41.574830000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Folio No.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape18: TfrxShapeView
        AllowVectorExport = True
        Left = 778.583180000000000000
        Top = 83.149660000000000000
        Width = 60.472252990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo9: TfrxMemoView
        AllowVectorExport = True
        Left = 778.583180000000000000
        Top = 83.149660000000000000
        Width = 60.472448270000000000
        Height = 41.574830000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Zona')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape19: TfrxShapeView
        AllowVectorExport = True
        Left = 839.055660000000000000
        Top = 83.149660000000000000
        Width = 113.385672990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Shape20: TfrxShapeView
        AllowVectorExport = True
        Left = 952.441560000000000000
        Top = 83.149660000000000000
        Width = 98.267552990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Shape21: TfrxShapeView
        AllowVectorExport = True
        Left = 1050.709340000000000000
        Top = 83.149660000000000000
        Width = 56.692722990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Shape22: TfrxShapeView
        AllowVectorExport = True
        Left = 1107.402290000000000000
        Top = 83.149660000000000000
        Width = 64.251782990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo10: TfrxMemoView
        AllowVectorExport = True
        Left = 839.055660000000000000
        Top = 83.149660000000000000
        Width = 113.385868270000000000
        Height = 41.574830000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Este Mts.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo11: TfrxMemoView
        AllowVectorExport = True
        Left = 952.441560000000000000
        Top = 83.149660000000000000
        Width = 98.267748270000000000
        Height = 41.574830000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Norte Mts.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo12: TfrxMemoView
        AllowVectorExport = True
        Left = 1050.709340000000000000
        Top = 83.149660000000000000
        Width = 56.692918270000000000
        Height = 41.574830000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Hoff/Lot'
          'Nr.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo13: TfrxMemoView
        AllowVectorExport = True
        Left = 1107.402290000000000000
        Top = 83.149660000000000000
        Width = 64.251978270000000000
        Height = 41.574830000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Acres')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape23: TfrxShapeView
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 566.929500000000000000
        Width = 1080.945352990000000000
        Height = 151.180924170000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Subreport1: TfrxSubreport
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 124.724490000000000000
        Width = 1080.945580000000000000
        Height = 427.086890000000000000
        Page = rpConsolidado.Page2
      end
      object Memo16: TfrxMemoView
        AllowVectorExport = True
        Left = 98.267780000000000000
        Top = 578.268090000000000000
        Width = 56.692918270000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'NOTA:')
        ParentFont = False
        VAlign = vaCenter
      end
      object DatasetConsolidadoNOMBRE: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 230.551330000000000000
        Top = 64.252010000000000000
        Width = 604.724800000000000000
        Height = 18.897650000000000000
        DataField = 'NOMBRE'
        DataSet = dssConsolidado
        DataSetName = 'DatasetConsolidado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[DatasetConsolidado."NOMBRE"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object DatasetConsolidadoTELEFONO1: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 952.441560000000000000
        Top = 64.252010000000000000
        Width = 219.212740000000000000
        Height = 18.897650000000000000
        DataField = 'TELEFONO1'
        DataSet = dssConsolidado
        DataSetName = 'DatasetConsolidado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[DatasetConsolidado."TELEFONO1"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object DatasetConsolidadoRFC_CURP: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 64.252010000000000000
        Width = 136.063080000000000000
        Height = 18.897650000000000000
        DataField = 'RFC_CURP'
        DataSet = dssConsolidado
        DataSetName = 'DatasetConsolidado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[DatasetConsolidado."RFC_CURP"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object DatasetConsolidadoNOTAS: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 158.740260000000000000
        Top = 578.268090000000000000
        Width = 1001.575450000000000000
        Height = 128.504020000000000000
        DataField = 'NOTAS'
        DataSet = dssConsolidado
        DataSetName = 'DatasetConsolidado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[DatasetConsolidado."NOTAS"]')
        ParentFont = False
      end
    end
    object Page2: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 355.600000000000000000
      PaperHeight = 215.900000000000000000
      PaperSize = 5
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 18.897650000000000000
        Width = 1268.410268000000000000
        DataSet = dssConsolidado
        DataSetName = 'DatasetConsolidado'
        RowCount = 0
        object DatasetConsolidadoCAMPO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 136.063080000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataField = 'CAMPO'
          DataSet = dssConsolidado
          DataSetName = 'DatasetConsolidado'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[DatasetConsolidado."CAMPO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoDIRECCION: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 192.756030000000000000
          Width = 173.858267720000000000
          Height = 18.897650000000000000
          DataField = 'DIRECCION'
          DataSet = dssConsolidado
          DataSetName = 'DatasetConsolidado'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[DatasetConsolidado."DIRECCION"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoMUNICIPIO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 366.614410000000000000
          Width = 90.708661420000000000
          Height = 18.897650000000000000
          DataField = 'MUNICIPIO'
          DataSet = dssConsolidado
          DataSetName = 'DatasetConsolidado'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[DatasetConsolidado."MUNICIPIO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoTIPO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 457.323130000000000000
          Width = 86.929133860000000000
          Height = 18.897650000000000000
          DataField = 'TIPO'
          DataSet = dssConsolidado
          DataSetName = 'DatasetConsolidado'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[DatasetConsolidado."TIPO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoFOLIO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 544.252320000000000000
          Width = 143.622047240000000000
          Height = 18.897650000000000000
          DataField = 'FOLIO'
          DataSet = dssConsolidado
          DataSetName = 'DatasetConsolidado'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[DatasetConsolidado."FOLIO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoZONA: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 687.874460000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'ZONA'
          DataSet = dssConsolidado
          DataSetName = 'DatasetConsolidado'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[DatasetConsolidado."ZONA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoMTS_ESTE: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 748.346940000000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DataField = 'MTS_ESTE'
          DataSet = dssConsolidado
          DataSetName = 'DatasetConsolidado'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[DatasetConsolidado."MTS_ESTE"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoMTS_NORTE: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 861.732840000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DataField = 'MTS_NORTE'
          DataSet = dssConsolidado
          DataSetName = 'DatasetConsolidado'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[DatasetConsolidado."MTS_NORTE"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoNUMERO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 960.000620000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataField = 'NUMERO'
          DataSet = dssConsolidado
          DataSetName = 'DatasetConsolidado'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[DatasetConsolidado."NUMERO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoACRES: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 1016.693570000000000000
          Width = 64.251968500000000000
          Height = 18.897650000000000000
          DataField = 'ACRES'
          DataSet = dssConsolidado
          DataSetName = 'DatasetConsolidado'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[DatasetConsolidado."ACRES"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoFECHA: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          DataField = 'FECHA'
          DataSet = dssConsolidado
          DataSetName = 'DatasetConsolidado'
          DisplayFormat.FormatStr = 'dd mmm yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[DatasetConsolidado."FECHA"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object qryConsolidado: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select s.*,(select nombre from clientes where cliente_id=:client' +
        'e)'
      ',(select rfc_curp from dirs_clientes where cliente_id=:cliente)'
      ',(select telefono1 from dirs_clientes where cliente_id=:cliente)'
      ',(select notas from clientes where cliente_id=:cliente)'
      
        ',(select fecha_hora from sic_predial_bitacora where cliente_id=:' +
        'cliente and folio=s.folio and concepto='#39'TERRENO NUEVO'#39') as fecha' +
        ' from sic_predial_terrenos s where cliente_id=:cliente order by ' +
        'folio')
    Left = 264
    Top = 488
    ParamData = <
      item
        Name = 'CLIENTE'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object qryHistorial: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select sbp.*,(select nombre from clientes where cliente_id=:cli)' +
        ' from sic_predial_bitacora sbp'
      'where sbp.cliente_id=:cli order by Fecha_hora')
    Left = 816
    Top = 88
    ParamData = <
      item
        Name = 'CLI'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dssHistorial: TDataSource
    DataSet = qryHistorial
    Left = 872
    Top = 88
  end
  object rptHistorial: TfrxReport
    Version = '6.7.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44089.480742002300000000
    ReportOptions.LastChange = 44097.669668576390000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 928
    Top = 88
    Datasets = <
      item
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 279.400000000000000000
      PaperSize = 1
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 71.811070000000000000
        Top = 18.897650000000000000
        Width = 740.409927000000000000
        object frxDBDataset1NOMBRE: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Top = 26.456710000000000000
          Width = 737.008350000000000000
          Height = 18.897650000000000000
          DataField = 'NOMBRE'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDBDataset1."NOMBRE"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 52.913420000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Folio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 52.913420000000000000
          Width = 204.094620000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Concepto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 52.913420000000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor Anterior')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 52.913420000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor Nuevo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Top = 52.913420000000000000
          Width = 185.196970000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Fecha')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Width = 737.008350000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'HISTORIAL DE MOVIMIENTOS')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 151.181200000000000000
        Width = 740.409927000000000000
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
        RowCount = 0
        object frxDBDataset1FOLIO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DataField = 'FOLIO'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBDataset1."FOLIO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBDataset1VALOR_ANTERIOR: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 355.275820000000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_ANTERIOR'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBDataset1."VALOR_ANTERIOR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBDataset1VALOR_NUEVO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_NUEVO'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBDataset1."VALOR_NUEVO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBDataset1FECHA_HORA: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 551.811380000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          DataField = 'FECHA_HORA'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBDataset1."FECHA_HORA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBDataset1CONCEPTO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 204.094620000000000000
          Height = 18.897650000000000000
          DataField = 'CONCEPTO'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDBDataset1."CONCEPTO"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDBDataset1: TfrxDBDataset
    UserName = 'frxDBDataset1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'FOLIO=FOLIO'
      'CLIENTE_ID=CLIENTE_ID'
      'CONCEPTO=CONCEPTO'
      'VALOR_ANTERIOR=VALOR_ANTERIOR'
      'VALOR_NUEVO=VALOR_NUEVO'
      'METROS=METROS'
      'USUARIO=USUARIO'
      'FECHA_HORA=FECHA_HORA'
      'NOMBRE=NOMBRE')
    DataSource = dssHistorial
    BCDToCurrency = False
    Left = 712
    Top = 96
  end
  object PopupMenu2: TPopupMenu
    Left = 568
    Top = 152
    object Traspasar: TMenuItem
      Caption = 'Traspasar'
      OnClick = TraspasarClick
    end
    object CambiarFolio1: TMenuItem
      Caption = 'Cambiar Folio'
      OnClick = CambiarFolio1Click
    end
    object AsignarPropietarios1: TMenuItem
      Caption = 'Propietarios'
      OnClick = AsignarPropietarios1Click
    end
  end
  object rptHistorialTraslados: TfrxReport
    Version = '6.7.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44089.480742002300000000
    ReportOptions.LastChange = 44098.404619212960000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 928
    Top = 152
    Datasets = <
      item
        DataSet = frxDBDataset2
        DataSetName = 'Historial Traslados'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 279.400000000000000000
      PaperSize = 1
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 71.811070000000000000
        Top = 18.897650000000000000
        Width = 740.409927000000000000
        object frxDBDataset1NOMBRE: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Top = 19.118120000000000000
          Width = 737.008350000000000000
          Height = 18.897650000000000000
          DataField = 'NOMBRE'
          DataSet = frxDBDataset2
          DataSetName = 'Historial Traslados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Historial Traslados."NOMBRE"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354360000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Folio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047310000000000000
          Top = 45.354360000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Concepto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Top = 45.354360000000000000
          Width = 170.078740160000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'De')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409710000000000000
          Top = 45.354360000000000000
          Width = 170.078740160000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'A')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 646.299630000000000000
          Top = 45.354360000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Fecha')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 45.354360000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Top = 2.000000000000000000
          Width = 737.008350000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset2
          DataSetName = 'Historial Traslados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'TRASPASOS DE TERRENOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object SysMemo1: TfrxSysMemoView
          AllowVectorExport = True
          Top = 26.456710000000000000
          Width = 245.669450000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DATE]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 41.574830000000000000
        Top = 151.181200000000000000
        Width = 740.409927000000000000
        DataSet = frxDBDataset2
        DataSetName = 'Historial Traslados'
        RowCount = 0
        object frxDBDataset1FOLIO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Width = 102.047310000000000000
          Height = 26.456710000000000000
          DataField = 'FOLIO'
          DataSet = frxDBDataset2
          DataSetName = 'Historial Traslados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Historial Traslados."FOLIO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBDataset1FECHA_HORA: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 646.299630000000000000
          Width = 94.488250000000000000
          Height = 26.456710000000000000
          DataField = 'FECHA_HORA'
          DataSet = frxDBDataset2
          DataSetName = 'Historial Traslados'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Historial Traslados."FECHA_HORA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBDataset1CONCEPTO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 102.047310000000000000
          Width = 132.283550000000000000
          Height = 26.456710000000000000
          DataField = 'CONCEPTO'
          DataSet = frxDBDataset2
          DataSetName = 'Historial Traslados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Historial Traslados."CONCEPTO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object HistorialTrasladosCLIENTE1: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 234.330860000000000000
          Width = 170.078740160000000000
          Height = 26.456710000000000000
          DataField = 'CLIENTE1'
          DataSet = frxDBDataset2
          DataSetName = 'Historial Traslados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Historial Traslados."CLIENTE1"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object HistorialTrasladosCLIENTE2: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 404.409710000000000000
          Width = 170.078740160000000000
          Height = 26.456710000000000000
          DataField = 'CLIENTE2'
          DataSet = frxDBDataset2
          DataSetName = 'Historial Traslados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Historial Traslados."CLIENTE2"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 34.015770000000000000
          Width = 740.787880000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object HistorialTrasladosMETROS: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 71.811070000000000000
          Height = 26.456710000000000000
          DataField = 'METROS'
          DataSet = frxDBDataset2
          DataSetName = 'Historial Traslados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Historial Traslados."METROS"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object dssHistorialTraslados: TDataSource
    DataSet = qryHistorialTraslados
    Left = 872
    Top = 216
  end
  object qryHistorialTraslados: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select sbp.id, sbp.folio,sbp.concepto,'
      
        '(select nombre from clientes where cliente_id=sbp.valor_anterior' +
        ') as Cliente1,'
      
        '(select nombre from clientes where cliente_id=sbp.valor_nuevo) a' +
        's Cliente2,'
      '(select nombre from clientes where cliente_id=:cli),'
      'sbp.fecha_hora,sbp.metros from sic_predial_bitacora sbp'
      
        'where sbp.valor_anterior=:cli or (sbp.valor_nuevo=:cli)   order ' +
        'by Fecha_hora')
    Left = 816
    Top = 152
    ParamData = <
      item
        Name = 'CLI'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object frxDBDataset2: TfrxDBDataset
    UserName = 'Historial Traslados'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'FOLIO=FOLIO'
      'CONCEPTO=CONCEPTO'
      'CLIENTE1=CLIENTE1'
      'CLIENTE2=CLIENTE2'
      'NOMBRE=NOMBRE'
      'FECHA_HORA=FECHA_HORA'
      'METROS=METROS')
    DataSource = dssHistorialTraslados
    BCDToCurrency = False
    Left = 712
    Top = 160
  end
  object dsCampos: TDataSource
    DataSet = qryCampos
    Left = 640
    Top = 40
  end
  object MainMenu1: TMainMenu
    Left = 784
    Top = 376
    object Consolidado1: TMenuItem
      Caption = 'Reportes'
      object Consolidado2: TMenuItem
        Caption = 'Consolidado'
        OnClick = Consolidado2Click
      end
      object HistorialdeMovimientos1: TMenuItem
        Caption = 'Historial de Mov.'
        OnClick = HistorialdeMovimientos1Click
      end
      object HistorialTraspasos1: TMenuItem
        Caption = 'Historial Traspasos'
        OnClick = HistorialTraspasos1Click
      end
      object Impuestos1: TMenuItem
        Caption = 'Impuestos'
        OnClick = Impuestos1Click
      end
    end
  end
  object dsConsolidado: TDataSource
    DataSet = qryConsolidado
    Left = 256
    Top = 440
  end
  object dssConsolidado: TfrxDBDataset
    UserName = 'DatasetConsolidado'
    CloseDataSource = False
    DataSource = dsConsolidado
    BCDToCurrency = False
    Left = 192
    Top = 496
  end
  object rpImpuestos: TfrxReport
    Version = '6.7.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44085.402654224500000000
    ReportOptions.LastChange = 44124.381962673610000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 104
    Top = 368
    Datasets = <
      item
        DataSet = dssConsolidado
        DataSetName = 'DatasetConsolidado'
      end
      item
        DataSet = dssImpuestos
        DataSetName = 'DatasetImpuestos'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 355.600000000000000000
      PaperHeight = 215.900000000000000000
      PaperSize = 5
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object Shape24: TfrxShapeView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 86.929190000000000000
        Width = 944.882272990000000000
        Height = 18.758696690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Shape2: TfrxShapeView
        AllowVectorExport = True
        Left = 90.693230120000000000
        Top = 49.050518010000000000
        Width = 1080.945352990000000000
        Height = 18.758696690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Shape5: TfrxShapeView
        AllowVectorExport = True
        Left = 90.693230120000000000
        Top = 67.809214710000000000
        Width = 136.062852990000000000
        Height = 18.758696690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Shape6: TfrxShapeView
        AllowVectorExport = True
        Left = 90.693230120000000000
        Top = 86.567911400000000000
        Width = 136.062852990000000000
        Height = 18.758696690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Shape9: TfrxShapeView
        AllowVectorExport = True
        Left = 226.862933400000000000
        Top = 86.567911400000000000
        Width = 612.283632990000000000
        Height = 18.758696690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo1: TfrxMemoView
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 49.133890000000000000
        Width = 1080.945352990000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Manitoba Kolonie Cd. Cuauht'#233'moc, Chihuahua. Mx.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo42: TfrxMemoView
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 70.031540000000000000
        Width = 136.063048270000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Gebortsdatum')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo49: TfrxMemoView
        AllowVectorExport = True
        Left = 905.307670000000000000
        Top = 88.929190000000000000
        Width = 45.354328270000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          'Tel.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape11: TfrxShapeView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 68.031540000000000000
        Width = 944.882272990000000000
        Height = 18.758696690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo46: TfrxMemoView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 70.031540000000000000
        Width = 612.283828270000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Name von den Eigent'#252'mer')
        ParentFont = False
        VAlign = vaCenter
      end
      object Subreport1: TfrxSubreport
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 105.826840000000000000
        Width = 1080.945580000000000000
        Height = 604.724800000000000000
        Page = rpImpuestos.Page2
      end
      object DatasetConsolidadoNOMBRE: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 230.551330000000000000
        Top = 86.929190000000000000
        Width = 604.724800000000000000
        Height = 18.897650000000000000
        DataField = 'NOMBRE'
        DataSet = dssConsolidado
        DataSetName = 'DatasetConsolidado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[DatasetConsolidado."NOMBRE"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object DatasetConsolidadoTELEFONO1: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 952.441560000000000000
        Top = 86.929190000000000000
        Width = 219.212740000000000000
        Height = 18.897650000000000000
        DataField = 'TELEFONO1'
        DataSet = dssConsolidado
        DataSetName = 'DatasetConsolidado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[DatasetConsolidado."TELEFONO1"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object DatasetConsolidadoRFC_CURP: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 86.929190000000000000
        Width = 136.063080000000000000
        Height = 18.897650000000000000
        DataField = 'RFC_CURP'
        DataSet = dssConsolidado
        DataSetName = 'DatasetConsolidado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[DatasetConsolidado."RFC_CURP"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape13: TfrxShapeView
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 105.826840000000000000
        Width = 1080.945580000000000000
        Height = 604.724800000000000000
        Frame.Typ = []
      end
      object Memo7: TfrxMemoView
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 7.559060000000000000
        Width = 1080.945352990000000000
        Height = 30.236240000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Impuestos')
        ParentFont = False
        VAlign = vaCenter
      end
    end
    object Page2: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 355.600000000000000000
      PaperHeight = 215.900000000000000000
      PaperSize = 5
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 79.370130000000000000
        Width = 1268.410268000000000000
        DataSet = dssImpuestos
        DataSetName = 'DatasetImpuestos'
        RowCount = 0
        object DatasetImpuestosNOMBRE: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Width = 268.346630000000000000
          Height = 18.897650000000000000
          DataField = 'NOMBRE'
          DataSet = dssImpuestos
          DataSetName = 'DatasetImpuestos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[DatasetImpuestos."NOMBRE"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetImpuestosANO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 268.346630000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          DataField = 'ANO'
          DataSet = dssImpuestos
          DataSetName = 'DatasetImpuestos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[DatasetImpuestos."ANO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetImpuestosUNIDADES: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 139.842610000000000000
          Height = 18.897650000000000000
          DataField = 'UNIDADES'
          DataSet = dssImpuestos
          DataSetName = 'DatasetImpuestos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[DatasetImpuestos."UNIDADES"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object SysMemo1: TfrxSysMemoView
          AllowVectorExport = True
          Left = 744.567410000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[(<DatasetImpuestos."VALOR">*<DatasetImpuestos."UNIDADES">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetImpuestosMEDIDA: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 374.173470000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          DataField = 'MEDIDA'
          DataSet = dssImpuestos
          DataSetName = 'DatasetImpuestos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[DatasetImpuestos."MEDIDA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 876.850960000000000000
          Width = 139.842610000000000000
          Height = 18.897650000000000000
          DataSet = dssImpuestos
          DataSetName = 'DatasetImpuestos'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[DatasetImpuestos."PAGO"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 1268.410268000000000000
        object Shape7: TfrxShapeView
          AllowVectorExport = True
          Width = 268.346630000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338590000000000000
          Width = 268.346630000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Impuesto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape8: TfrxShapeView
          AllowVectorExport = True
          Left = 268.346630000000000000
          Width = 105.826840000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 268.346630000000000000
          Top = 11.338590000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'A'#241'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape10: TfrxShapeView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 139.842610000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 11.338590000000000000
          Width = 139.842610000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Unidades')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape12: TfrxShapeView
          AllowVectorExport = True
          Left = 744.567410000000000000
          Width = 132.283550000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 744.567410000000000000
          Top = 11.338590000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Costo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape14: TfrxShapeView
          AllowVectorExport = True
          Left = 374.173470000000000000
          Width = 230.551330000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 374.173470000000000000
          Top = 11.338590000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Medida')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape15: TfrxShapeView
          AllowVectorExport = True
          Left = 876.850960000000000000
          Width = 139.842610000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 876.850960000000000000
          Top = 7.559060000000000000
          Width = 139.842610000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Pag'#243)
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object dsImpuestos: TDataSource
    DataSet = qryImpuestos
    Left = 224
    Top = 344
  end
  object qryImpuestos: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from sic_predial_imp_cli ic'
      'join sic_predial_imp i on ic.impuesto_id=i.impuesto_id'
      'where cliente_id=:cliente')
    Left = 232
    Top = 392
    ParamData = <
      item
        Name = 'CLIENTE'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object dssImpuestos: TfrxDBDataset
    UserName = 'DatasetImpuestos'
    CloseDataSource = False
    DataSource = dsImpuestos
    BCDToCurrency = False
    Left = 160
    Top = 400
  end
end
