unit UCallesNuevo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Vcl.StdCtrls, Data.DB,
  FireDAC.Comp.Client;

type
  TFormCallesNuevo = class(TForm)
    btnGuardar: TButton;
    Conexion: TFDConnection;
    txtNombre: TEdit;
    lblNombre: TLabel;
    btnEliminar: TButton;
    procedure btnGuardarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    calle_id:integer;
  end;

var
  FormCallesNuevo: TFormCallesNuevo;

implementation

{$R *.dfm}

procedure TFormCallesNuevo.btnEliminarClick(Sender: TObject);
var
buttonSelected:Integer;
calle:string;
begin
      calle:=txtNombre.Text;
      buttonSelected := messagedlg('�Esta seguro de que desea eliminar la calle '+calle+' ?',mtError, mbOKCancel, 0);
      if buttonSelected = mrOK     then
        begin
          try
            Conexion.ExecSQL('DELETE FROM SIC_PREDIAL_CALLES WHERE ID_CALLES='+calle_id.ToString+'');
            ShowMessage('Se elimino la calle '+calle+'');
            Close;
          Except
            ShowMessage('Ocurrio un error al eliminar la calle '+calle+'');
          end;

        end;
      if buttonSelected = mrCancel then
        begin

        end;
end;

procedure TFormCallesNuevo.btnGuardarClick(Sender: TObject);
begin
if calle_id <> 0  then
  begin
    try
      Conexion.ExecSQL('UPDATE SIC_PREDIAL_CALLES SET NOMBRE='''+txtNombre.Text+''' WHERE ID_CALLES='+calle_id.ToString+'');
      ShowMessage('Calle modificada correctamente');
      Close;
    Except
      ShowMessage('Hubo un error al modificar la calle');
    end;
  end
else
  begin
    if txtNombre.Text = '' then  ShowMessage('Falta Nombre')
    else
    begin
    try
    Conexion.ExecSQL('INSERT INTO SIC_PREDIAL_CALLES(ID_CALLES,NOMBRE) VALUES(-1,'''+txtNombre.Text+''')') ;
    ShowMessage('Calle agregada correctamente');
    Close;
    Except
    ShowMessage('Hubo un error al agregar la calle');
    end;
    end;
  end;
end;

procedure TFormCallesNuevo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Conexion.Close;
end;

procedure TFormCallesNuevo.FormShow(Sender: TObject);
begin
if calle_id <> 0 then
  begin
    txtNombre.Text:=Conexion.ExecSQLScalar('SELECT NOMBRE FROM SIC_PREDIAL_CALLES WHERE ID_CALLES='+calle_id.ToString+'');
  end
else btnEliminar.Visible:=False;
end;

end.
