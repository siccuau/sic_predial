object FormConcepto: TFormConcepto
  Left = 0
  Top = 0
  Caption = 'Concepto'
  ClientHeight = 218
  ClientWidth = 365
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lblNombre: TLabel
    Left = 8
    Top = 16
    Width = 42
    Height = 13
    Caption = 'NOMBRE'
  end
  object lblNaturaleza: TLabel
    Left = 200
    Top = 16
    Width = 65
    Height = 13
    Caption = 'NATURALEZA'
  end
  object lblSerie: TLabel
    Left = 8
    Top = 72
    Width = 29
    Height = 13
    Caption = 'SERIE'
  end
  object lblConsecutivo: TLabel
    Left = 200
    Top = 72
    Width = 72
    Height = 13
    Caption = 'CONSECUTIVO'
  end
  object txtNombre: TEdit
    Left = 8
    Top = 35
    Width = 153
    Height = 21
    TabOrder = 0
  end
  object btnGuardar: TButton
    Left = 270
    Top = 185
    Width = 75
    Height = 25
    Caption = 'Guardar'
    TabOrder = 1
    OnClick = btnGuardarClick
  end
  object cboNaturaleza: TComboBox
    Left = 200
    Top = 35
    Width = 157
    Height = 21
    TabOrder = 2
    Items.Strings = (
      'Cargo'
      'Credito')
  end
  object txtSerie: TEdit
    Left = 8
    Top = 91
    Width = 153
    Height = 21
    TabOrder = 3
  end
  object txtConsecutivo: TEdit
    Left = 200
    Top = 91
    Width = 157
    Height = 21
    Alignment = taRightJustify
    Enabled = False
    NumbersOnly = True
    TabOrder = 4
    Text = '1'
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    LoginPrompt = False
    Left = 9
    Top = 174
  end
end
