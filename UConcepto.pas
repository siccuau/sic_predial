unit UConcepto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  Vcl.StdCtrls;

type
  TFormConcepto = class(TForm)
    lblNombre: TLabel;
    txtNombre: TEdit;
    btnGuardar: TButton;
    cboNaturaleza: TComboBox;
    lblNaturaleza: TLabel;
    txtSerie: TEdit;
    lblSerie: TLabel;
    txtConsecutivo: TEdit;
    lblConsecutivo: TLabel;
    Conexion: TFDConnection;
    procedure btnGuardarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormConcepto: TFormConcepto;

implementation

{$R *.dfm}

procedure TFormConcepto.btnGuardarClick(Sender: TObject);
var
id_concepto,sucursal_id:Integer;
naturaleza:string;
begin
sucursal_id:=Conexion.ExecSQLScalar('select sucursal_id from sucursales where nombre=''Matriz''');
if cboNaturaleza.Text='Cargo' then naturaleza:='C'
else naturaleza:='R';

if (cboNaturaleza.Text='') or (txtNombre.Text='') or (txtSerie.Text='') then
  begin
    ShowMessage('Falta llenar informacion');
  end
else
  begin
    try
      id_concepto:=Conexion.ExecSQLScalar('INSERT INTO CONCEPTOS_CC(CONCEPTO_CC_ID,NOMBRE,NOMBRE_ABREV,NATURALEZA,OCULTO,TIPO) VALUES(-1,'''+txtNombre.Text+''','''+txtNombre.Text+''','''+naturaleza+''',''N'',''R'') RETURNING CONCEPTO_CC_ID');
      Conexion.ExecSQL('INSERT INTO FOLIOS_CONCEPTOS(FOLIO_CONCEPTO_ID,SISTEMA,CONCEPTO_ID,SUCURSAL_ID,SERIE,CONSECUTIVO) VALUES(-1,''CC'','+id_concepto.ToString+','+sucursal_id.ToString+','''+txtSerie.Text+''','+txtConsecutivo.Text+')');
      ShowMessage('Concepto agregado correctamente');
    Except
      Conexion.Rollback;
      ShowMessage('Ocurrio un error');
    end;

  end;

end;

end.
