object ImportarTerrenos: TImportarTerrenos
  Left = 0
  Top = 0
  Caption = 'Importar Terrenos'
  ClientHeight = 133
  ClientWidth = 378
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbl_archivo: TLabel
    Left = 189
    Top = 16
    Width = 51
    Height = 13
    Caption = 'lbl_archivo'
  end
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 120
    Height = 13
    Caption = 'Seleccionar archivo excel'
  end
  object lbl_progress: TLabel
    Left = 254
    Top = 41
    Width = 3
    Height = 13
    Alignment = taCenter
    Caption = ' '
  end
  object pb_articulos: TProgressBar
    Left = 23
    Top = 41
    Width = 217
    Height = 17
    TabOrder = 0
  end
  object btn_archivo: TButton
    Left = 142
    Top = 10
    Width = 41
    Height = 25
    Caption = '...'
    TabOrder = 1
    OnClick = btn_archivoClick
  end
  object btn_importar: TButton
    Left = 48
    Top = 64
    Width = 135
    Height = 25
    Caption = 'Importar Terrenos'
    Enabled = False
    TabOrder = 2
    OnClick = btn_importarClick
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    LoginPrompt = False
    Left = 220
    Top = 70
  end
  object OpenDialog1: TOpenDialog
    Left = 288
    Top = 32
  end
end
