unit UCampoNMA;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client;

type
  TFormCampoNMA = class(TForm)
    lblNombre: TLabel;
    txtNombre: TEdit;
    btnGuardar: TButton;
    Conexion: TFDConnection;
    procedure btnGuardarClick(Sender: TObject);
  private
    { Private declarations }
  public
    id_campo:Integer;
    { Public declarations }
  end;

var
  FormCampoNMA: TFormCampoNMA;

implementation

{$R *.dfm}

procedure TFormCampoNMA.btnGuardarClick(Sender: TObject);
begin
if txtNombre.Text = '' then  ShowMessage('Falta Nombre')
else
begin
try
Conexion.ExecSQL('INSERT INTO SIC_PREDIAL_CAMPOS(ID_CAMPOS,NOMBRE) VALUES(-1,'''+txtNombre.Text+''')') ;
ShowMessage('Campo agregado');
Close;
Except
ShowMessage('Hubo un error al agregar el campo');
end;
end;



end;

end.
