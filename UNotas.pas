unit UNotas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Vcl.StdCtrls, Vcl.DBCtrls,
  Vcl.ComCtrls, Data.DB, FireDAC.Comp.Client, FireDAC.Comp.DataSet;

type
  TNotas = class(TForm)
    Label1: TLabel;
    cbCliente: TDBLookupComboBox;
    qryClientes: TFDQuery;
    dsClientes: TDataSource;
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    btnGuardar: TButton;
    DBMemo1: TDBMemo;
    qryNotas: TFDQuery;
    dsNotas: TDataSource;
    btnCancelar: TButton;
    procedure FormShow(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Notas: TNotas;

implementation

{$R *.dfm}

procedure TNotas.btnCancelarClick(Sender: TObject);
begin
Close;
end;

procedure TNotas.btnGuardarClick(Sender: TObject);
begin
  Conexion.ExecSQL('update clientes set notas=:notas where cliente_id=:cli',[DBMemo1.Text,cbCliente.KeyValue]);
  ShowMessage('Se Actualizaron las notas correctamente.');
  Close;
end;

procedure TNotas.FormShow(Sender: TObject);
begin
qryNotas.ParamByName('cli').Value:=cbCliente.KeyValue;
qryNotas.Open();
qryClientes.Open();
end;

end.
