object Notas: TNotas
  Left = 0
  Top = 0
  Caption = 'Notas'
  ClientHeight = 354
  ClientWidth = 537
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 71
    Height = 25
    Caption = 'Cliente:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object cbCliente: TDBLookupComboBox
    Left = 93
    Top = 16
    Width = 420
    Height = 27
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyField = 'CLIENTE_ID'
    ListField = 'NOMBRE'
    ListSource = dsClientes
    ParentFont = False
    TabOrder = 0
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 335
    Width = 537
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object btnGuardar: TButton
    Left = 144
    Top = 284
    Width = 89
    Height = 36
    Caption = 'Guardar'
    TabOrder = 2
    OnClick = btnGuardarClick
  end
  object DBMemo1: TDBMemo
    Left = 16
    Top = 49
    Width = 497
    Height = 224
    DataField = 'NOTAS'
    DataSource = dsNotas
    TabOrder = 3
  end
  object btnCancelar: TButton
    Left = 256
    Top = 284
    Width = 89
    Height = 36
    Caption = 'Cancelar'
    TabOrder = 4
    OnClick = btnCancelarClick
  end
  object qryClientes: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from clientes')
    Left = 472
    Top = 48
  end
  object dsClientes: TDataSource
    DataSet = qryClientes
    Left = 464
    Top = 104
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'Protocol=TCPIP'
      'Server=localhost'
      'CharacterSet=ISO8859_1'
      'RoleName=USUARIO_MICROSIP'
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    UpdateOptions.AssignedValues = [uvAutoCommitUpdates]
    UpdateOptions.AutoCommitUpdates = True
    LoginPrompt = False
    Left = 469
    Top = 160
  end
  object qryNotas: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select notas from clientes where cliente_id=:cli')
    Left = 24
    Top = 288
    ParamData = <
      item
        Name = 'CLI'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dsNotas: TDataSource
    DataSet = qryNotas
    Left = 72
    Top = 288
  end
end
