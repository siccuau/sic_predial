unit UCatalogoImpuestos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, Data.DB, Vcl.DBCtrls, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.ComCtrls, Vcl.StdCtrls,UMedidas, Vcl.Menus;

type
  TCatalogoImpuestos = class(TForm)
    lblNombreImpuesto: TLabel;
    lblA�o: TLabel;
    lblMedida: TLabel;
    lblValor: TLabel;
    txtA�o: TEdit;
    txtValor: TEdit;
    btnGuardar: TButton;
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    txtNombre: TEdit;
    qryUnidades: TFDQuery;
    cboMedida: TDBLookupComboBox;
    dsUnidades: TDataSource;
    popmnuConcepto: TPopupMenu;
    Nuevo1: TMenuItem;
    procedure btnGuardarClick(Sender: TObject);
    procedure Nuevo1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CatalogoImpuestos: TCatalogoImpuestos;

implementation

{$R *.dfm}

procedure TCatalogoImpuestos.btnGuardarClick(Sender: TObject);
var
nombre_impuesto:Integer;
buttonSelected : Integer;
begin
if (txtA�o.Text='') or (txtValor.Text='') or (cboMedida.Text='')  then
  begin
    ShowMessage('Faltan campos por llenar.');
  end
else
  begin
    try
    nombre_impuesto:=Conexion.ExecSQLScalar('SELECT count(NOMBRE) FROM SIC_PREDIAL_IMP WHERE NOMBRE=:nom AND ANO=:ano',[txtNombre.Text,txtA�o.Text]);
    if nombre_impuesto = 0 then
      begin
        Conexion.ExecSQL('INSERT INTO SIC_PREDIAL_IMP(IMPUESTO_ID,NOMBRE,ANO,MEDIDA,VALOR) VALUES(-1,'''+txtNombre.Text+''','''+txtA�o.Text+''','''+cboMedida.Text+''','+txtValor.Text+')');
        ShowMessage('Impuesto agregado');
        Close;
      end
    else
      begin
        buttonSelected := messagedlg('Ya existe un valor registrado para el impuesto: '+txtNombre.Text+' quiere actualizarlo?',mtError, mbOKCancel, 0);
        if buttonSelected = mrOK     then
        begin
          Conexion.ExecSQL('UPDATE SIC_PREDIAL_IMP SET VALOR=:val,MEDIDA=:med WHERE NOMBRE=:nom AND ANO=:ano',[txtValor.Text,cboMedida.Text,txtNombre.Text,txtA�o.Text]);
          ShowMessage('Impuesto actualizado');
        end;
        if buttonSelected = mrCancel then Close;
      end;

    Except

    end;
  end;

end;

procedure TCatalogoImpuestos.FormShow(Sender: TObject);
begin
qryUnidades.Open();
end;

procedure TCatalogoImpuestos.Nuevo1Click(Sender: TObject);
var
FormMedidas:TMedidas;
begin
  FormMedidas:= TMedidas.Create(nil);
  FormMedidas.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormMedidas.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormMedidas.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormMedidas.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormMedidas.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  FormMedidas.ShowModal;
  qryUnidades.Refresh;
end;

end.
