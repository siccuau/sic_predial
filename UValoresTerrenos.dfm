﻿object FormValoresTerrenos: TFormValoresTerrenos
  Left = 0
  Top = 0
  Caption = 'Valores Terrenos'
  ClientHeight = 362
  ClientWidth = 548
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblTipoTerreno: TLabel
    Left = 16
    Top = 8
    Width = 74
    Height = 13
    Caption = 'TIPO TERRENO'
  end
  object lblAño: TLabel
    Left = 224
    Top = 8
    Width = 22
    Height = 13
    Caption = 'A'#209'O'
  end
  object lblMedida: TLabel
    Left = 16
    Top = 64
    Width = 39
    Height = 13
    Caption = 'MEDIDA'
  end
  object lblValor: TLabel
    Left = 224
    Top = 64
    Width = 33
    Height = 13
    Caption = 'VALOR'
  end
  object lblMunicipio: TLabel
    Left = 16
    Top = 121
    Width = 55
    Height = 13
    Caption = 'MUNICIPIO'
  end
  object cboTipoTerreno: TComboBox
    Left = 16
    Top = 27
    Width = 145
    Height = 21
    TabOrder = 0
    OnChange = cboTipoTerrenoChange
    Items.Strings = (
      'RIEGO'
      'TEMPORAL')
  end
  object txtAño: TEdit
    Left = 224
    Top = 27
    Width = 121
    Height = 21
    MaxLength = 4
    NumbersOnly = True
    TabOrder = 1
  end
  object cboMedida: TComboBox
    Left = 16
    Top = 83
    Width = 145
    Height = 21
    TabOrder = 2
    Items.Strings = (
      'METROS'
      'ACRES')
  end
  object txtValor: TEdit
    Left = 224
    Top = 83
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object btnGuardar: TButton
    Left = 270
    Top = 136
    Width = 75
    Height = 25
    Caption = 'Guardar'
    TabOrder = 4
    OnClick = btnGuardarClick
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 184
    Width = 532
    Height = 137
    DataSource = dsValores
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 343
    Width = 548
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object dtpYear: TDateTimePicker
    Left = 224
    Top = 27
    Width = 121
    Height = 21
    Date = 44097.000000000000000000
    Format = 'yyyy'
    Time = 0.573060266200627700
    DateMode = dmUpDown
    TabOrder = 7
    OnChange = dtpYearChange
  end
  object cboMun: TComboBox
    Left = 16
    Top = 138
    Width = 170
    Height = 21
    TabOrder = 8
    Visible = False
  end
  object cboMunicipio: TComboBox
    Left = 16
    Top = 138
    Width = 170
    Height = 21
    ItemIndex = 0
    TabOrder = 9
    Text = 'Cuauht'#233'moc'
    Items.Strings = (
      'Cuauht'#233'moc'
      'Cusihuiriachi')
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    LoginPrompt = False
    Left = 160
    Top = 313
  end
  object qryValores: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from sic_predial_valores')
    Left = 248
    Top = 312
    object qryValoresTIPO_TERRENO: TStringField
      DisplayWidth = 30
      FieldName = 'TIPO_TERRENO'
      Origin = 'TIPO_TERRENO'
      Size = 50
    end
    object qryValoresANO: TStringField
      FieldName = 'ANO'
      Origin = 'ANO'
      Size = 4
    end
    object qryValoresMEDIDA: TStringField
      DisplayWidth = 15
      FieldName = 'MEDIDA'
      Origin = 'MEDIDA'
      Size = 30
    end
    object qryValoresVALOR: TIntegerField
      DisplayWidth = 5
      FieldName = 'VALOR'
      Origin = 'VALOR'
    end
    object qryValoresMUNICIPIO: TStringField
      DisplayWidth = 30
      FieldName = 'MUNICIPIO'
      Origin = 'MUNICIPIO'
      Size = 100
    end
  end
  object dsValores: TDataSource
    DataSet = qryValores
    Left = 40
    Top = 312
  end
  object qryMunicipio: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from ciudades')
    Left = 94
    Top = 312
    object qryMunicipioCIUDAD_ID: TIntegerField
      FieldName = 'CIUDAD_ID'
      Origin = 'CIUDAD_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryMunicipioNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Required = True
      Size = 50
    end
    object qryMunicipioCLAVE_FISCAL: TStringField
      FieldName = 'CLAVE_FISCAL'
      Origin = 'CLAVE_FISCAL'
      Size = 3
    end
    object qryMunicipioES_PREDET: TStringField
      FieldName = 'ES_PREDET'
      Origin = 'ES_PREDET'
      FixedChar = True
      Size = 1
    end
    object qryMunicipioESTADO_ID: TIntegerField
      FieldName = 'ESTADO_ID'
      Origin = 'ESTADO_ID'
      Required = True
    end
    object qryMunicipioUSUARIO_CREADOR: TStringField
      FieldName = 'USUARIO_CREADOR'
      Origin = 'USUARIO_CREADOR'
      Size = 31
    end
    object qryMunicipioFECHA_HORA_CREACION: TSQLTimeStampField
      FieldName = 'FECHA_HORA_CREACION'
      Origin = 'FECHA_HORA_CREACION'
    end
    object qryMunicipioUSUARIO_AUT_CREACION: TStringField
      FieldName = 'USUARIO_AUT_CREACION'
      Origin = 'USUARIO_AUT_CREACION'
      Size = 31
    end
    object qryMunicipioUSUARIO_ULT_MODIF: TStringField
      FieldName = 'USUARIO_ULT_MODIF'
      Origin = 'USUARIO_ULT_MODIF'
      Size = 31
    end
    object qryMunicipioFECHA_HORA_ULT_MODIF: TSQLTimeStampField
      FieldName = 'FECHA_HORA_ULT_MODIF'
      Origin = 'FECHA_HORA_ULT_MODIF'
    end
    object qryMunicipioUSUARIO_AUT_MODIF: TStringField
      FieldName = 'USUARIO_AUT_MODIF'
      Origin = 'USUARIO_AUT_MODIF'
      Size = 31
    end
  end
  object BindSourceDB1: TBindSourceDB
    DataSet = qryMunicipio
    ScopeMappings = <>
    Left = 392
    Top = 16
  end
  object BindingsList1: TBindingsList
    Methods = <>
    OutputConverters = <>
    Left = 396
    Top = 69
    object LinkListControlToField1: TLinkListControlToField
      Category = 'Quick Bindings'
      DataSource = BindSourceDB1
      FieldName = 'NOMBRE'
      Control = cboMun
      FillExpressions = <>
      FillHeaderExpressions = <>
      FillBreakGroups = <>
    end
  end
end
