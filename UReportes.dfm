﻿object Reportes: TReportes
  Left = 0
  Top = 0
  Caption = 'Reporteador'
  ClientHeight = 603
  ClientWidth = 591
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 71
    Height = 25
    Caption = 'Cliente:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 584
    Width = 591
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 56
    Width = 433
    Height = 65
    Caption = 'Historial de Movimientos'
    TabOrder = 1
    object Label2: TLabel
      Left = 19
      Top = 30
      Width = 14
      Height = 13
      Caption = 'del'
    end
    object Label3: TLabel
      Left = 179
      Top = 30
      Width = 8
      Height = 13
      Caption = 'al'
    end
    object DateTimePicker1: TDateTimePicker
      Left = 39
      Top = 24
      Width = 129
      Height = 21
      Date = 44099.000000000000000000
      Time = 0.412942627313896100
      TabOrder = 0
    end
    object DateTimePicker2: TDateTimePicker
      Left = 199
      Top = 24
      Width = 129
      Height = 21
      Date = 44099.000000000000000000
      Time = 0.412942627313896100
      TabOrder = 1
    end
    object btnHistMov: TButton
      Left = 344
      Top = 22
      Width = 75
      Height = 25
      Caption = 'Imprimir'
      TabOrder = 2
      OnClick = btnHistMovClick
    end
  end
  object cbCliente: TDBLookupComboBox
    Left = 93
    Top = 14
    Width = 491
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyField = 'CLIENTE_ID'
    ListField = 'NOMBRE'
    ListSource = dsClientes
    ParentFont = False
    TabOrder = 2
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 127
    Width = 433
    Height = 65
    Caption = 'Historial de Traspasos'
    TabOrder = 3
    object Label4: TLabel
      Left = 19
      Top = 30
      Width = 14
      Height = 13
      Caption = 'del'
    end
    object Label5: TLabel
      Left = 179
      Top = 30
      Width = 8
      Height = 13
      Caption = 'al'
    end
    object DateTimePicker3: TDateTimePicker
      Left = 39
      Top = 24
      Width = 129
      Height = 21
      Date = 44099.000000000000000000
      Time = 0.412942627313896100
      TabOrder = 0
    end
    object DateTimePicker4: TDateTimePicker
      Left = 199
      Top = 24
      Width = 129
      Height = 21
      Date = 44099.000000000000000000
      Time = 0.412942627313896100
      TabOrder = 1
    end
    object btnHistTrasp: TButton
      Left = 344
      Top = 22
      Width = 75
      Height = 25
      Caption = 'Imprimir'
      TabOrder = 2
      OnClick = btnHistTraspClick
    end
  end
  object GroupBox3: TGroupBox
    Left = 447
    Top = 56
    Width = 137
    Height = 65
    Caption = 'Propiedades Consolidadas'
    TabOrder = 4
    object btnConsolidado: TButton
      Left = 31
      Top = 22
      Width = 75
      Height = 25
      Caption = 'Imprimir'
      TabOrder = 0
      OnClick = btnConsolidadoClick
    end
  end
  object GroupBox4: TGroupBox
    Left = 447
    Top = 192
    Width = 137
    Height = 139
    Caption = 'Predial Anterior al A'#241'o'
    TabOrder = 5
    object btnPredialAnterior: TButton
      Left = 31
      Top = 78
      Width = 75
      Height = 25
      Caption = 'Imprimir'
      TabOrder = 0
      OnClick = btnPredialAnteriorClick
    end
    object cbAño: TComboBox
      Left = 32
      Top = 43
      Width = 73
      Height = 21
      ItemIndex = 5
      TabOrder = 1
      Text = '2020'
      Items.Strings = (
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030')
    end
  end
  object GroupBox5: TGroupBox
    Left = 8
    Top = 192
    Width = 433
    Height = 139
    Caption = 'Predial Pendiente de Pago'
    TabOrder = 6
    object Button1: TButton
      Left = 344
      Top = 62
      Width = 75
      Height = 25
      Caption = 'Imprimir'
      TabOrder = 0
      OnClick = Button1Click
    end
    object RadioGroup1: TRadioGroup
      Left = 8
      Top = 24
      Width = 320
      Height = 105
      Caption = 'Filtrar por'
      ItemIndex = 0
      Items.Strings = (
        'Consolidado'
        'Por Persona'
        'Por Campo')
      TabOrder = 1
      OnClick = RadioGroup1Click
    end
    object cbZona: TDBLookupComboBox
      Left = 92
      Top = 101
      Width = 133
      Height = 21
      KeyField = 'NOMBRE'
      ListSource = dsCampos
      TabOrder = 2
      Visible = False
    end
  end
  object GroupBox6: TGroupBox
    Left = 447
    Top = 127
    Width = 137
    Height = 65
    Caption = 'Auxiliar Cliente'
    TabOrder = 7
    object Button2: TButton
      Left = 31
      Top = 22
      Width = 75
      Height = 25
      Caption = 'Imprimir'
      TabOrder = 0
      OnClick = Button2Click
    end
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'Protocol=TCPIP'
      'Server=localhost'
      'CharacterSet=ISO8859_1'
      'RoleName=USUARIO_MICROSIP'
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    UpdateOptions.AssignedValues = [uvAutoCommitUpdates]
    UpdateOptions.AutoCommitUpdates = True
    LoginPrompt = False
    Left = 13
    Top = 344
  end
  object dsClientes: TDataSource
    DataSet = qryClientes
    Left = 96
    Top = 344
  end
  object qryClientes: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from clientes')
    Left = 56
    Top = 344
    object qryClientesCLIENTE_ID: TIntegerField
      FieldName = 'CLIENTE_ID'
      Origin = 'CLIENTE_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryClientesNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Required = True
      Size = 200
    end
    object qryClientesCONTACTO1: TStringField
      FieldName = 'CONTACTO1'
      Origin = 'CONTACTO1'
      Size = 50
    end
    object qryClientesCONTACTO2: TStringField
      FieldName = 'CONTACTO2'
      Origin = 'CONTACTO2'
      Size = 50
    end
    object qryClientesESTATUS: TStringField
      FieldName = 'ESTATUS'
      Origin = 'ESTATUS'
      FixedChar = True
      Size = 1
    end
    object qryClientesCAUSA_SUSP: TStringField
      FieldName = 'CAUSA_SUSP'
      Origin = 'CAUSA_SUSP'
      Size = 100
    end
    object qryClientesFECHA_SUSP: TDateField
      FieldName = 'FECHA_SUSP'
      Origin = 'FECHA_SUSP'
    end
    object qryClientesCOBRAR_IMPUESTOS: TStringField
      FieldName = 'COBRAR_IMPUESTOS'
      Origin = 'COBRAR_IMPUESTOS'
      FixedChar = True
      Size = 1
    end
    object qryClientesRETIENE_IMPUESTOS: TStringField
      FieldName = 'RETIENE_IMPUESTOS'
      Origin = 'RETIENE_IMPUESTOS'
      FixedChar = True
      Size = 1
    end
    object qryClientesSUJETO_IEPS: TStringField
      FieldName = 'SUJETO_IEPS'
      Origin = 'SUJETO_IEPS'
      Required = True
      FixedChar = True
      Size = 1
    end
    object qryClientesGENERAR_INTERESES: TStringField
      FieldName = 'GENERAR_INTERESES'
      Origin = 'GENERAR_INTERESES'
      FixedChar = True
      Size = 1
    end
    object qryClientesEMITIR_EDOCTA: TStringField
      FieldName = 'EMITIR_EDOCTA'
      Origin = 'EMITIR_EDOCTA'
      FixedChar = True
      Size = 1
    end
    object qryClientesDIFERIR_CFDI_COBROS: TBooleanField
      FieldName = 'DIFERIR_CFDI_COBROS'
      Origin = 'DIFERIR_CFDI_COBROS'
      Required = True
    end
    object qryClientesLIMITE_CREDITO: TFMTBCDField
      FieldName = 'LIMITE_CREDITO'
      Origin = 'LIMITE_CREDITO'
      Required = True
      Precision = 18
      Size = 2
    end
    object qryClientesMONEDA_ID: TIntegerField
      FieldName = 'MONEDA_ID'
      Origin = 'MONEDA_ID'
      Required = True
    end
    object qryClientesCOND_PAGO_ID: TIntegerField
      FieldName = 'COND_PAGO_ID'
      Origin = 'COND_PAGO_ID'
      Required = True
    end
    object qryClientesTIPO_CLIENTE_ID: TIntegerField
      FieldName = 'TIPO_CLIENTE_ID'
      Origin = 'TIPO_CLIENTE_ID'
    end
    object qryClientesZONA_CLIENTE_ID: TIntegerField
      FieldName = 'ZONA_CLIENTE_ID'
      Origin = 'ZONA_CLIENTE_ID'
    end
    object qryClientesCOBRADOR_ID: TIntegerField
      FieldName = 'COBRADOR_ID'
      Origin = 'COBRADOR_ID'
    end
    object qryClientesVENDEDOR_ID: TIntegerField
      FieldName = 'VENDEDOR_ID'
      Origin = 'VENDEDOR_ID'
    end
    object qryClientesNOTAS: TMemoField
      FieldName = 'NOTAS'
      Origin = 'NOTAS'
      BlobType = ftMemo
    end
    object qryClientesCUENTA_CXC: TStringField
      FieldName = 'CUENTA_CXC'
      Origin = 'CUENTA_CXC'
      Size = 30
    end
    object qryClientesCUENTA_ANTICIPOS: TStringField
      FieldName = 'CUENTA_ANTICIPOS'
      Origin = 'CUENTA_ANTICIPOS'
      Size = 30
    end
    object qryClientesFORMATOS_EMAIL: TStringField
      FieldName = 'FORMATOS_EMAIL'
      Origin = 'FORMATOS_EMAIL'
      Size = 100
    end
    object qryClientesRECEPTOR_CFD: TStringField
      FieldName = 'RECEPTOR_CFD'
      Origin = 'RECEPTOR_CFD'
      Size = 30
    end
    object qryClientesNUM_PROV_CLIENTE: TStringField
      FieldName = 'NUM_PROV_CLIENTE'
      Origin = 'NUM_PROV_CLIENTE'
      Size = 35
    end
    object qryClientesCAMPOS_ADDENDA: TMemoField
      FieldName = 'CAMPOS_ADDENDA'
      Origin = 'CAMPOS_ADDENDA'
      BlobType = ftMemo
    end
    object qryClientesUSUARIO_CREADOR: TStringField
      FieldName = 'USUARIO_CREADOR'
      Origin = 'USUARIO_CREADOR'
      Size = 31
    end
    object qryClientesFECHA_HORA_CREACION: TSQLTimeStampField
      FieldName = 'FECHA_HORA_CREACION'
      Origin = 'FECHA_HORA_CREACION'
    end
    object qryClientesUSUARIO_AUT_CREACION: TStringField
      FieldName = 'USUARIO_AUT_CREACION'
      Origin = 'USUARIO_AUT_CREACION'
      Size = 31
    end
    object qryClientesUSUARIO_ULT_MODIF: TStringField
      FieldName = 'USUARIO_ULT_MODIF'
      Origin = 'USUARIO_ULT_MODIF'
      Size = 31
    end
    object qryClientesFECHA_HORA_ULT_MODIF: TSQLTimeStampField
      FieldName = 'FECHA_HORA_ULT_MODIF'
      Origin = 'FECHA_HORA_ULT_MODIF'
    end
    object qryClientesUSUARIO_AUT_MODIF: TStringField
      FieldName = 'USUARIO_AUT_MODIF'
      Origin = 'USUARIO_AUT_MODIF'
      Size = 31
    end
  end
  object qryHistorial: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select sbp.*,(select nombre from clientes where cliente_id=:cli)' +
        ' from sic_predial_bitacora sbp'
      
        'where sbp.cliente_id=:cli and fecha_hora between :F_FINI and :F_' +
        'FFIN order by Fecha_hora')
    Left = 10
    Top = 392
    ParamData = <
      item
        Name = 'CLI'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'F_FINI'
        ParamType = ptInput
      end
      item
        Name = 'F_FFIN'
        ParamType = ptInput
      end>
    object qryHistorialID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object qryHistorialFOLIO: TStringField
      FieldName = 'FOLIO'
      Origin = 'FOLIO'
    end
    object qryHistorialCLIENTE_ID: TIntegerField
      FieldName = 'CLIENTE_ID'
      Origin = 'CLIENTE_ID'
    end
    object qryHistorialCONCEPTO: TStringField
      FieldName = 'CONCEPTO'
      Origin = 'CONCEPTO'
      Size = 350
    end
    object qryHistorialVALOR_ANTERIOR: TStringField
      FieldName = 'VALOR_ANTERIOR'
      Origin = 'VALOR_ANTERIOR'
      Size = 100
    end
    object qryHistorialVALOR_NUEVO: TStringField
      FieldName = 'VALOR_NUEVO'
      Origin = 'VALOR_NUEVO'
      Size = 100
    end
    object qryHistorialMETROS: TFMTBCDField
      FieldName = 'METROS'
      Origin = 'METROS'
      Precision = 18
      Size = 2
    end
    object qryHistorialUSUARIO: TStringField
      FieldName = 'USUARIO'
      Origin = 'USUARIO'
      Size = 30
    end
    object qryHistorialFECHA_HORA: TSQLTimeStampField
      FieldName = 'FECHA_HORA'
      Origin = 'FECHA_HORA'
    end
    object qryHistorialNOMBRE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 200
    end
  end
  object dssHistorial: TDataSource
    DataSet = qryHistorial
    Left = 58
    Top = 392
  end
  object rptHistorial: TfrxReport
    Version = '6.7.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44089.480742002300000000
    ReportOptions.LastChange = 44097.669668576390000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 98
    Top = 392
    Datasets = <
      item
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 279.400000000000000000
      PaperSize = 1
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 71.811070000000000000
        Top = 18.897650000000000000
        Width = 740.409927000000000000
        object frxDBDataset1NOMBRE: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Top = 26.456710000000000000
          Width = 737.008350000000000000
          Height = 18.897650000000000000
          DataField = 'NOMBRE'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDBDataset1."NOMBRE"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 52.913420000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Folio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 52.913420000000000000
          Width = 204.094620000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Concepto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 52.913420000000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor Anterior')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 52.913420000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor Nuevo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Top = 52.913420000000000000
          Width = 185.196970000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Fecha')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Width = 737.008350000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'HISTORIAL DE MOVIMIENTOS')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 151.181200000000000000
        Width = 740.409927000000000000
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
        RowCount = 0
        object frxDBDataset1FOLIO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DataField = 'FOLIO'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBDataset1."FOLIO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBDataset1VALOR_ANTERIOR: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 355.275820000000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_ANTERIOR'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBDataset1."VALOR_ANTERIOR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBDataset1VALOR_NUEVO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_NUEVO'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBDataset1."VALOR_NUEVO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBDataset1FECHA_HORA: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 551.811380000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          DataField = 'FECHA_HORA'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBDataset1."FECHA_HORA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBDataset1CONCEPTO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 204.094620000000000000
          Height = 18.897650000000000000
          DataField = 'CONCEPTO'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDBDataset1."CONCEPTO"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object qryHistorialTraslados: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select sbp.id, sbp.folio,sbp.concepto,'
      
        '(select nombre from clientes where cliente_id=sbp.valor_anterior' +
        ') as Cliente1,'
      
        '(select nombre from clientes where cliente_id=sbp.valor_nuevo) a' +
        's Cliente2,'
      '(select nombre from clientes where cliente_id=:cli),'
      'sbp.fecha_hora,sbp.metros from sic_predial_bitacora sbp'
      
        'where (sbp.valor_anterior=:cli and sbp.fecha_hora between :F_FIN' +
        'I and :F_FFIN)'
      
        ' or (sbp.valor_nuevo=:cli and sbp.fecha_hora between :F_FINI and' +
        ' :F_FFIN)'
      '    order by Fecha_hora')
    Left = 10
    Top = 448
    ParamData = <
      item
        Name = 'CLI'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'F_FINI'
        ParamType = ptInput
      end
      item
        Name = 'F_FFIN'
        ParamType = ptInput
      end>
    object qryHistorialTrasladosID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object qryHistorialTrasladosFOLIO: TStringField
      FieldName = 'FOLIO'
      Origin = 'FOLIO'
    end
    object qryHistorialTrasladosCONCEPTO: TStringField
      FieldName = 'CONCEPTO'
      Origin = 'CONCEPTO'
      Size = 350
    end
    object qryHistorialTrasladosCLIENTE1: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CLIENTE1'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 200
    end
    object qryHistorialTrasladosCLIENTE2: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CLIENTE2'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 200
    end
    object qryHistorialTrasladosNOMBRE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 200
    end
    object qryHistorialTrasladosFECHA_HORA: TSQLTimeStampField
      FieldName = 'FECHA_HORA'
      Origin = 'FECHA_HORA'
    end
    object qryHistorialTrasladosMETROS: TFMTBCDField
      FieldName = 'METROS'
      Origin = 'METROS'
      Precision = 18
      Size = 2
    end
  end
  object rptHistorialTraslados: TfrxReport
    Version = '6.7.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44089.480742002300000000
    ReportOptions.LastChange = 44098.404619212960000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 98
    Top = 448
    Datasets = <
      item
        DataSet = frxDBDataset2
        DataSetName = 'Historial Traslados'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 279.400000000000000000
      PaperSize = 1
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 71.811070000000000000
        Top = 18.897650000000000000
        Width = 740.409927000000000000
        object frxDBDataset1NOMBRE: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Top = 19.118120000000000000
          Width = 737.008350000000000000
          Height = 18.897650000000000000
          DataField = 'NOMBRE'
          DataSet = frxDBDataset2
          DataSetName = 'Historial Traslados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Historial Traslados."NOMBRE"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354360000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Folio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047310000000000000
          Top = 45.354360000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Concepto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Top = 45.354360000000000000
          Width = 170.078740160000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'De')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409710000000000000
          Top = 45.354360000000000000
          Width = 170.078740160000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'A')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 646.299630000000000000
          Top = 45.354360000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Fecha')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 45.354360000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Top = 2.000000000000000000
          Width = 737.008350000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset2
          DataSetName = 'Historial Traslados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'TRASPASOS DE TERRENOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object SysMemo1: TfrxSysMemoView
          AllowVectorExport = True
          Top = 26.456710000000000000
          Width = 245.669450000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DATE]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 41.574830000000000000
        Top = 151.181200000000000000
        Width = 740.409927000000000000
        DataSet = frxDBDataset2
        DataSetName = 'Historial Traslados'
        RowCount = 0
        object frxDBDataset1FOLIO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Width = 102.047310000000000000
          Height = 26.456710000000000000
          DataField = 'FOLIO'
          DataSet = frxDBDataset2
          DataSetName = 'Historial Traslados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Historial Traslados."FOLIO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBDataset1FECHA_HORA: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 646.299630000000000000
          Width = 94.488250000000000000
          Height = 26.456710000000000000
          DataField = 'FECHA_HORA'
          DataSet = frxDBDataset2
          DataSetName = 'Historial Traslados'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Historial Traslados."FECHA_HORA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBDataset1CONCEPTO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 102.047310000000000000
          Width = 132.283550000000000000
          Height = 26.456710000000000000
          DataField = 'CONCEPTO'
          DataSet = frxDBDataset2
          DataSetName = 'Historial Traslados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Historial Traslados."CONCEPTO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object HistorialTrasladosCLIENTE1: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 234.330860000000000000
          Width = 170.078740160000000000
          Height = 26.456710000000000000
          DataField = 'CLIENTE1'
          DataSet = frxDBDataset2
          DataSetName = 'Historial Traslados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Historial Traslados."CLIENTE1"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object HistorialTrasladosCLIENTE2: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 404.409710000000000000
          Width = 170.078740160000000000
          Height = 26.456710000000000000
          DataField = 'CLIENTE2'
          DataSet = frxDBDataset2
          DataSetName = 'Historial Traslados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Historial Traslados."CLIENTE2"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 34.015770000000000000
          Width = 740.787880000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object HistorialTrasladosMETROS: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 71.811070000000000000
          Height = 26.456710000000000000
          DataField = 'METROS'
          DataSet = frxDBDataset2
          DataSetName = 'Historial Traslados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Historial Traslados."METROS"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object dssHistorialTraslados: TDataSource
    DataSet = qryHistorialTraslados
    Left = 58
    Top = 448
  end
  object frxDBDataset1: TfrxDBDataset
    UserName = 'frxDBDataset1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'FOLIO=FOLIO'
      'CLIENTE_ID=CLIENTE_ID'
      'CONCEPTO=CONCEPTO'
      'VALOR_ANTERIOR=VALOR_ANTERIOR'
      'VALOR_NUEVO=VALOR_NUEVO'
      'METROS=METROS'
      'USUARIO=USUARIO'
      'FECHA_HORA=FECHA_HORA'
      'NOMBRE=NOMBRE')
    DataSource = dssHistorial
    BCDToCurrency = False
    Left = 146
    Top = 392
  end
  object frxDBDataset2: TfrxDBDataset
    UserName = 'Historial Traslados'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'FOLIO=FOLIO'
      'CONCEPTO=CONCEPTO'
      'CLIENTE1=CLIENTE1'
      'CLIENTE2=CLIENTE2'
      'NOMBRE=NOMBRE'
      'FECHA_HORA=FECHA_HORA'
      'METROS=METROS')
    DataSource = dssHistorialTraslados
    BCDToCurrency = False
    Left = 146
    Top = 448
  end
  object qryPredialPend: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select cc.*,c.nombre,dc.colonia from get_cargos_cc(current_date,' +
        ' '#39'P'#39',0,'#39'S'#39','#39'S'#39','#39'N'#39') cc'
      'join conceptos_cc c on c.concepto_cc_id=cc.concepto_cc_id'
      'join dirs_clientes dc on dc.cliente_id=cc.cliente_id'
      'where UPPER(c.nombre)='#39'PREDIAL'#39)
    Left = 10
    Top = 496
  end
  object rptPredialPend: TfrxReport
    Version = '6.7.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44089.480742002300000000
    ReportOptions.LastChange = 44102.548048692130000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 98
    Top = 496
    Datasets = <
      item
        DataSet = frxDBDataset3
        DataSetName = 'Predial Pendiente'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 279.400000000000000000
      PaperSize = 1
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 68.031540000000000000
        Top = 18.897650000000000000
        Width = 740.409927000000000000
        object Memo7: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Top = 2.000000000000000000
          Width = 737.008350000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset3
          DataSetName = 'Predial Pendiente'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'PENDIENTE DE PREDIAL')
          ParentFont = False
          VAlign = vaCenter
        end
        object SysMemo1: TfrxSysMemoView
          AllowVectorExport = True
          Top = 26.456710000000000000
          Width = 245.669450000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DATE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 49.133890000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Folio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 49.133890000000000000
          Width = 154.960620160000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Zona')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Top = 49.133890000000000000
          Width = 185.196860160000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Importe')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 49.133890000000000000
          Width = 128.504020000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Fecha')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 49.133890000000000000
          Width = 154.960620160000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Campo')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 26.456710000000000000
        Top = 200.315090000000000000
        Width = 740.409927000000000000
        DataSet = frxDBDataset3
        DataSetName = 'Predial Pendiente'
        RowCount = 0
        object frxDBDataset1FOLIO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Width = 102.047310000000000000
          Height = 26.456710000000000000
          DataField = 'FOLIO'
          DataSet = frxDBDataset3
          DataSetName = 'Predial Pendiente'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Predial Pendiente."FOLIO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBDataset1FECHA_HORA: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 128.504020000000000000
          Height = 26.456710000000000000
          DataSet = frxDBDataset3
          DataSetName = 'Predial Pendiente'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Predial Pendiente."FECHA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object HistorialTrasladosCLIENTE1: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 154.960620160000000000
          Height = 26.456710000000000000
          DataSet = frxDBDataset3
          DataSetName = 'Predial Pendiente'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '13 R')
          ParentFont = False
          VAlign = vaCenter
        end
        object HistorialTrasladosCLIENTE2: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 415.748300000000000000
          Width = 185.196860160000000000
          Height = 26.456710000000000000
          DataSet = frxDBDataset3
          DataSetName = 'Predial Pendiente'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Predial Pendiente."SALDO_CARGO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 264.567100000000000000
          Width = 154.960620160000000000
          Height = 26.456710000000000000
          DataSet = frxDBDataset3
          DataSetName = 'Predial Pendiente'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Predial Pendiente."COLONIA"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 147.401670000000000000
        Width = 740.409927000000000000
        Condition = 'Predial Pendiente."CLIENTE_ID"'
        object frxDBDataset1CONCEPTO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 389.291590000000000000
          Height = 26.456710000000000000
          DataSet = frxDBDataset3
          DataSetName = 'Predial Pendiente'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '-------- [Predial Pendiente."NOMBRE_CLIENTE"]--------')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object dsPredialPend: TDataSource
    DataSet = qryPredialPend
    Left = 58
    Top = 496
  end
  object frxDBDataset3: TfrxDBDataset
    UserName = 'Predial Pendiente'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DOCTO_CC_ID=DOCTO_CC_ID'
      'CONCEPTO_CC_ID=CONCEPTO_CC_ID'
      'NOMBRE_ABREV_CONCEPTO=NOMBRE_ABREV_CONCEPTO'
      'FOLIO=FOLIO'
      'FECHA=FECHA'
      'FECHA_VENCIMIENTO=FECHA_VENCIMIENTO'
      'IMPORTE_ORIG_CARGO=IMPORTE_ORIG_CARGO'
      'SALDO_CARGO=SALDO_CARGO'
      'CLIENTE_ID=CLIENTE_ID'
      'NOMBRE_CLIENTE=NOMBRE_CLIENTE'
      'CLAVE_CLIENTE=CLAVE_CLIENTE'
      'MONEDA_ID=MONEDA_ID'
      'ESTADO_ID=ESTADO_ID'
      'CIUDAD_ID=CIUDAD_ID'
      'NOMBRE_CIUDAD=NOMBRE_CIUDAD'
      'TIPO_CLIENTE_ID=TIPO_CLIENTE_ID'
      'NOMBRE_TIPO_CLIENTE=NOMBRE_TIPO_CLIENTE'
      'ZONA_CLIENTE_ID=ZONA_CLIENTE_ID'
      'NOMBRE_ZONA_CLIENTE=NOMBRE_ZONA_CLIENTE'
      'COBRADOR_ID=COBRADOR_ID'
      'NOMBRE_COBRADOR=NOMBRE_COBRADOR'
      'SALDO_CLIENTE=SALDO_CLIENTE'
      'SALDO_XACR_CLIENTE=SALDO_XACR_CLIENTE'
      'NOMBRE=NOMBRE'
      'COLONIA=COLONIA')
    DataSource = dsPredialPend
    BCDToCurrency = False
    Left = 146
    Top = 496
  end
  object qryCampos: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from sic_predial_campos')
    Left = 8
    Top = 544
  end
  object dsCampos: TDataSource
    DataSet = qryCampos
    Left = 58
    Top = 544
  end
  object qryAuxiliar: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select * from auxiliar_cliente(:cli,'#39'01/01/2020'#39',current_date,0,' +
        #39'N'#39') ac'
      'join clientes c on c.cliente_id=ac.cliente_id'
      
        'where upper(nombre_concep) like '#39'%PREDIAL%'#39' and (select naturale' +
        'za from conceptos_cc where nombre=ac.nombre_concep)='#39'C'#39)
    Left = 354
    Top = 504
    ParamData = <
      item
        Name = 'CLI'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryAuxiliarCLIENTE_ID: TIntegerField
      FieldName = 'CLIENTE_ID'
      Origin = 'CLIENTE_ID'
    end
    object qryAuxiliarDOCTO_CC_ID: TIntegerField
      FieldName = 'DOCTO_CC_ID'
      Origin = 'DOCTO_CC_ID'
    end
    object qryAuxiliarFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object qryAuxiliarNOMBRE_CONCEP: TStringField
      FieldName = 'NOMBRE_CONCEP'
      Origin = 'NOMBRE_CONCEP'
      Size = 30
    end
    object qryAuxiliarFOLIO: TStringField
      FieldName = 'FOLIO'
      Origin = 'FOLIO'
      FixedChar = True
      Size = 9
    end
    object qryAuxiliarDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 200
    end
    object qryAuxiliarIMPORTE_CARGO: TFMTBCDField
      FieldName = 'IMPORTE_CARGO'
      Origin = 'IMPORTE_CARGO'
      Precision = 18
      Size = 2
    end
    object qryAuxiliarIMPORTE_CREDITO: TFMTBCDField
      FieldName = 'IMPORTE_CREDITO'
      Origin = 'IMPORTE_CREDITO'
      Precision = 18
      Size = 2
    end
    object qryAuxiliarSALDO_FIN: TFMTBCDField
      FieldName = 'SALDO_FIN'
      Origin = 'SALDO_FIN'
      Precision = 18
      Size = 2
    end
    object qryAuxiliarMOVTO_ESPECIAL: TStringField
      FieldName = 'MOVTO_ESPECIAL'
      Origin = 'MOVTO_ESPECIAL'
      FixedChar = True
      Size = 1
    end
    object qryAuxiliarCLIENTE_ID_1: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'CLIENTE_ID_1'
      Origin = 'CLIENTE_ID'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliarNOMBRE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 200
    end
    object qryAuxiliarCONTACTO1: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CONTACTO1'
      Origin = 'CONTACTO1'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object qryAuxiliarCONTACTO2: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CONTACTO2'
      Origin = 'CONTACTO2'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object qryAuxiliarESTATUS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ESTATUS'
      Origin = 'ESTATUS'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object qryAuxiliarCAUSA_SUSP: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CAUSA_SUSP'
      Origin = 'CAUSA_SUSP'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object qryAuxiliarFECHA_SUSP: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'FECHA_SUSP'
      Origin = 'FECHA_SUSP'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliarCOBRAR_IMPUESTOS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'COBRAR_IMPUESTOS'
      Origin = 'COBRAR_IMPUESTOS'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object qryAuxiliarRETIENE_IMPUESTOS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'RETIENE_IMPUESTOS'
      Origin = 'RETIENE_IMPUESTOS'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object qryAuxiliarSUJETO_IEPS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'SUJETO_IEPS'
      Origin = 'SUJETO_IEPS'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object qryAuxiliarGENERAR_INTERESES: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'GENERAR_INTERESES'
      Origin = 'GENERAR_INTERESES'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object qryAuxiliarEMITIR_EDOCTA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'EMITIR_EDOCTA'
      Origin = 'EMITIR_EDOCTA'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object qryAuxiliarDIFERIR_CFDI_COBROS: TBooleanField
      AutoGenerateValue = arDefault
      FieldName = 'DIFERIR_CFDI_COBROS'
      Origin = 'DIFERIR_CFDI_COBROS'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliarLIMITE_CREDITO: TFMTBCDField
      AutoGenerateValue = arDefault
      FieldName = 'LIMITE_CREDITO'
      Origin = 'LIMITE_CREDITO'
      ProviderFlags = []
      ReadOnly = True
      Precision = 18
      Size = 2
    end
    object qryAuxiliarMONEDA_ID: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'MONEDA_ID'
      Origin = 'MONEDA_ID'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliarCOND_PAGO_ID: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'COND_PAGO_ID'
      Origin = 'COND_PAGO_ID'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliarTIPO_CLIENTE_ID: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'TIPO_CLIENTE_ID'
      Origin = 'TIPO_CLIENTE_ID'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliarZONA_CLIENTE_ID: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ZONA_CLIENTE_ID'
      Origin = 'ZONA_CLIENTE_ID'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliarCOBRADOR_ID: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'COBRADOR_ID'
      Origin = 'COBRADOR_ID'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliarVENDEDOR_ID: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'VENDEDOR_ID'
      Origin = 'VENDEDOR_ID'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliarNOTAS: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'NOTAS'
      Origin = 'NOTAS'
      ProviderFlags = []
      ReadOnly = True
      BlobType = ftMemo
    end
    object qryAuxiliarCUENTA_CXC: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CUENTA_CXC'
      Origin = 'CUENTA_CXC'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object qryAuxiliarCUENTA_ANTICIPOS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CUENTA_ANTICIPOS'
      Origin = 'CUENTA_ANTICIPOS'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object qryAuxiliarFORMATOS_EMAIL: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'FORMATOS_EMAIL'
      Origin = 'FORMATOS_EMAIL'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object qryAuxiliarRECEPTOR_CFD: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'RECEPTOR_CFD'
      Origin = 'RECEPTOR_CFD'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object qryAuxiliarNUM_PROV_CLIENTE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NUM_PROV_CLIENTE'
      Origin = 'NUM_PROV_CLIENTE'
      ProviderFlags = []
      ReadOnly = True
      Size = 35
    end
    object qryAuxiliarCAMPOS_ADDENDA: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'CAMPOS_ADDENDA'
      Origin = 'CAMPOS_ADDENDA'
      ProviderFlags = []
      ReadOnly = True
      BlobType = ftMemo
    end
    object qryAuxiliarUSUARIO_CREADOR: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'USUARIO_CREADOR'
      Origin = 'USUARIO_CREADOR'
      ProviderFlags = []
      ReadOnly = True
      Size = 31
    end
    object qryAuxiliarFECHA_HORA_CREACION: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'FECHA_HORA_CREACION'
      Origin = 'FECHA_HORA_CREACION'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliarUSUARIO_AUT_CREACION: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'USUARIO_AUT_CREACION'
      Origin = 'USUARIO_AUT_CREACION'
      ProviderFlags = []
      ReadOnly = True
      Size = 31
    end
    object qryAuxiliarUSUARIO_ULT_MODIF: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'USUARIO_ULT_MODIF'
      Origin = 'USUARIO_ULT_MODIF'
      ProviderFlags = []
      ReadOnly = True
      Size = 31
    end
    object qryAuxiliarFECHA_HORA_ULT_MODIF: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'FECHA_HORA_ULT_MODIF'
      Origin = 'FECHA_HORA_ULT_MODIF'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliarUSUARIO_AUT_MODIF: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'USUARIO_AUT_MODIF'
      Origin = 'USUARIO_AUT_MODIF'
      ProviderFlags = []
      ReadOnly = True
      Size = 31
    end
  end
  object rptAuxiliar: TfrxReport
    Version = '6.7.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44089.480742002300000000
    ReportOptions.LastChange = 44102.548048692130000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 442
    Top = 504
    Datasets = <
      item
        DataSet = frxDBDataset4
        DataSetName = 'Auxiliar'
      end
      item
        DataSet = frxDBDataset5
        DataSetName = 'Auxiliar2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 279.400000000000000000
      PaperSize = 1
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 49.133890000000000000
        Top = 18.897650000000000000
        Width = 740.409927000000000000
        object Memo7: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Top = 2.000000000000000000
          Width = 737.008350000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset4
          DataSetName = 'Auxiliar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'HISTORIAL AUXILIAR')
          ParentFont = False
          VAlign = vaCenter
        end
        object SysMemo1: TfrxSysMemoView
          AllowVectorExport = True
          Top = 26.456710000000000000
          Width = 245.669450000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DATE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 737.008350000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset4
          DataSetName = 'Auxiliar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Auxiliar."NOMBRE"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 26.456710000000000000
        Top = 192.756030000000000000
        Width = 740.409927000000000000
        DataSet = frxDBDataset4
        DataSetName = 'Auxiliar'
        RowCount = 0
        object frxDBDataset1FOLIO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Width = 102.047310000000000000
          Height = 26.456710000000000000
          DataField = 'FOLIO'
          DataSet = frxDBDataset4
          DataSetName = 'Auxiliar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Auxiliar."FOLIO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBDataset1FECHA_HORA: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 498.897960000000000000
          Width = 128.504020000000000000
          Height = 26.456710000000000000
          DataSet = frxDBDataset4
          DataSetName = 'Auxiliar'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Auxiliar."FECHA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object HistorialTrasladosCLIENTE1: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 154.960620160000000000
          Height = 26.456710000000000000
          DataSet = frxDBDataset4
          DataSetName = 'Auxiliar'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Auxiliar."IMPORTE_CARGO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 268.346630000000000000
          Width = 226.771690160000000000
          Height = 26.456710000000000000
          DataSet = frxDBDataset4
          DataSetName = 'Auxiliar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Auxiliar."DESCRIPCION"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 41.574830000000000000
        Top = 128.504020000000000000
        Width = 740.409927000000000000
        Condition = 'Auxiliar."CLIENTE_ID"'
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Folio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 22.677180000000000000
          Width = 154.960620160000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Cargo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 22.677180000000000000
          Width = 128.504020000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Fecha')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 268.346630000000000000
          Top = 22.677180000000000000
          Width = 226.771690160000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Descripcion')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Width = 737.008350000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset4
          DataSetName = 'Auxiliar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = clScrollBar
          Memo.UTF8W = (
            'CARGOS')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 34.015770000000000000
        Top = 241.889920000000000000
        Width = 740.409927000000000000
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 41.574830000000000000
        Top = 298.582870000000000000
        Width = 740.409927000000000000
        Condition = 'Auxiliar2."CLIENTE_ID"'
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Folio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 22.677180000000000000
          Width = 154.960620160000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Cobro')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 22.677180000000000000
          Width = 128.504020000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Fecha')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 268.346630000000000000
          Top = 22.677180000000000000
          Width = 226.771690160000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Descripcion')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Width = 737.008350000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset4
          DataSetName = 'Auxiliar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = clScrollBar
          Memo.UTF8W = (
            'COBROS')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 34.015770000000000000
        Top = 411.968770000000000000
        Width = 740.409927000000000000
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 26.456710000000000000
        Top = 362.834880000000000000
        Width = 740.409927000000000000
        DataSet = frxDBDataset5
        DataSetName = 'Auxiliar2'
        RowCount = 0
        object Memo11: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Width = 102.047310000000000000
          Height = 26.456710000000000000
          DataSet = frxDBDataset4
          DataSetName = 'Auxiliar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Auxiliar2."FOLIO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 498.897960000000000000
          Width = 128.504020000000000000
          Height = 26.456710000000000000
          DataSet = frxDBDataset4
          DataSetName = 'Auxiliar'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Auxiliar2."FECHA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 154.960620160000000000
          Height = 26.456710000000000000
          DataSet = frxDBDataset4
          DataSetName = 'Auxiliar'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Auxiliar2."IMPORTE_CREDITO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 268.346630000000000000
          Width = 226.771690160000000000
          Height = 26.456710000000000000
          DataSet = frxDBDataset4
          DataSetName = 'Auxiliar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Auxiliar2."DESCRIPCION"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object dsAuxiliar: TDataSource
    DataSet = qryAuxiliar
    Left = 402
    Top = 504
  end
  object frxDBDataset4: TfrxDBDataset
    UserName = 'Auxiliar'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CLIENTE_ID=CLIENTE_ID'
      'DOCTO_CC_ID=DOCTO_CC_ID'
      'FECHA=FECHA'
      'NOMBRE_CONCEP=NOMBRE_CONCEP'
      'FOLIO=FOLIO'
      'DESCRIPCION=DESCRIPCION'
      'IMPORTE_CARGO=IMPORTE_CARGO'
      'IMPORTE_CREDITO=IMPORTE_CREDITO'
      'SALDO_FIN=SALDO_FIN'
      'MOVTO_ESPECIAL=MOVTO_ESPECIAL'
      'CLIENTE_ID_1=CLIENTE_ID_1'
      'NOMBRE=NOMBRE'
      'CONTACTO1=CONTACTO1'
      'CONTACTO2=CONTACTO2'
      'ESTATUS=ESTATUS'
      'CAUSA_SUSP=CAUSA_SUSP'
      'FECHA_SUSP=FECHA_SUSP'
      'COBRAR_IMPUESTOS=COBRAR_IMPUESTOS'
      'RETIENE_IMPUESTOS=RETIENE_IMPUESTOS'
      'SUJETO_IEPS=SUJETO_IEPS'
      'GENERAR_INTERESES=GENERAR_INTERESES'
      'EMITIR_EDOCTA=EMITIR_EDOCTA'
      'DIFERIR_CFDI_COBROS=DIFERIR_CFDI_COBROS'
      'LIMITE_CREDITO=LIMITE_CREDITO'
      'MONEDA_ID=MONEDA_ID'
      'COND_PAGO_ID=COND_PAGO_ID'
      'TIPO_CLIENTE_ID=TIPO_CLIENTE_ID'
      'ZONA_CLIENTE_ID=ZONA_CLIENTE_ID'
      'COBRADOR_ID=COBRADOR_ID'
      'VENDEDOR_ID=VENDEDOR_ID'
      'NOTAS=NOTAS'
      'CUENTA_CXC=CUENTA_CXC'
      'CUENTA_ANTICIPOS=CUENTA_ANTICIPOS'
      'FORMATOS_EMAIL=FORMATOS_EMAIL'
      'RECEPTOR_CFD=RECEPTOR_CFD'
      'NUM_PROV_CLIENTE=NUM_PROV_CLIENTE'
      'CAMPOS_ADDENDA=CAMPOS_ADDENDA'
      'USUARIO_CREADOR=USUARIO_CREADOR'
      'FECHA_HORA_CREACION=FECHA_HORA_CREACION'
      'USUARIO_AUT_CREACION=USUARIO_AUT_CREACION'
      'USUARIO_ULT_MODIF=USUARIO_ULT_MODIF'
      'FECHA_HORA_ULT_MODIF=FECHA_HORA_ULT_MODIF'
      'USUARIO_AUT_MODIF=USUARIO_AUT_MODIF')
    DataSource = dsAuxiliar
    BCDToCurrency = False
    Left = 490
    Top = 504
  end
  object qryAuxiliar2: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select * from auxiliar_cliente(:cli,'#39'01/01/2020'#39',current_date,0,' +
        #39'N'#39') ac'
      'join clientes c on c.cliente_id=ac.cliente_id'
      
        'where upper(nombre_concep) like '#39'%PREDIAL%'#39' and (select naturale' +
        'za from conceptos_cc where nombre=ac.nombre_concep)='#39'R'#39)
    Left = 354
    Top = 552
    ParamData = <
      item
        Name = 'CLI'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object qryAuxiliar2CLIENTE_ID: TIntegerField
      FieldName = 'CLIENTE_ID'
      Origin = 'CLIENTE_ID'
    end
    object qryAuxiliar2DOCTO_CC_ID: TIntegerField
      FieldName = 'DOCTO_CC_ID'
      Origin = 'DOCTO_CC_ID'
    end
    object qryAuxiliar2FECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object qryAuxiliar2NOMBRE_CONCEP: TStringField
      FieldName = 'NOMBRE_CONCEP'
      Origin = 'NOMBRE_CONCEP'
      Size = 30
    end
    object qryAuxiliar2FOLIO: TStringField
      FieldName = 'FOLIO'
      Origin = 'FOLIO'
      FixedChar = True
      Size = 9
    end
    object qryAuxiliar2DESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 200
    end
    object qryAuxiliar2IMPORTE_CARGO: TFMTBCDField
      FieldName = 'IMPORTE_CARGO'
      Origin = 'IMPORTE_CARGO'
      Precision = 18
      Size = 2
    end
    object qryAuxiliar2IMPORTE_CREDITO: TFMTBCDField
      FieldName = 'IMPORTE_CREDITO'
      Origin = 'IMPORTE_CREDITO'
      Precision = 18
      Size = 2
    end
    object qryAuxiliar2SALDO_FIN: TFMTBCDField
      FieldName = 'SALDO_FIN'
      Origin = 'SALDO_FIN'
      Precision = 18
      Size = 2
    end
    object qryAuxiliar2MOVTO_ESPECIAL: TStringField
      FieldName = 'MOVTO_ESPECIAL'
      Origin = 'MOVTO_ESPECIAL'
      FixedChar = True
      Size = 1
    end
    object qryAuxiliar2CLIENTE_ID_1: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'CLIENTE_ID_1'
      Origin = 'CLIENTE_ID'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliar2NOMBRE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 200
    end
    object qryAuxiliar2CONTACTO1: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CONTACTO1'
      Origin = 'CONTACTO1'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object qryAuxiliar2CONTACTO2: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CONTACTO2'
      Origin = 'CONTACTO2'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object qryAuxiliar2ESTATUS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ESTATUS'
      Origin = 'ESTATUS'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object qryAuxiliar2CAUSA_SUSP: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CAUSA_SUSP'
      Origin = 'CAUSA_SUSP'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object qryAuxiliar2FECHA_SUSP: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'FECHA_SUSP'
      Origin = 'FECHA_SUSP'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliar2COBRAR_IMPUESTOS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'COBRAR_IMPUESTOS'
      Origin = 'COBRAR_IMPUESTOS'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object qryAuxiliar2RETIENE_IMPUESTOS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'RETIENE_IMPUESTOS'
      Origin = 'RETIENE_IMPUESTOS'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object qryAuxiliar2SUJETO_IEPS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'SUJETO_IEPS'
      Origin = 'SUJETO_IEPS'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object qryAuxiliar2GENERAR_INTERESES: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'GENERAR_INTERESES'
      Origin = 'GENERAR_INTERESES'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object qryAuxiliar2EMITIR_EDOCTA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'EMITIR_EDOCTA'
      Origin = 'EMITIR_EDOCTA'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object qryAuxiliar2DIFERIR_CFDI_COBROS: TBooleanField
      AutoGenerateValue = arDefault
      FieldName = 'DIFERIR_CFDI_COBROS'
      Origin = 'DIFERIR_CFDI_COBROS'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliar2LIMITE_CREDITO: TFMTBCDField
      AutoGenerateValue = arDefault
      FieldName = 'LIMITE_CREDITO'
      Origin = 'LIMITE_CREDITO'
      ProviderFlags = []
      ReadOnly = True
      Precision = 18
      Size = 2
    end
    object qryAuxiliar2MONEDA_ID: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'MONEDA_ID'
      Origin = 'MONEDA_ID'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliar2COND_PAGO_ID: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'COND_PAGO_ID'
      Origin = 'COND_PAGO_ID'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliar2TIPO_CLIENTE_ID: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'TIPO_CLIENTE_ID'
      Origin = 'TIPO_CLIENTE_ID'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliar2ZONA_CLIENTE_ID: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ZONA_CLIENTE_ID'
      Origin = 'ZONA_CLIENTE_ID'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliar2COBRADOR_ID: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'COBRADOR_ID'
      Origin = 'COBRADOR_ID'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliar2VENDEDOR_ID: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'VENDEDOR_ID'
      Origin = 'VENDEDOR_ID'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliar2NOTAS: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'NOTAS'
      Origin = 'NOTAS'
      ProviderFlags = []
      ReadOnly = True
      BlobType = ftMemo
    end
    object qryAuxiliar2CUENTA_CXC: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CUENTA_CXC'
      Origin = 'CUENTA_CXC'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object qryAuxiliar2CUENTA_ANTICIPOS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CUENTA_ANTICIPOS'
      Origin = 'CUENTA_ANTICIPOS'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object qryAuxiliar2FORMATOS_EMAIL: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'FORMATOS_EMAIL'
      Origin = 'FORMATOS_EMAIL'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object qryAuxiliar2RECEPTOR_CFD: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'RECEPTOR_CFD'
      Origin = 'RECEPTOR_CFD'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object qryAuxiliar2NUM_PROV_CLIENTE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NUM_PROV_CLIENTE'
      Origin = 'NUM_PROV_CLIENTE'
      ProviderFlags = []
      ReadOnly = True
      Size = 35
    end
    object qryAuxiliar2CAMPOS_ADDENDA: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'CAMPOS_ADDENDA'
      Origin = 'CAMPOS_ADDENDA'
      ProviderFlags = []
      ReadOnly = True
      BlobType = ftMemo
    end
    object qryAuxiliar2USUARIO_CREADOR: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'USUARIO_CREADOR'
      Origin = 'USUARIO_CREADOR'
      ProviderFlags = []
      ReadOnly = True
      Size = 31
    end
    object qryAuxiliar2FECHA_HORA_CREACION: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'FECHA_HORA_CREACION'
      Origin = 'FECHA_HORA_CREACION'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliar2USUARIO_AUT_CREACION: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'USUARIO_AUT_CREACION'
      Origin = 'USUARIO_AUT_CREACION'
      ProviderFlags = []
      ReadOnly = True
      Size = 31
    end
    object qryAuxiliar2USUARIO_ULT_MODIF: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'USUARIO_ULT_MODIF'
      Origin = 'USUARIO_ULT_MODIF'
      ProviderFlags = []
      ReadOnly = True
      Size = 31
    end
    object qryAuxiliar2FECHA_HORA_ULT_MODIF: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'FECHA_HORA_ULT_MODIF'
      Origin = 'FECHA_HORA_ULT_MODIF'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryAuxiliar2USUARIO_AUT_MODIF: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'USUARIO_AUT_MODIF'
      Origin = 'USUARIO_AUT_MODIF'
      ProviderFlags = []
      ReadOnly = True
      Size = 31
    end
  end
  object dsAuxiliar2: TDataSource
    DataSet = qryAuxiliar2
    Left = 402
    Top = 552
  end
  object frxDBDataset5: TfrxDBDataset
    UserName = 'Auxiliar2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CLIENTE_ID=CLIENTE_ID'
      'DOCTO_CC_ID=DOCTO_CC_ID'
      'FECHA=FECHA'
      'NOMBRE_CONCEP=NOMBRE_CONCEP'
      'FOLIO=FOLIO'
      'DESCRIPCION=DESCRIPCION'
      'IMPORTE_CARGO=IMPORTE_CARGO'
      'IMPORTE_CREDITO=IMPORTE_CREDITO'
      'SALDO_FIN=SALDO_FIN'
      'MOVTO_ESPECIAL=MOVTO_ESPECIAL'
      'CLIENTE_ID_1=CLIENTE_ID_1'
      'NOMBRE=NOMBRE'
      'CONTACTO1=CONTACTO1'
      'CONTACTO2=CONTACTO2'
      'ESTATUS=ESTATUS'
      'CAUSA_SUSP=CAUSA_SUSP'
      'FECHA_SUSP=FECHA_SUSP'
      'COBRAR_IMPUESTOS=COBRAR_IMPUESTOS'
      'RETIENE_IMPUESTOS=RETIENE_IMPUESTOS'
      'SUJETO_IEPS=SUJETO_IEPS'
      'GENERAR_INTERESES=GENERAR_INTERESES'
      'EMITIR_EDOCTA=EMITIR_EDOCTA'
      'DIFERIR_CFDI_COBROS=DIFERIR_CFDI_COBROS'
      'LIMITE_CREDITO=LIMITE_CREDITO'
      'MONEDA_ID=MONEDA_ID'
      'COND_PAGO_ID=COND_PAGO_ID'
      'TIPO_CLIENTE_ID=TIPO_CLIENTE_ID'
      'ZONA_CLIENTE_ID=ZONA_CLIENTE_ID'
      'COBRADOR_ID=COBRADOR_ID'
      'VENDEDOR_ID=VENDEDOR_ID'
      'NOTAS=NOTAS'
      'CUENTA_CXC=CUENTA_CXC'
      'CUENTA_ANTICIPOS=CUENTA_ANTICIPOS'
      'FORMATOS_EMAIL=FORMATOS_EMAIL'
      'RECEPTOR_CFD=RECEPTOR_CFD'
      'NUM_PROV_CLIENTE=NUM_PROV_CLIENTE'
      'CAMPOS_ADDENDA=CAMPOS_ADDENDA'
      'USUARIO_CREADOR=USUARIO_CREADOR'
      'FECHA_HORA_CREACION=FECHA_HORA_CREACION'
      'USUARIO_AUT_CREACION=USUARIO_AUT_CREACION'
      'USUARIO_ULT_MODIF=USUARIO_ULT_MODIF'
      'FECHA_HORA_ULT_MODIF=FECHA_HORA_ULT_MODIF'
      'USUARIO_AUT_MODIF=USUARIO_AUT_MODIF')
    DataSource = dsAuxiliar2
    BCDToCurrency = False
    Left = 490
    Top = 552
  end
  object rpConsolidado: TfrxReport
    Version = '6.7.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44085.402654224500000000
    ReportOptions.LastChange = 44116.433980057870000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 336
    Top = 448
    Datasets = <
      item
        DataSet = dsConsolidado
        DataSetName = 'DatasetConsolidado'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 355.600000000000000000
      PaperHeight = 215.900000000000000000
      PaperSize = 5
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object Shape24: TfrxShapeView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 64.252010000000000000
        Width = 944.882272990000000000
        Height = 18.758696690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Shape2: TfrxShapeView
        AllowVectorExport = True
        Left = 90.693230120000000000
        Top = 26.373338010000000000
        Width = 1080.945352990000000000
        Height = 18.758696690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Shape5: TfrxShapeView
        AllowVectorExport = True
        Left = 90.693230120000000000
        Top = 45.132034710000000000
        Width = 136.062852990000000000
        Height = 18.758696690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Shape6: TfrxShapeView
        AllowVectorExport = True
        Left = 90.693230120000000000
        Top = 63.890731400000000000
        Width = 136.062852990000000000
        Height = 18.758696690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Shape9: TfrxShapeView
        AllowVectorExport = True
        Left = 226.862933400000000000
        Top = 63.890731400000000000
        Width = 612.283632990000000000
        Height = 18.758696690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo1: TfrxMemoView
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 26.456710000000000000
        Width = 1080.945352990000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Manitoba Kolonie Cd. Cuauht'#233'moc, Chihuahua. Mx.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape13: TfrxShapeView
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 124.724490000000000000
        Width = 1080.945352990000000000
        Height = 427.086614170000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo42: TfrxMemoView
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 47.354360000000000000
        Width = 136.063048270000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Gebortsdatum')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo49: TfrxMemoView
        AllowVectorExport = True
        Left = 905.307670000000000000
        Top = 66.252010000000000000
        Width = 45.354328270000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          'Tel.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape11: TfrxShapeView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 45.354360000000000000
        Width = 944.882272990000000000
        Height = 18.758696690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo46: TfrxMemoView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 47.354360000000000000
        Width = 612.283828270000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Name von den Eigent'#252'mer')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo43: TfrxMemoView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 66.252010000000000000
        Width = 612.283828270000000000
        Height = 15.118120000000000000
        DataField = 'NOMBRE'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[DatasetConsolidado."NOMBRE"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape7: TfrxShapeView
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 83.149660000000000000
        Width = 136.062852990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo2: TfrxMemoView
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 98.267780000000000000
        Width = 136.063048270000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Datum')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape12: TfrxShapeView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 83.149660000000000000
        Width = 56.692722990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo3: TfrxMemoView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 98.267780000000000000
        Width = 56.692918270000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Campo')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape14: TfrxShapeView
        AllowVectorExport = True
        Left = 283.464750000000000000
        Top = 83.149660000000000000
        Width = 173.858152990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo4: TfrxMemoView
        AllowVectorExport = True
        Left = 283.464750000000000000
        Top = 83.149660000000000000
        Width = 173.858348270000000000
        Height = 41.574830000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Calle/ Av./ Fracc./'
          'Residencial/ Corredor C.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape15: TfrxShapeView
        AllowVectorExport = True
        Left = 457.323130000000000000
        Top = 83.149660000000000000
        Width = 90.708520890000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo6: TfrxMemoView
        AllowVectorExport = True
        Left = 457.323130000000000000
        Top = 98.267780000000000000
        Width = 90.708688270000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Municipio')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape16: TfrxShapeView
        AllowVectorExport = True
        Left = 548.031850000000000000
        Top = 83.149660000000000000
        Width = 86.928962990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo7: TfrxMemoView
        AllowVectorExport = True
        Left = 548.031850000000000000
        Top = 83.149660000000000000
        Width = 86.929158270000000000
        Height = 41.574830000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Lote/ Temporal/'
          'Riego')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape17: TfrxShapeView
        AllowVectorExport = True
        Left = 634.961040000000000000
        Top = 83.149660000000000000
        Width = 143.621912990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo8: TfrxMemoView
        AllowVectorExport = True
        Left = 634.961040000000000000
        Top = 83.149660000000000000
        Width = 143.622108270000000000
        Height = 41.574830000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Folio No.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape18: TfrxShapeView
        AllowVectorExport = True
        Left = 778.583180000000000000
        Top = 83.149660000000000000
        Width = 60.472252990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo9: TfrxMemoView
        AllowVectorExport = True
        Left = 778.583180000000000000
        Top = 83.149660000000000000
        Width = 60.472448270000000000
        Height = 41.574830000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Zona')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape19: TfrxShapeView
        AllowVectorExport = True
        Left = 839.055660000000000000
        Top = 83.149660000000000000
        Width = 113.385672990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Shape20: TfrxShapeView
        AllowVectorExport = True
        Left = 952.441560000000000000
        Top = 83.149660000000000000
        Width = 98.267552990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Shape21: TfrxShapeView
        AllowVectorExport = True
        Left = 1050.709340000000000000
        Top = 83.149660000000000000
        Width = 56.692722990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Shape22: TfrxShapeView
        AllowVectorExport = True
        Left = 1107.402290000000000000
        Top = 83.149660000000000000
        Width = 64.251782990000000000
        Height = 41.435876690000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object Memo10: TfrxMemoView
        AllowVectorExport = True
        Left = 839.055660000000000000
        Top = 83.149660000000000000
        Width = 113.385868270000000000
        Height = 41.574830000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Este Mts.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo11: TfrxMemoView
        AllowVectorExport = True
        Left = 952.441560000000000000
        Top = 83.149660000000000000
        Width = 98.267748270000000000
        Height = 41.574830000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Norte Mts.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo12: TfrxMemoView
        AllowVectorExport = True
        Left = 1050.709340000000000000
        Top = 83.149660000000000000
        Width = 56.692918270000000000
        Height = 41.574830000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Hoff/Lot'
          'Nr.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo13: TfrxMemoView
        AllowVectorExport = True
        Left = 1107.402290000000000000
        Top = 83.149660000000000000
        Width = 64.251978270000000000
        Height = 41.574830000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Acres')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape23: TfrxShapeView
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 566.929500000000000000
        Width = 1080.945352990000000000
        Height = 151.180924170000000000
        Fill.BackColor = clWhite
        Frame.Typ = []
      end
      object DatasetConsolidadoRFC_CURP: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 64.252010000000000000
        Width = 136.063080000000000000
        Height = 18.897650000000000000
        DataField = 'RFC_CURP'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[DatasetConsolidado."RFC_CURP"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object DatasetConsolidadoTELEFONO1: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 952.441560000000000000
        Top = 64.252010000000000000
        Width = 215.433210000000000000
        Height = 18.897650000000000000
        DataField = 'TELEFONO1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[DatasetConsolidado."TELEFONO1"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Subreport1: TfrxSubreport
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 124.724490000000000000
        Width = 1080.945580000000000000
        Height = 427.086890000000000000
        Page = rpConsolidado.Page2
      end
      object Memo16: TfrxMemoView
        AllowVectorExport = True
        Left = 98.267780000000000000
        Top = 578.268090000000000000
        Width = 56.692918270000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'NOTA:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo17: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 158.740260000000000000
        Top = 578.268090000000000000
        Width = 997.795920000000000000
        Height = 128.504020000000000000
        DataField = 'NOTAS'
        Frame.Typ = []
        Memo.UTF8W = (
          '[DatasetConsolidado."NOTAS"]')
      end
    end
    object Page2: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 355.600000000000000000
      PaperHeight = 215.900000000000000000
      PaperSize = 5
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 18.897650000000000000
        Width = 1268.410268000000000000
        DataSet = dsConsolidado
        DataSetName = 'DatasetConsolidado'
        RowCount = 0
        object DatasetConsolidadoCAMPO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 136.063080000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataField = 'CAMPO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[DatasetConsolidado."CAMPO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoDIRECCION: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 192.756030000000000000
          Width = 173.858267720000000000
          Height = 18.897650000000000000
          DataField = 'DIRECCION'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[DatasetConsolidado."DIRECCION"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoMUNICIPIO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 366.614410000000000000
          Width = 90.708661420000000000
          Height = 18.897650000000000000
          DataField = 'MUNICIPIO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[DatasetConsolidado."MUNICIPIO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoTIPO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 457.323130000000000000
          Width = 86.929133860000000000
          Height = 18.897650000000000000
          DataField = 'TIPO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[DatasetConsolidado."TIPO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoFOLIO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 544.252320000000000000
          Width = 143.622047240000000000
          Height = 18.897650000000000000
          DataField = 'FOLIO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[DatasetConsolidado."FOLIO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoZONA: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 687.874460000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'ZONA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[DatasetConsolidado."ZONA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoMTS_ESTE: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 748.346940000000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DataField = 'MTS_ESTE'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[DatasetConsolidado."MTS_ESTE"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoMTS_NORTE: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 861.732840000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DataField = 'MTS_NORTE'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[DatasetConsolidado."MTS_NORTE"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoNUMERO: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 960.000620000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataField = 'NUMERO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[DatasetConsolidado."NUMERO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoACRES: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 1016.693570000000000000
          Width = 64.251968500000000000
          Height = 18.897650000000000000
          DataField = 'ACRES'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[DatasetConsolidado."ACRES"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object DatasetConsolidadoFECHA: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          DataField = 'FECHA'
          DisplayFormat.FormatStr = 'dd mmm yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[DatasetConsolidado."FECHA"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object dssConsolidado: TDataSource
    DataSet = qryConsolidado
    Left = 512
    Top = 360
  end
  object qryConsolidado: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select s.*,(select nombre from clientes where cliente_id=:client' +
        'e)'
      ',(select rfc_curp from dirs_clientes where cliente_id=:cliente)'
      ',(select telefono1 from dirs_clientes where cliente_id=:cliente)'
      
        ',(select notas from clientes where cliente_id=:cliente) from sic' +
        '_predial_terrenos s where cliente_id=:cliente order by folio')
    Left = 512
    Top = 400
    ParamData = <
      item
        Name = 'CLIENTE'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object qryConsolidadoID_TERRENO: TIntegerField
      FieldName = 'ID_TERRENO'
      Origin = 'ID_TERRENO'
    end
    object qryConsolidadoTIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Size = 50
    end
    object qryConsolidadoCLIENTE_ID: TIntegerField
      FieldName = 'CLIENTE_ID'
      Origin = 'CLIENTE_ID'
    end
    object qryConsolidadoFOLIO: TStringField
      FieldName = 'FOLIO'
      Origin = 'FOLIO'
    end
    object qryConsolidadoZONA: TStringField
      FieldName = 'ZONA'
      Origin = 'ZONA'
      Size = 50
    end
    object qryConsolidadoMTS_ESTE: TFMTBCDField
      FieldName = 'MTS_ESTE'
      Origin = 'MTS_ESTE'
      Precision = 18
      Size = 2
    end
    object qryConsolidadoMTS_NORTE: TFMTBCDField
      FieldName = 'MTS_NORTE'
      Origin = 'MTS_NORTE'
      Precision = 18
      Size = 2
    end
    object qryConsolidadoNUMERO: TIntegerField
      FieldName = 'NUMERO'
      Origin = 'NUMERO'
    end
    object qryConsolidadoMETROS: TFMTBCDField
      FieldName = 'METROS'
      Origin = 'METROS'
      Precision = 18
      Size = 2
    end
    object qryConsolidadoACRES: TBCDField
      FieldName = 'ACRES'
      Origin = 'ACRES'
      Precision = 18
    end
    object qryConsolidadoCAMPO: TStringField
      FieldName = 'CAMPO'
      Origin = 'CAMPO'
      Size = 50
    end
    object qryConsolidadoDIRECCION: TStringField
      FieldName = 'DIRECCION'
      Origin = 'DIRECCION'
      Size = 250
    end
    object qryConsolidadoMUNICIPIO: TStringField
      FieldName = 'MUNICIPIO'
      Origin = 'MUNICIPIO'
      Size = 100
    end
    object qryConsolidadoTRASPASO: TStringField
      FieldName = 'TRASPASO'
      Origin = 'TRASPASO'
      FixedChar = True
      Size = 1
    end
    object qryConsolidadoNOMBRE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 200
    end
    object qryConsolidadoRFC_CURP: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'RFC_CURP'
      Origin = 'RFC_CURP'
      ProviderFlags = []
      ReadOnly = True
      Size = 18
    end
    object qryConsolidadoTELEFONO1: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      ProviderFlags = []
      ReadOnly = True
      Size = 35
    end
    object qryConsolidadoFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object qryConsolidadoNOTAS: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'NOTAS'
      Origin = 'NOTAS'
      ProviderFlags = []
      ReadOnly = True
      BlobType = ftMemo
    end
  end
  object dsConsolidado: TfrxDBDataset
    UserName = 'DatasetConsolidado'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID_TERRENO=ID_TERRENO'
      'TIPO=TIPO'
      'CLIENTE_ID=CLIENTE_ID'
      'FOLIO=FOLIO'
      'ZONA=ZONA'
      'MTS_ESTE=MTS_ESTE'
      'MTS_NORTE=MTS_NORTE'
      'NUMERO=NUMERO'
      'METROS=METROS'
      'ACRES=ACRES'
      'CAMPO=CAMPO'
      'DIRECCION=DIRECCION'
      'MUNICIPIO=MUNICIPIO'
      'TRASPASO=TRASPASO'
      'NOMBRE=NOMBRE'
      'RFC_CURP=RFC_CURP'
      'TELEFONO1=TELEFONO1'
      'FECHA=FECHA'
      'NOTAS=NOTAS')
    DataSource = dssConsolidado
    BCDToCurrency = False
    Left = 264
    Top = 400
  end
  object frxReport1: TfrxReport
    Version = '6.7.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44089.480742002300000000
    ReportOptions.LastChange = 44099.739369409700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 360
    Top = 352
    Datasets = <
      item
        DataSet = Principal.frxDBDataset1
        DataSetName = 'frxDBDataset1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 279.400000000000000000
      PaperSize = 1
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object Shape1: TfrxShapeView
        AllowVectorExport = True
        Left = 37.795300000000000000
        Top = 37.795300000000000000
        Width = 680.315400000000000000
        Height = 298.582870000000000000
        Frame.Typ = []
      end
      object Shape4: TfrxShapeView
        AllowVectorExport = True
        Left = 37.795300000000000000
        Top = 37.795300000000000000
        Width = 680.315400000000000000
        Height = 128.504020000000000000
        Fill.BackColor = 14211288
        Frame.Typ = []
      end
      object Shape2: TfrxShapeView
        AllowVectorExport = True
        Left = 37.795300000000000000
        Top = 37.795300000000000000
        Width = 680.315400000000000000
        Height = 60.472480000000000000
        Frame.Typ = []
      end
      object Memo1: TfrxMemoView
        AllowVectorExport = True
        Left = 37.795300000000000000
        Top = 45.354360000000000000
        Width = 680.315400000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'COLONIA MANITOBA DE CD.CUAUHTEMOC., CHIH. MX.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo2: TfrxMemoView
        AllowVectorExport = True
        Left = 37.795300000000000000
        Top = 64.252010000000000000
        Width = 555.590910000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Campo 6 1/2 B Casa No. 2122 Cd. Cuauhtemoc, Chih.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo3: TfrxMemoView
        AllowVectorExport = True
        Left = 37.795300000000000000
        Top = 79.370130000000000000
        Width = 151.181200000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          'CORREO:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo4: TfrxMemoView
        AllowVectorExport = True
        Left = 188.976500000000000000
        Top = 79.370130000000000000
        Width = 151.181200000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsUnderline]
        Frame.Typ = []
        Memo.UTF8W = (
          'c.mc-15@hotmail.com')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo5: TfrxMemoView
        AllowVectorExport = True
        Left = 532.913730000000000000
        Top = 79.370130000000000000
        Width = 147.401670000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          'RECIBO DE PREDIAL')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape3: TfrxShapeView
        AllowVectorExport = True
        Left = 37.795300000000000000
        Top = 98.267780000000000000
        Width = 680.315400000000000000
        Height = 37.795300000000000000
        Frame.Typ = []
      end
      object Memo6: TfrxMemoView
        AllowVectorExport = True
        Left = 37.795300000000000000
        Top = 105.826840000000000000
        Width = 98.267780000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Nombre:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Line1: TfrxLineView
        AllowVectorExport = True
        Left = 136.063080000000000000
        Top = 98.267780000000000000
        Height = 37.795300000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
      end
      object Line2: TfrxLineView
        AllowVectorExport = True
        Left = 340.157700000000000000
        Top = 98.267780000000000000
        Height = 52.913420000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
      end
      object Line3: TfrxLineView
        AllowVectorExport = True
        Left = 415.748300000000000000
        Top = 98.267780000000000000
        Height = 52.913420000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
      end
      object Line4: TfrxLineView
        AllowVectorExport = True
        Left = 532.913730000000000000
        Top = 98.267780000000000000
        Height = 52.913420000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
      end
      object Line5: TfrxLineView
        AllowVectorExport = True
        Left = 532.913730000000000000
        Top = 113.385900000000000000
        Width = 185.196970000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
      end
      object Line6: TfrxLineView
        AllowVectorExport = True
        Left = 37.795300000000000000
        Top = 151.181200000000000000
        Width = 680.315400000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
      end
      object Line7: TfrxLineView
        AllowVectorExport = True
        Left = 37.795300000000000000
        Top = 185.196970000000000000
        Width = 680.315400000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
      end
      object Line8: TfrxLineView
        AllowVectorExport = True
        Left = 37.795300000000000000
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
      end
      object Line9: TfrxLineView
        AllowVectorExport = True
        Left = 37.795300000000000000
        Top = 215.433210000000000000
        Width = 680.315400000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
      end
      object Memo7: TfrxMemoView
        AllowVectorExport = True
        Left = 272.126160000000000000
        Top = 264.567100000000000000
        Width = 30.236240000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Firma:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Line10: TfrxLineView
        AllowVectorExport = True
        Left = 306.141930000000000000
        Top = 283.464750000000000000
        Width = 185.196970000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
      end
      object Memo8: TfrxMemoView
        AllowVectorExport = True
        Left = 306.141930000000000000
        Top = 287.244280000000000000
        Width = 185.196970000000000000
        Height = 30.236240000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Chiro Knelsen K.'
          'TEL. 625-118-3741')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo9: TfrxMemoView
        AllowVectorExport = True
        Left = 45.354360000000000000
        Top = 166.299320000000000000
        Width = 139.842610000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'IMPUESTO PREDIAL DEL A'#209'O')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo10: TfrxMemoView
        AllowVectorExport = True
        Left = 532.913730000000000000
        Top = 98.267780000000000000
        Width = 185.196970000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Dia    Mes    A'#241'o')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo11: TfrxMemoView
        AllowVectorExport = True
        Left = 340.157700000000000000
        Top = 136.063080000000000000
        Width = 75.590600000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          'No. Casa:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo12: TfrxMemoView
        AllowVectorExport = True
        Left = 415.748300000000000000
        Top = 98.267780000000000000
        Width = 117.165430000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'R.F.C')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo13: TfrxMemoView
        AllowVectorExport = True
        Left = 340.157700000000000000
        Top = 98.267780000000000000
        Width = 75.590600000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Codigo')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo14: TfrxMemoView
        AllowVectorExport = True
        Left = 532.913730000000000000
        Top = 136.063080000000000000
        Width = 71.811070000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'TEL:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo15: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 136.063080000000000000
        Width = 75.590600000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          'Km:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Line11: TfrxLineView
        AllowVectorExport = True
        Left = 37.795300000000000000
        Top = 166.299320000000000000
        Width = 680.315400000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
      end
      object Line12: TfrxLineView
        AllowVectorExport = True
        Left = 480.000310000000000000
        Top = 151.181200000000000000
        Height = 64.252010000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
      end
      object Memo16: TfrxMemoView
        AllowVectorExport = True
        Left = 185.196970000000000000
        Top = 166.299320000000000000
        Width = 98.267780000000000000
        Height = 18.897650000000000000
        DataField = 'ANO'
        DataSet = Principal.frxDBDataset1
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDBDataset1."ANO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo17: TfrxMemoView
        AllowVectorExport = True
        Left = 631.181510000000000000
        Top = 151.181200000000000000
        Width = 86.929190000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Importe')
        ParentFont = False
        VAlign = vaCenter
      end
      object Line13: TfrxLineView
        AllowVectorExport = True
        Left = 631.181510000000000000
        Top = 166.299320000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
      end
      object Memo18: TfrxMemoView
        AllowVectorExport = True
        Left = 480.000310000000000000
        Top = 185.196970000000000000
        Width = 143.622140000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '10% Descuento')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo19: TfrxMemoView
        AllowVectorExport = True
        Left = 480.000310000000000000
        Top = 200.315090000000000000
        Width = 143.622140000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          'Total A Pagar')
        ParentFont = False
        VAlign = vaCenter
      end
      object frxDBDataset1RFC_CURP: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 415.748300000000000000
        Top = 113.385900000000000000
        Width = 117.165430000000000000
        Height = 22.677180000000000000
        DataField = 'RFC_CURP'
        DataSet = Principal.frxDBDataset1
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDBDataset1."RFC_CURP"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object frxDBDataset1NOMBRE: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 136.063080000000000000
        Top = 98.267780000000000000
        Width = 204.094620000000000000
        Height = 37.795300000000000000
        DataField = 'NOMBRE'
        DataSet = Principal.frxDBDataset1
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDBDataset1."NOMBRE"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object frxDBDataset1FECHA: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 532.913730000000000000
        Top = 113.385900000000000000
        Width = 185.196970000000000000
        Height = 22.677180000000000000
        DataField = 'FECHA'
        DataSet = Principal.frxDBDataset1
        DataSetName = 'frxDBDataset1'
        DisplayFormat.FormatStr = 'dd mmm yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDBDataset1."FECHA"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object frxDBDataset1TELEFONO1: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 136.063080000000000000
        Width = 113.385900000000000000
        Height = 15.118120000000000000
        DataField = 'TELEFONO1'
        DataSet = Principal.frxDBDataset1
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDBDataset1."TELEFONO1"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object frxDBDataset1CALLE: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 200.315090000000000000
        Top = 136.063080000000000000
        Width = 139.842610000000000000
        Height = 15.118120000000000000
        DataField = 'CALLE'
        DataSet = Principal.frxDBDataset1
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDBDataset1."CALLE"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo20: TfrxMemoView
        AllowVectorExport = True
        Left = 154.960730000000000000
        Top = 151.181200000000000000
        Width = 86.929190000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Descripcion')
        ParentFont = False
        VAlign = vaCenter
      end
      object frxDBDataset1IMPORTE_COBRO: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 631.181510000000000000
        Top = 166.299320000000000000
        Width = 86.929190000000000000
        Height = 18.897650000000000000
        DataField = 'IMPORTE_COBRO'
        DataSet = Principal.frxDBDataset1
        DataSetName = 'frxDBDataset1'
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDBDataset1."IMPORTE_COBRO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object SysMemo1: TfrxSysMemoView
        AllowVectorExport = True
        Left = 631.181510000000000000
        Top = 185.196970000000000000
        Width = 86.929190000000000000
        Height = 15.118120000000000000
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '[(<frxDBDataset1."IMPORTE_COBRO">*.10)]')
        ParentFont = False
        VAlign = vaCenter
      end
      object SysMemo2: TfrxSysMemoView
        AllowVectorExport = True
        Left = 631.181510000000000000
        Top = 200.315090000000000000
        Width = 86.929190000000000000
        Height = 15.118120000000000000
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          
            '[([frxDBDataset1."IMPORTE_COBRO"]-(<frxDBDataset1."IMPORTE_COBRO' +
            '">*.10))]')
        ParentFont = False
        VAlign = vaCenter
      end
    end
  end
end
