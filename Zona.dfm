object NuevaZona: TNuevaZona
  Left = 0
  Top = 0
  Caption = 'Asignar Zona'
  ClientHeight = 138
  ClientWidth = 472
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 63
    Height = 23
    Caption = 'Cliente:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 30
    Top = 48
    Width = 49
    Height = 23
    Caption = 'Zona:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 119
    Width = 472
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object Edit1: TEdit
    Left = 88
    Top = 8
    Width = 377
    Height = 27
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Text = 'Edit1'
  end
  object btnGuardar: TButton
    Left = 192
    Top = 81
    Width = 89
    Height = 32
    Caption = 'Guardar'
    TabOrder = 2
    OnClick = btnGuardarClick
  end
  object cbZona: TDBLookupComboBox
    Left = 88
    Top = 48
    Width = 297
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyField = 'ZONA_CLIENTE_ID'
    ListField = 'NOMBRE'
    ListSource = dsZonas
    ParentFont = False
    PopupMenu = PopupMenu1
    TabOrder = 3
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'Protocol=TCPIP'
      'Server=localhost'
      'CharacterSet=ISO8859_1'
      'RoleName=USUARIO_MICROSIP'
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    UpdateOptions.AssignedValues = [uvAutoCommitUpdates]
    UpdateOptions.AutoCommitUpdates = True
    LoginPrompt = False
    Left = 405
    Top = 56
  end
  object qryZonas: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from zonas_clientes')
    Left = 24
    Top = 80
  end
  object dsZonas: TDataSource
    DataSet = qryZonas
    Left = 72
    Top = 80
  end
  object PopupMenu1: TPopupMenu
    Left = 336
    Top = 96
    object ZonaNueva1: TMenuItem
      Caption = 'Zona Nueva'
      OnClick = ZonaNueva1Click
    end
  end
end
