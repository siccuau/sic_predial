﻿object FormListaClientes: TFormListaClientes
  Left = 0
  Top = 0
  Caption = 'Lista Clientes'
  ClientHeight = 570
  ClientWidth = 1000
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblBuscar: TLabel
    Left = 16
    Top = 16
    Width = 32
    Height = 13
    Caption = 'Buscar'
  end
  object lblAño: TLabel
    Left = 280
    Top = 11
    Width = 22
    Height = 13
    Caption = 'A'#209'O'
    Visible = False
  end
  object lblConcepto: TLabel
    Left = 431
    Top = 11
    Width = 55
    Height = 13
    Caption = 'CONCEPTO'
    Visible = False
  end
  object txtBucar: TEdit
    Left = 16
    Top = 30
    Width = 241
    Height = 21
    TabOrder = 0
    OnChange = txtBucarChange
  end
  object dbgListaClientes: TDBGrid
    Left = 16
    Top = 88
    Width = 1550
    Height = 457
    DataSource = dataListaClientes
    PopupMenu = popMCliente
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDrawColumnCell = dbgListaClientesDrawColumnCell
    OnDblClick = dbgListaClientesDblClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 551
    Width = 1000
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cbAll: TCheckBox
    Left = 16
    Top = 65
    Width = 97
    Height = 17
    Checked = True
    Ctl3D = True
    ParentCtl3D = False
    State = cbChecked
    TabOrder = 2
    OnClick = cbAllClick
  end
  object CheckBox1: TCheckBox
    Left = 16
    Top = 88
    Width = 97
    Height = 13
    Ctl3D = True
    ParentCtl3D = False
    TabOrder = 4
    Visible = False
    OnClick = CheckBox1Click
  end
  object txtAño: TEdit
    Left = 280
    Top = 30
    Width = 121
    Height = 21
    MaxLength = 4
    NumbersOnly = True
    TabOrder = 5
    Visible = False
  end
  object cboConcepto: TComboBox
    Left = 431
    Top = 30
    Width = 145
    Height = 21
    TabOrder = 6
    Visible = False
  end
  object dtpYear: TDateTimePicker
    Left = 280
    Top = 30
    Width = 121
    Height = 21
    Date = 44097.000000000000000000
    Format = 'yyyy'
    Time = 0.573060266200627700
    DateMode = dmUpDown
    TabOrder = 7
    Visible = False
    OnChange = dtpYearChange
  end
  object btnGenerar: TButton
    Left = 592
    Top = 28
    Width = 91
    Height = 25
    Caption = 'Generar cargos'
    TabOrder = 8
    Visible = False
    OnClick = btnGenerarClick
  end
  object btnCancelar: TButton
    Left = 689
    Top = 28
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 9
    Visible = False
    OnClick = btnCancelarClick
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    LoginPrompt = False
    Left = 876
    Top = 14
  end
  object dataListaClientes: TDataSource
    DataSet = qryListaClientes
    Left = 800
    Top = 16
  end
  object qryListaClientes: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select '#39'S'#39' as seleccionado, cl.cliente_id, cl.nombre,cl.contacto' +
        '1,dcl.nombre_calle,dcl.rfc_curp,dcl.telefono1,dcl.num_exterior,d' +
        'cl.colonia,(select nombre from ciudades where ciudad_id=dcl.ciud' +
        'ad_id ) as Municipio from clientes cl'
      'left join dirs_clientes dcl'
      'on cl.cliente_id=dcl.cliente_id')
    Left = 712
    Top = 16
    object qryListaClientesSELECCIONADO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = ' '
      DisplayWidth = 2
      FieldName = 'SELECCIONADO'
      Origin = 'SELECCIONADO'
      ProviderFlags = []
      FixedChar = True
      Size = 1
    end
    object qryListaClientesCLIENTE_ID: TIntegerField
      FieldName = 'CLIENTE_ID'
      Origin = 'CLIENTE_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Visible = False
    end
    object qryListaClientesRFC_CURP: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'R.F.C'
      FieldName = 'RFC_CURP'
      Origin = 'RFC_CURP'
      ProviderFlags = []
      ReadOnly = True
      Size = 18
    end
    object qryListaClientesNOMBRE: TStringField
      DisplayLabel = 'NOMBRE COMPLETO'
      DisplayWidth = 50
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Required = True
      Size = 200
    end
    object qryListaClientesCONTACTO1: TStringField
      DisplayLabel = 'REPRESENTANTE LEGAL'
      DisplayWidth = 50
      FieldName = 'CONTACTO1'
      Origin = 'CONTACTO1'
      Size = 50
    end
    object qryListaClientesTELEFONO1: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'TEL.'
      DisplayWidth = 18
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      ProviderFlags = []
      ReadOnly = True
      Size = 35
    end
    object qryListaClientesMUNICIPIO: TStringField
      AutoGenerateValue = arDefault
      DisplayWidth = 15
      FieldName = 'MUNICIPIO'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object qryListaClientesNUM_EXTERIOR: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NUM_EXTERIOR'
      Origin = 'NUM_EXTERIOR'
      ProviderFlags = []
      ReadOnly = True
      Size = 10
    end
    object qryListaClientesCOLONIA: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CAMPO/FRACCIONAMIENTO'
      DisplayWidth = 40
      FieldName = 'COLONIA'
      Origin = 'COLONIA'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object qryListaClientesNOMBRE_CALLE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'CALLE/AVENIDA'
      DisplayWidth = 40
      FieldName = 'NOMBRE_CALLE'
      Origin = 'NOMBRE_CALLE'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
  end
  object MainMenu1: TMainMenu
    Left = 65528
    Top = 65520
    object NuevoCliente: TMenuItem
      Caption = 'Nuevo Cliente'
      OnClick = NuevoClienteClick
    end
    object Cargos1: TMenuItem
      Caption = 'Cargos x Cliente'
      OnClick = Cargos1Click
    end
    object CobrosxCliente1: TMenuItem
      Caption = 'Cobros x Cliente'
      OnClick = CobrosxCliente1Click
    end
    object Reportes1: TMenuItem
      Caption = 'Reportes'
      OnClick = Reportes1Click
    end
    object Configuracion1: TMenuItem
      Caption = 'Configuracion'
      object Predialporao1: TMenuItem
        Caption = 'Predial por a'#241'o'
        OnClick = Predialporao1Click
      end
    end
    object Generarcargosautomaticos1: TMenuItem
      Caption = 'Generar cargos automaticos'
      OnClick = Generarcargosautomaticos1Click
    end
    object Herramientas1: TMenuItem
      Caption = 'Herramientas'
      object Importarterrenos1: TMenuItem
        Caption = 'Importar terrenos'
        OnClick = Importarterrenos1Click
      end
    end
  end
  object qryConceptos: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from conceptos_cc where naturaleza='#39'C'#39)
    Left = 972
    Top = 376
    object qryConceptosCONCEPTO_CC_ID: TIntegerField
      FieldName = 'CONCEPTO_CC_ID'
      Origin = 'CONCEPTO_CC_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryConceptosNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Required = True
      Size = 50
    end
    object qryConceptosNOMBRE_ABREV: TStringField
      FieldName = 'NOMBRE_ABREV'
      Origin = 'NOMBRE_ABREV'
      Required = True
      Size = 30
    end
    object qryConceptosNATURALEZA: TStringField
      FieldName = 'NATURALEZA'
      Origin = 'NATURALEZA'
      FixedChar = True
      Size = 1
    end
    object qryConceptosMODALIDAD_FACTURACION: TStringField
      FieldName = 'MODALIDAD_FACTURACION'
      Origin = 'MODALIDAD_FACTURACION'
      Size = 10
    end
    object qryConceptosFOLIO_AUTOM: TStringField
      FieldName = 'FOLIO_AUTOM'
      Origin = 'FOLIO_AUTOM'
      FixedChar = True
      Size = 1
    end
    object qryConceptosTIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      FixedChar = True
      Size = 1
    end
    object qryConceptosES_PREDEF: TStringField
      FieldName = 'ES_PREDEF'
      Origin = 'ES_PREDEF'
      FixedChar = True
      Size = 1
    end
    object qryConceptosES_PREDET: TStringField
      FieldName = 'ES_PREDET'
      Origin = 'ES_PREDET'
      FixedChar = True
      Size = 1
    end
    object qryConceptosOCULTO: TStringField
      FieldName = 'OCULTO'
      Origin = 'OCULTO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object qryConceptosID_INTERNO: TStringField
      FieldName = 'ID_INTERNO'
      Origin = 'ID_INTERNO'
      FixedChar = True
      Size = 1
    end
    object qryConceptosAPLICAR_IMPUESTO: TStringField
      FieldName = 'APLICAR_IMPUESTO'
      Origin = 'APLICAR_IMPUESTO'
      FixedChar = True
      Size = 1
    end
    object qryConceptosAFECTO_A_IETU: TStringField
      FieldName = 'AFECTO_A_IETU'
      Origin = 'AFECTO_A_IETU'
      FixedChar = True
      Size = 1
    end
    object qryConceptosRETENER_IVA: TStringField
      FieldName = 'RETENER_IVA'
      Origin = 'RETENER_IVA'
      FixedChar = True
      Size = 1
    end
    object qryConceptosRETENER_ISR: TStringField
      FieldName = 'RETENER_ISR'
      Origin = 'RETENER_ISR'
      FixedChar = True
      Size = 1
    end
    object qryConceptosINTEGRAR_COMIS: TStringField
      FieldName = 'INTEGRAR_COMIS'
      Origin = 'INTEGRAR_COMIS'
      FixedChar = True
      Size = 1
    end
    object qryConceptosAPLICAR_DSCTO_PPAG: TStringField
      FieldName = 'APLICAR_DSCTO_PPAG'
      Origin = 'APLICAR_DSCTO_PPAG'
      FixedChar = True
      Size = 1
    end
    object qryConceptosAPLICAR_INTERES_MOR: TStringField
      FieldName = 'APLICAR_INTERES_MOR'
      Origin = 'APLICAR_INTERES_MOR'
      FixedChar = True
      Size = 1
    end
    object qryConceptosCREAR_POLIZAS: TStringField
      FieldName = 'CREAR_POLIZAS'
      Origin = 'CREAR_POLIZAS'
      FixedChar = True
      Size = 1
    end
    object qryConceptosCUENTA_CONTABLE: TStringField
      FieldName = 'CUENTA_CONTABLE'
      Origin = 'CUENTA_CONTABLE'
      Size = 30
    end
    object qryConceptosPREG_CUENTA: TStringField
      FieldName = 'PREG_CUENTA'
      Origin = 'PREG_CUENTA'
      FixedChar = True
      Size = 1
    end
    object qryConceptosTIPO_POLIZA: TStringField
      FieldName = 'TIPO_POLIZA'
      Origin = 'TIPO_POLIZA'
      FixedChar = True
      Size = 1
    end
    object qryConceptosDESCRIPCION_POLIZA: TStringField
      FieldName = 'DESCRIPCION_POLIZA'
      Origin = 'DESCRIPCION_POLIZA'
      Size = 200
    end
    object qryConceptosCLAVE_PRODSERV_SAT: TStringField
      FieldName = 'CLAVE_PRODSERV_SAT'
      Origin = 'CLAVE_PRODSERV_SAT'
      FixedChar = True
      Size = 8
    end
    object qryConceptosNUM_CTA_PREDIAL: TStringField
      FieldName = 'NUM_CTA_PREDIAL'
      Origin = 'NUM_CTA_PREDIAL'
      Size = 50
    end
    object qryConceptosDESCRIPCION_INMUEBLE: TMemoField
      FieldName = 'DESCRIPCION_INMUEBLE'
      Origin = 'DESCRIPCION_INMUEBLE'
      BlobType = ftMemo
    end
    object qryConceptosDESC_CONCEP_XML: TStringField
      FieldName = 'DESC_CONCEP_XML'
      Origin = 'DESC_CONCEP_XML'
      FixedChar = True
      Size = 1
    end
    object qryConceptosCLAVE_UMED_SAT: TStringField
      FieldName = 'CLAVE_UMED_SAT'
      Origin = 'CLAVE_UMED_SAT'
      FixedChar = True
      Size = 3
    end
    object qryConceptosUSUARIO_CREADOR: TStringField
      FieldName = 'USUARIO_CREADOR'
      Origin = 'USUARIO_CREADOR'
      Size = 31
    end
    object qryConceptosFECHA_HORA_CREACION: TSQLTimeStampField
      FieldName = 'FECHA_HORA_CREACION'
      Origin = 'FECHA_HORA_CREACION'
    end
    object qryConceptosUSUARIO_AUT_CREACION: TStringField
      FieldName = 'USUARIO_AUT_CREACION'
      Origin = 'USUARIO_AUT_CREACION'
      Size = 31
    end
    object qryConceptosUSUARIO_ULT_MODIF: TStringField
      FieldName = 'USUARIO_ULT_MODIF'
      Origin = 'USUARIO_ULT_MODIF'
      Size = 31
    end
    object qryConceptosFECHA_HORA_ULT_MODIF: TSQLTimeStampField
      FieldName = 'FECHA_HORA_ULT_MODIF'
      Origin = 'FECHA_HORA_ULT_MODIF'
    end
    object qryConceptosUSUARIO_AUT_MODIF: TStringField
      FieldName = 'USUARIO_AUT_MODIF'
      Origin = 'USUARIO_AUT_MODIF'
      Size = 31
    end
  end
  object BindSourceDB1: TBindSourceDB
    DataSet = qryConceptos
    ScopeMappings = <>
    Left = 496
    Top = 304
  end
  object BindingsList1: TBindingsList
    Methods = <>
    OutputConverters = <>
    Left = 116
    Top = 45
    object LinkListControlToField1: TLinkListControlToField
      Category = 'Quick Bindings'
      DataSource = BindSourceDB1
      FieldName = 'NOMBRE'
      Control = cboConcepto
      FillExpressions = <>
      FillHeaderExpressions = <>
      FillBreakGroups = <>
    end
  end
  object qryTerrenos: TFDQuery
    Connection = Conexion
    Left = 840
    Top = 65528
  end
  object qryValor: TFDQuery
    Connection = Conexion
    Left = 432
    Top = 144
  end
  object qryMunicipioTerrenos: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'SELECT MUNICIPIO FROM SIC_PREDIAL_TERRENOS group by MUNICIPIO')
    Left = 928
    Top = 24
  end
  object popMCliente: TPopupMenu
    Left = 584
    Top = 184
    object Modificar1: TMenuItem
      Caption = 'Modificar'
      OnClick = Modificar1Click
    end
  end
end
