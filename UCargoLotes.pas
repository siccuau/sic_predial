unit UCargoLotes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Vcl.Grids, Vcl.StdCtrls,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,DateUtils, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, System.Rtti, System.Bindings.Outputs,
  Vcl.Bind.Editors, Data.Bind.EngExt, Vcl.Bind.DBEngExt, Data.Bind.Components,
  Data.Bind.DBScope,UConcepto, Vcl.Menus, Vcl.ComCtrls,UValoresTerrenos;

type
  TFormCargoLotes = class(TForm)
    qryCliente: TFDQuery;
    cboCliente: TComboBox;
    lblCliente: TLabel;
    lblA�o: TLabel;
    txtA�o: TEdit;
    Conexion: TFDConnection;
    lblConcepto: TLabel;
    cboConcepto: TComboBox;
    qryConceptos: TFDQuery;
    qryTerrenos: TFDQuery;
    sgTerreno: TStringGrid;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    BindSourceDB2: TBindSourceDB;
    LinkListControlToField2: TLinkListControlToField;
    popmnuConcepto: TPopupMenu;
    Nuevo1: TMenuItem;
    btnCargos: TButton;
    cboTipoTerreno: TComboBox;
    lblTipo: TLabel;
    btnCalcular: TButton;
    dtpYear: TDateTimePicker;
    qryValores: TFDQuery;
    StatusBar1: TStatusBar;
    procedure Cargo(id_concepto:Integer;folio_terreno:string;importe:Double;id_cliente:Integer);
    procedure CargarDatos;
    procedure formatogrid;
    procedure FormShow(Sender: TObject);
    procedure cboClienteChange(Sender: TObject);
    procedure sgTerrenoSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure Nuevo1Click(Sender: TObject);
    procedure btnCargosClick(Sender: TObject);
    procedure cboTipoTerrenoChange(Sender: TObject);
    procedure btnCalcularClick(Sender: TObject);
    procedure dtpYearChange(Sender: TObject);
    procedure txtA�oChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormCargoLotes: TFormCargoLotes;

implementation

{$R *.dfm}


procedure TFormCargoLotes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
qryConceptos.Close();
qryCliente.Close();
qryValores.Close();
end;

procedure TFormCargoLotes.FormShow(Sender: TObject);
begin
qryConceptos.Open();
qryCliente.Open();
qryValores.Open();
CargarDatos;
txtA�o.Text:=YearOf(dtpYear.Date).ToString;
end;
procedure TFormCargoLotes.Nuevo1Click(Sender: TObject);
var
FormConcepto:TFormConcepto;
begin
  FormConcepto:= TFormConcepto.Create(nil);
  FormConcepto.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormConcepto.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormConcepto.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormConcepto.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormConcepto.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  //TFormCampoNMA.tipo:='Marca';
  FormConcepto.ShowModal;
  qryConceptos.Refresh;
end;

procedure TFormCargoLotes.sgTerrenoSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
R: TRect;
begin
if ACol = 7 then
 begin
  if (cboTipoTerreno.ItemIndex = 1) or (cboTipoTerreno.ItemIndex = 2)  then
    begin
      CanSelect:=False;
    end
  else
    begin
      CanSelect := True;
      sgTerreno.Options := sgTerreno.Options + [goEditing];
    end;
 end;
 if ACol < 7 then
 begin
   CanSelect:=False;
 end;



end;

procedure TFormCargoLotes.txtA�oChange(Sender: TObject);
begin
CargarDatos;
if (cboTipoTerreno.ItemIndex = 1) or (cboTipoTerreno.ItemIndex = 2)  then
  begin
    btnCargos.Visible:=False;
    btnCalcular.Visible:=True;
  end
else
  begin
    btnCargos.Visible:=True;
    btnCalcular.Visible:=False;
  end;
end;

procedure TFormCargoLotes.btnCalcularClick(Sender: TObject);
var
tipo,medida:string;
valor:Double;
i,buttonSelected:Integer;
FormPrincipal:TFormValoresTerrenos;
begin
     case cboTipoTerreno.ItemIndex of
      0:tipo:='LOTE';
      1:tipo:='TEMPORAL';
      2:tipo:='RIEGO';
      3:tipo:='CASA';
     end;
if (txtA�o.Text='') then
begin
  ShowMessage('Falta A�o');
  qryValores.Filtered := False;
end
else
  begin

      for i := 1 to sgTerreno.RowCount-2 do
        begin
          qryValores.Filter := 'ANO='''+txtA�o.Text+''' AND TIPO_TERRENO='''+tipo+''' AND MUNICIPIO='''+sgTerreno.Cells[1,i]+''' ';
          qryValores.Filtered := True;
          medida:=qryValores.FieldByName('MEDIDA').AsString;
          if medida <> '' then
            begin
            valor:=qryValores.FieldByName('VALOR').AsFloat;
            if medida='METROS' then sgTerreno.Cells[7,i]:= (sgTerreno.Cells[5,i].ToDouble*valor).ToString;
            if medida='ACRES' then sgTerreno.Cells[7,i]:= (sgTerreno.Cells[6,i].ToDouble*valor).ToString;
            btnCalcular.Visible:=False;
            btnCargos.Visible:=True;
            end
          else
            begin

              buttonSelected := messagedlg('No hay un valor para el a�o '+txtA�o.Text+' del tipo '+tipo+' y el municipio '+sgTerreno.Cells[1,i]+' desea agregarlo?',mtError, mbOKCancel, 0);
              if buttonSelected = mrOK     then
                begin
                  FormPrincipal:= TFormValoresTerrenos.Create(nil);
                  FormPrincipal.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
                  FormPrincipal.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
                  FormPrincipal.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
                  FormPrincipal.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
                  FormPrincipal.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
                  FormPrincipal.ShowModal;
                  qryValores.Refresh;
                  Break;
                end;
              if buttonSelected = mrCancel then
                begin
                end;
            end;
          end;
        end;


end;

procedure TFormCargoLotes.btnCargosClick(Sender: TObject);
var
i:Integer;
folio_cargo:string;
costo:string;
saldo:Double;
begin
if (txtA�o.Text='') then ShowMessage('Falta A�o')
else
  begin
     for i := 1 to sgTerreno.RowCount-2 do
     begin
        if sgTerreno.Cells[7,i]='' then
        begin
          ShowMessage('No hay costo');
          Conexion.Rollback;
          Break;
        end
        else
        begin
          folio_cargo:=Conexion.ExecSQLScalar('SELECT FOLIO_CARGO FROM SIC_PREDIAL_CARGOS WHERE FOLIO_TERRENO='''+sgTerreno.Cells[0,i]+''' AND ANO='''+txtA�o.Text+'''');
          if folio_cargo = '' then
           begin
            costo:=sgTerreno.Cells[7,i];
            Cargo(qryConceptos.FieldByName('CONCEPTO_CC_ID').AsInteger,sgTerreno.Cells[0,i],StrToFloat(costo),qryCliente.FieldByName('CLIENTE_ID').AsInteger);
           end
          else
           begin
            saldo:=Conexion.ExecSQLScalar('SELECT IMPORTE_COBRO FROM DOCTOS_CC WHERE FOLIO='''+folio_cargo+'''');
            sgTerreno.Cells[7,i]:=saldo.ToString;
            ShowMessage('Existe cargo generado de el a�o '+txtA�o.Text+' al terreno con folio '+sgTerreno.Cells[0,i]+'');
           end;

        end;

     end;
  end;

end;

procedure TFormCargoLotes.CargarDatos;
var tipo,folio,zona,traspaso,campo,direccion,municipio:string;
i,numero,a�o:integer;
este,norte,metros,acres,MetrosTotal,AcresTotal,costo:double;
begin
      case cboTipoTerreno.ItemIndex of
      0:tipo:='LOTE';
      1:tipo:='TEMPORAL';
      2:tipo:='RIEGO';
      3:tipo:='CASA';
      4:tipo:='CORREDOR';
     end;
     //if qryCliente.FieldByName('CLIENTE_ID').Value=0 then

      if Conexion.ExecSQLScalar('select count(id_terreno) from sic_predial_terrenos where cliente_id=:cliente and tipo=:tipo',[qryCliente.FieldByName('CLIENTE_ID').AsString,tipo])>0 then
       begin
         formatogrid;
         i:=1;
         qryTerrenos.SQL.Text:='select * from sic_predial_terrenos where cliente_id=:cliente and tipo=:tipo';
         qryTerrenos.ParamByName('cliente').Value:=qryCliente.FieldByName('CLIENTE_ID').AsString;
         qryTerrenos.ParamByName('tipo').Value:=tipo;
         qryTerrenos.Open();
         qryTerrenos.First;
         while not qryTerrenos.Eof do
         begin
           folio:=qryTerrenos.FieldByName('folio').Value;

           if qryTerrenos.FieldByName('municipio').Value<>null then
           municipio:=qryTerrenos.FieldByName('municipio').Value
           else municipio:='';

           este:=qryTerrenos.FieldByName('mts_este').Value;
           norte:=qryTerrenos.FieldByName('mts_norte').Value;
           numero:=qryTerrenos.FieldByName('numero').Value;
           metros:=qryTerrenos.FieldByName('metros').Value;
           acres:=qryTerrenos.FieldByName('acres').Value;


            sgTerreno.Cells[0,i]:=folio;;
            sgTerreno.Cells[1,i]:=municipio;
            sgTerreno.Cells[2,i]:=floattostr(este);
            sgTerreno.Cells[3,i]:=floattostr(norte);
            sgTerreno.Cells[4,i]:=floattostr(numero);
            sgTerreno.Cells[5,i]:=floattostr(metros);
            sgTerreno.Cells[6,i]:=floattostr(acres);
            i:=i+1;
            sgTerreno.RowCount:=sgTerreno.RowCount+1;
            qryTerrenos.Next;
         end;

       end
       else formatogrid;
end;
procedure TFormCargoLotes.formatogrid();
begin
sgTerreno.Cols[0].Text:='Folio No.';
sgTerreno.ColWidths[0]:=180;
sgTerreno.Cols[1].Text:='Municipio';
sgTerreno.ColWidths[1]:=110;
sgTerreno.Cols[2].Text:='Este mts.';
sgTerreno.ColWidths[2]:=140;
sgTerreno.Cols[3].Text:='Norte mts.';
sgTerreno.ColWidths[3]:=180;
sgTerreno.Cols[4].Text:='Numero';
sgTerreno.ColWidths[4]:=130;
sgTerreno.Cols[5].Text:='Metros M�';
sgTerreno.ColWidths[5]:=160;
sgTerreno.Cols[6].Text:='Acres';
sgTerreno.ColWidths[6]:=120;
sgTerreno.Cols[7].Text:='Costo';
sgTerreno.ColWidths[7]:=120;
sgTerreno.RowCount:=2;
sgTerreno.Cells[0,1]:='';
sgTerreno.Cells[1,1]:='';
sgTerreno.Cells[2,1]:='';
sgTerreno.Cells[3,1]:='';
sgTerreno.Cells[4,1]:='';
sgTerreno.Cells[5,1]:='';
sgTerreno.Cells[6,1]:='';
sgTerreno.Cells[7,1]:='';
end;
procedure TFormCargoLotes.Cargo(id_concepto:Integer;folio_terreno:string;importe:Double;id_cliente:Integer);
var
naturaleza,clave_cliente,folio_cc,serie,folio,fecha,descripcion:string;
sucursal_id,consecutivo,id_cc,id_ven_cc,a�o,mes,dia,id_cliente_saldo:Integer;
myDate : TDateTime;
saldo:Double;
begin
myDate:= Now;
a�o:=YearOf(myDate);
dia:=DayOf(myDate);
mes:=MonthOf(myDate);
myDate:=IncDay(myDate, 7);

fecha:=FormatDateTime('mm/dd/yy', myDate);
descripcion:='PREDIAL DEL '+txtA�o.Text+'';
naturaleza:=Conexion.ExecSQLScalar('select naturaleza from conceptos_cc where CONCEPTO_CC_ID='+id_concepto.ToString+'');
sucursal_id:=Conexion.ExecSQLScalar('select sucursal_id from sucursales where nombre=''Matriz''');
serie:=Conexion.ExecSQLScalar('select serie from folios_conceptos where concepto_id='+id_concepto.ToString+'');
consecutivo:=Conexion.ExecSQLScalar('select consecutivo from folios_conceptos where concepto_id='+id_concepto.ToString+'');
folio:= serie+ consecutivo.ToString;
clave_cliente:=Conexion.ExecSQLScalar('SELECT CLAVE_CLIENTE FROM CLAVES_CLIENTES WHERE CLIENTE_ID='''+id_cliente.ToString+'''');
try
id_cc:=Conexion.ExecSQLScalar('INSERT INTO DOCTOS_CC(DOCTO_CC_ID,CONCEPTO_CC_ID,FOLIO,NATURALEZA_CONCEPTO,SUCURSAL_ID,'+
                  'FECHA,CLAVE_CLIENTE,IMPORTE_COBRO,CLIENTE_ID,APLICADO,CONTABILIZADO_GYP,'+
                  'PCTJE_DSCTO_PPAG,SISTEMA_ORIGEN,ESTATUS,ESTATUS_ANT,ES_CFD,TIENE_ANTICIPO,'+
                  'CFDI_COBRO_DIFERIDO,ENVIADO,CFDI_CERTIFICADO,INTEG_BA,CONTABILIZADO_BA,DESCRIPCION) VALUES('+
                  '-1,'+id_concepto.ToString+','''+folio+''','''+naturaleza+''','+sucursal_id.ToString+','+
                  'CURRENT_DATE,'''+clave_cliente+''','+importe.ToString+','+id_cliente.ToString+',''S'',''N'','+
                  '0,''CC'',''N'',''N'',''N'',''N'',False,''N'',''N'',''N'',''N'','''+descripcion+''') RETURNING DOCTO_CC_ID');
Conexion.ExecSQL('INSERT INTO IMPORTES_DOCTOS_CC(IMPTE_DOCTO_CC_ID,DOCTO_CC_ID,DOCTO_CC_ACR_ID,FECHA,APLICADO,ESTATUS,'+
                  'IMPORTE,IMPUESTO,IVA_RETENIDO,ISR_RETENIDO,DSCTO_PPAG,PCTJE_COMIS_COB,TIPO_IMPTE)'+
                  'VALUES(-1,'+id_cc.ToString+','+id_cc.ToString+',CURRENT_DATE,''S'',''N'','+importe.ToString+',0,0,0,0,0,''C'')');
Conexion.ExecSQL('INSERT INTO VENCIMIENTOS_CARGOS_CC(DOCTO_CC_ID,FECHA_VENCIMIENTO,PCTJE_VEN) VALUES('+id_cc.ToString+','''+fecha+''',100)');
consecutivo:=consecutivo+1;
Conexion.ExecSQL('Update folios_conceptos set consecutivo='+consecutivo.ToString+' where concepto_id='+id_concepto.ToString+'');
Conexion.ExecSQL('INSERT INTO SIC_PREDIAL_CARGOS(ID_CARGO_PREDIAL,FOLIO_TERRENO,ANO,FECHA,FOLIO_CARGO) VALUES(-1,'''+folio_terreno+''','''+txtA�o.Text+''',CURRENT_DATE,'''+folio+''')');
id_cliente_saldo:=Conexion.ExecSQLScalar('SELECT CLIENTE_ID FROM SALDOS_CC WHERE CLIENTE_ID='+id_cliente.ToString+' AND ANO='+a�o.ToString+' AND MES='+mes.ToString+' AND ULTIMO_DIA='+dia.ToString+'');

ShowMessage('Cargo generado correctamente');
Except
Conexion.Rollback;
ShowMessage('Ocurrio un error');
end;
end;
procedure TFormCargoLotes.cboClienteChange(Sender: TObject);
begin
CargarDatos;

end;

procedure TFormCargoLotes.cboTipoTerrenoChange(Sender: TObject);
begin
CargarDatos;
if (cboTipoTerreno.ItemIndex = 1) or (cboTipoTerreno.ItemIndex = 2)  then
  begin
    btnCargos.Visible:=False;
    btnCalcular.Visible:=True;
  end
else
  begin
    btnCargos.Visible:=True;
    btnCalcular.Visible:=False;
  end;

end;

procedure TFormCargoLotes.dtpYearChange(Sender: TObject);
begin
txtA�o.Text:=YearOf(dtpYear.Date).ToString;
end;

end.
