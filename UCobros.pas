unit UCobros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Vcl.Menus, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.StdCtrls,UConcepto,
  Data.Bind.EngExt, Vcl.Bind.DBEngExt, System.Rtti, System.Bindings.Outputs,
  Vcl.Bind.Editors, Data.Bind.Components, Data.Bind.DBScope, Vcl.ComCtrls,
  Vcl.Grids,DateUtils;

type
  TFormCobro = class(TForm)
    lblConcepto: TLabel;
    lblCliente: TLabel;
    cboConcepto: TComboBox;
    cboCliente: TComboBox;
    Conexion: TFDConnection;
    qryCliente: TFDQuery;
    qryConceptos: TFDQuery;
    popmnuConcepto: TPopupMenu;
    Nuevo1: TMenuItem;
    qryConceptosCONCEPTO_CC_ID: TIntegerField;
    qryConceptosNOMBRE: TStringField;
    qryConceptosNOMBRE_ABREV: TStringField;
    qryConceptosNATURALEZA: TStringField;
    qryConceptosMODALIDAD_FACTURACION: TStringField;
    qryConceptosFOLIO_AUTOM: TStringField;
    qryConceptosTIPO: TStringField;
    qryConceptosES_PREDEF: TStringField;
    qryConceptosES_PREDET: TStringField;
    qryConceptosOCULTO: TStringField;
    qryConceptosID_INTERNO: TStringField;
    qryConceptosAPLICAR_IMPUESTO: TStringField;
    qryConceptosAFECTO_A_IETU: TStringField;
    qryConceptosRETENER_IVA: TStringField;
    qryConceptosRETENER_ISR: TStringField;
    qryConceptosINTEGRAR_COMIS: TStringField;
    qryConceptosAPLICAR_DSCTO_PPAG: TStringField;
    qryConceptosAPLICAR_INTERES_MOR: TStringField;
    qryConceptosCREAR_POLIZAS: TStringField;
    qryConceptosCUENTA_CONTABLE: TStringField;
    qryConceptosPREG_CUENTA: TStringField;
    qryConceptosTIPO_POLIZA: TStringField;
    qryConceptosDESCRIPCION_POLIZA: TStringField;
    qryConceptosCLAVE_PRODSERV_SAT: TStringField;
    qryConceptosNUM_CTA_PREDIAL: TStringField;
    qryConceptosDESCRIPCION_INMUEBLE: TMemoField;
    qryConceptosDESC_CONCEP_XML: TStringField;
    qryConceptosCLAVE_UMED_SAT: TStringField;
    qryConceptosUSUARIO_CREADOR: TStringField;
    qryConceptosFECHA_HORA_CREACION: TSQLTimeStampField;
    qryConceptosUSUARIO_AUT_CREACION: TStringField;
    qryConceptosUSUARIO_ULT_MODIF: TStringField;
    qryConceptosFECHA_HORA_ULT_MODIF: TSQLTimeStampField;
    qryConceptosUSUARIO_AUT_MODIF: TStringField;
    cboFormaCobro: TComboBox;
    lblForma: TLabel;
    qryFormaCobro: TFDQuery;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    BindSourceDB2: TBindSourceDB;
    LinkListControlToField2: TLinkListControlToField;
    qryFormaCobroFORMA_COBRO_CC_ID: TIntegerField;
    qryFormaCobroNOMBRE: TStringField;
    qryFormaCobroMONEDA_ID: TIntegerField;
    qryFormaCobroDIAS_TRANSITO: TIntegerField;
    qryFormaCobroMANEJO_REFER: TStringField;
    qryFormaCobroREFER_OBLIGATORIA: TStringField;
    qryFormaCobroCLAVE_FISCAL: TStringField;
    qryFormaCobroES_PREDET: TStringField;
    qryFormaCobroOCULTO: TStringField;
    qryFormaCobroREGISTRA_INFO_BAN: TStringField;
    qryFormaCobroCONCEPTO_BA_ID: TIntegerField;
    qryFormaCobroDESCRIPCION_MOVTO_BA: TStringField;
    qryFormaCobroUSUARIO_CREADOR: TStringField;
    qryFormaCobroFECHA_HORA_CREACION: TSQLTimeStampField;
    qryFormaCobroUSUARIO_AUT_CREACION: TStringField;
    qryFormaCobroUSUARIO_ULT_MODIF: TStringField;
    qryFormaCobroFECHA_HORA_ULT_MODIF: TSQLTimeStampField;
    qryFormaCobroUSUARIO_AUT_MODIF: TStringField;
    BindSourceDB3: TBindSourceDB;
    LinkListControlToField3: TLinkListControlToField;
    lblDescripcion: TLabel;
    txtDescripcion: TEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    stgCargoAcreditados: TStringGrid;
    qryCargosAcreditados: TFDQuery;
    lblImporte: TLabel;
    txtImporte: TEdit;
    btnGuardar: TButton;
    lblTotal: TLabel;
    txtTotal: TEdit;
    StatusBar1: TStatusBar;
    lblSALDO: TLabel;
    txtSaldoCliente: TEdit;
    btnGuardarC: TButton;
    btnGuardarN: TButton;
    btnLiquidar: TButton;
    procedure Nuevo1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Cargo(id_concepto:Integer;folio_terreno:string;importe:Double;id_cliente:Integer);
    procedure Totales;
    procedure formatogrid;
    procedure CargarDatos(tipo:string;cc_id:Integer);
    procedure cboClienteChange(Sender: TObject);
    procedure stgCargoAcreditadosSelectCell(Sender: TObject; ACol,
      ARow: Integer; var CanSelect: Boolean);
    procedure PageControl1Change(Sender: TObject);
    procedure stgCargoAcreditadosKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure stgCargoAcreditadosSetEditText(Sender: TObject; ACol,
      ARow: Integer; const Value: string);
    procedure btnGuardarClick(Sender: TObject);
    procedure btnGuardarCClick(Sender: TObject);
    procedure btnGuardarNClick(Sender: TObject);
    procedure btnLiquidarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormCobro: TFormCobro;

implementation

{$R *.dfm}

procedure TFormCobro.FormClose(Sender: TObject; var Action: TCloseAction);
begin
qryCliente.Close();
qryConceptos.Close();
qryFormaCobro.Close();
end;

procedure TFormCobro.FormShow(Sender: TObject);
var
result:string;
begin

qryCliente.Open();
qryConceptos.Open();
qryFormaCobro.Open();
CargarDatos('P',0);
Totales;

result:=Conexion.ExecSQLScalar('EXECUTE PROCEDURE CALC_SALDO_CLIENTE('+qryCliente.FieldByName('CLIENTE_ID').AsString+',current_date,''N'')');
txtSaldoCliente.Text:=result;
end;

procedure TFormCobro.Nuevo1Click(Sender: TObject);
var
FormConcepto:TFormConcepto;
begin
  FormConcepto:= TFormConcepto.Create(nil);
  FormConcepto.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormConcepto.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormConcepto.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormConcepto.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormConcepto.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  //TFormCampoNMA.tipo:='Marca';
  FormConcepto.ShowModal;
  qryConceptos.Refresh;
end;

procedure TFormCobro.PageControl1Change(Sender: TObject);
begin
if PageControl1.ActivePageIndex = 1 then
  begin
    if cboCliente.Text='' then
    begin
      ShowMessage('No hay cliente seleccionado');
      PageControl1.ActivePage:=TabSheet1;
    end
    else if cboFormaCobro.Text='' then
    begin
      ShowMessage('Falta forma de cobro');
      PageControl1.ActivePage:=TabSheet1;
    end
    else if qryCargosAcreditados.RecordCount=0  then
    begin
      ShowMessage('No hay cargos por acreditar al cliente');
      PageControl1.ActivePage:=TabSheet1;
    end
    else if (txtImporte.Text='0') or (txtImporte.Text='') then
    begin
      ShowMessage('Advertencia! No ha cargado el importe');
      PageControl1.ActivePage:=TabSheet1;
    end;

  end;
end;

procedure TFormCobro.stgCargoAcreditadosKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
Totales;
end;

procedure TFormCobro.stgCargoAcreditadosSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
if ACol = 4 then
 begin
  //R := sgTerreno.CellRect(ACol, ARow);
  //R.Left := R.Left + sgTerreno.Left;
  //R.Right := R.Right + sgTerreno.Left;
  //R.Top := R.Top + sgTerreno.Top;
  //R.Bottom := R.Bottom + sgTerreno.Top;
  CanSelect := True;
  stgCargoAcreditados.Options := stgCargoAcreditados.Options + [goEditing];
 end;
 if ACol < 4 then
 begin
   CanSelect:=False;
 end;

end;

procedure TFormCobro.stgCargoAcreditadosSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: string);
begin
if value <>'' then Totales;
end;

procedure TFormCobro.formatogrid();
begin
stgCargoAcreditados.Cols[0].Text:='Concepto';
stgCargoAcreditados.ColWidths[0]:=100;
stgCargoAcreditados.Cols[1].Text:='Folio ';
stgCargoAcreditados.ColWidths[1]:=80;
stgCargoAcreditados.Cols[2].Text:='Vencimiento';
stgCargoAcreditados.ColWidths[2]:=100;
stgCargoAcreditados.Cols[3].Text:='Saldo Actual';
stgCargoAcreditados.ColWidths[3]:=100;
stgCargoAcreditados.Cols[4].Text:='Acreditar';
stgCargoAcreditados.ColWidths[4]:=100;
//stgCargoAcreditados.Cols[5].Text:='Metros M�';
//stgCargoAcreditados.ColWidths[5]:=160;
//stgCargoAcreditados.Cols[6].Text:='Acres';
//stgCargoAcreditados.ColWidths[6]:=120;
//stgCargoAcreditados.Cols[7].Text:='Costo';
//stgCargoAcreditados.ColWidths[7]:=120;
stgCargoAcreditados.RowCount:=2;
stgCargoAcreditados.Cells[0,1]:='';
stgCargoAcreditados.Cells[1,1]:='';
stgCargoAcreditados.Cells[2,1]:='';
stgCargoAcreditados.Cells[3,1]:='';
stgCargoAcreditados.Cells[4,1]:='0';
//stgCargoAcreditados.Cells[5,1]:='';
//stgCargoAcreditados.Cells[6,1]:='';
//stgCargoAcreditados.Cells[7,1]:='';
end;


procedure TFormCobro.btnGuardarCClick(Sender: TObject);
var
total,importe:Double;

begin
importe:= StrToFloat(txtImporte.Text);
total:= StrToFloat(txtTotal.Text);
if (importe < total) or (total < importe) then ShowMessage('Indique un total acreditado que sea igual que el importe')
else
  begin
    Cargo(qryConceptos.FieldByName('CONCEPTO_CC_ID').AsInteger,'C',StrToFloat(txtImporte.Text),qryCliente.FieldByName('CLIENTE_ID').AsInteger);
    Close;
  end;

end;

procedure TFormCobro.btnGuardarClick(Sender: TObject);
var
total,importe:Double;

begin
importe:= StrToFloat(txtImporte.Text);
total:= StrToFloat(txtTotal.Text);
if (importe < total) or (total < importe) then ShowMessage('Indique un total acreditado que sea igual que el importe')
else if importe = 0 then ShowMessage('No hay importe que acreditar')
else
  begin
    Cargo(qryConceptos.FieldByName('CONCEPTO_CC_ID').AsInteger,'',StrToFloat(txtImporte.Text),qryCliente.FieldByName('CLIENTE_ID').AsInteger);
  end;



end;

procedure TFormCobro.btnGuardarNClick(Sender: TObject);
var
total,importe:Double;

begin
importe:= StrToFloat(txtImporte.Text);
total:= StrToFloat(txtTotal.Text);
if (importe < total) or (total < importe) then ShowMessage('Indique un total acreditado que sea igual que el importe')
else
  begin
    Cargo(qryConceptos.FieldByName('CONCEPTO_CC_ID').AsInteger,'N',StrToFloat(txtImporte.Text),qryCliente.FieldByName('CLIENTE_ID').AsInteger);
  end;



end;

procedure TFormCobro.btnLiquidarClick(Sender: TObject);
var
importe,saldo_restante:Double;
i:Integer;
begin
importe:=StrToFloat(txtImporte.Text);
saldo_restante:=importe;
for i := 1 to stgCargoAcreditados.RowCount-2 do
  begin
    if saldo_restante >0 then
    begin
      if stgCargoAcreditados.Cells[3,i].ToDouble > saldo_restante then
        begin
          stgCargoAcreditados.Cells[4,i]:=saldo_restante.ToString;
          saldo_restante:=saldo_restante - stgCargoAcreditados.Cells[3,i].ToDouble;
        end
      else
        begin
          stgCargoAcreditados.Cells[4,i]:=stgCargoAcreditados.Cells[3,i];
          saldo_restante:=saldo_restante - stgCargoAcreditados.Cells[3,i].ToDouble;
        end;

    end;

  end;
  Totales;
end;

procedure TFormCobro.CargarDatos(tipo:string;cc_id:Integer);
var fecha,folio,concepto,saldo:string;
i,numero:integer;
este,norte,metros,acres,MetrosTotal,AcresTotal:double;
begin

if Conexion.ExecSQLScalar('select count(SALDO_CARGO) FROM get_cargos_cc(current_date,'''+tipo+''',0,''S'',''S'',''N'') WHERE NOMBRE_CLIENTE=:nombre',[qryCliente.FieldByName('NOMBRE').AsString])>0 then
 begin
   formatogrid;
   i:=1;
   if cc_id <> 0 then
    begin
      qryCargosAcreditados.SQL.Text:='SELECT DOCTO_CC_ID,NOMBRE_ABREV_CONCEPTO,FOLIO,FECHA_VENCIMIENTO,SALDO_CARGO FROM get_cargos_cc(current_date,'''+tipo+''',0,''S'',''S'',''N'') WHERE NOMBRE_CLIENTE=:nombre AND DOCTO_CC_ID=:cc_id ORDER BY FECHA_VENCIMIENTO';
      qryCargosAcreditados.ParamByName('nombre').Value:=qryCliente.FieldByName('NOMBRE').AsString;
      qryCargosAcreditados.ParamByName('cc_id').Value:=cc_id;
    end
   else
    begin
     qryCargosAcreditados.SQL.Text:='SELECT DOCTO_CC_ID,NOMBRE_ABREV_CONCEPTO,FOLIO,FECHA_VENCIMIENTO,SALDO_CARGO FROM get_cargos_cc(current_date,'''+tipo+''',0,''S'',''S'',''N'') WHERE NOMBRE_CLIENTE=:nombre ORDER BY FECHA_VENCIMIENTO';
     qryCargosAcreditados.ParamByName('nombre').Value:=qryCliente.FieldByName('NOMBRE').AsString;
    end;

   qryCargosAcreditados.Open();
   qryCargosAcreditados.First;
   while not qryCargosAcreditados.Eof do
   begin
   concepto:=qryCargosAcreditados.FieldByName('NOMBRE_ABREV_CONCEPTO').Value;
   folio:=qryCargosAcreditados.FieldByName('FOLIO').Value;
   fecha:=qryCargosAcreditados.FieldByName('FECHA_VENCIMIENTO').Value;
   saldo:=qryCargosAcreditados.FieldByName('SALDO_CARGO').Value;

    stgCargoAcreditados.Cells[0,i]:=concepto;
    stgCargoAcreditados.Cells[1,i]:=folio;
    stgCargoAcreditados.Cells[2,i]:=fecha;
    stgCargoAcreditados.Cells[3,i]:=saldo;
    //sgTerreno.Cells[4,i]:=floattostr(numero);

    i:=i+1;
    stgCargoAcreditados.RowCount:=stgCargoAcreditados.RowCount+1;
    qryCargosAcreditados.Next;
   end;

 end
 else
 begin
  formatogrid;
  qryCargosAcreditados.ClearBlobs;
 end;

end;
procedure TFormCobro.Totales();
var
i:Integer;
total:Double;
begin
      for i := 1 to stgCargoAcreditados.RowCount-2 do
        begin
          if stgCargoAcreditados.Cells[4,i] <> '' then
          begin
            if stgCargoAcreditados.Cells[3,i].ToDouble < stgCargoAcreditados.Cells[4,i].ToDouble  then
            begin
              ShowMessage('El importe por acreditar no puede ser mayor que el saldo');
            end
            else
            begin
              total:= total+stgCargoAcreditados.Cells[4,i].ToDouble;
              txtTotal.Text:=total.ToString;
            end;
          end
          else stgCargoAcreditados.Cells[4,i]:='0';

        end;
end;

procedure TFormCobro.Cargo(id_concepto:Integer;folio_terreno:string;importe:Double;id_cliente:Integer);
var
naturaleza,clave_cliente,folio_cc,serie,folio,fecha,descripcion:string;
sucursal_id,consecutivo,id_cc,id_ven_cc,a�o,mes,dia,id_cliente_saldo,id_cc_acr,i,buttonSelected:Integer;
myDate : TDateTime;
saldo:Double;
begin
myDate:= Now;
a�o:=YearOf(myDate);
dia:=DayOf(myDate);
mes:=MonthOf(myDate);
myDate:=IncDay(myDate, 7);

fecha:=FormatDateTime('mm/dd/yy', myDate);
descripcion:='COBRO PREDIAL';
naturaleza:=Conexion.ExecSQLScalar('select naturaleza from conceptos_cc where CONCEPTO_CC_ID='+id_concepto.ToString+'');
sucursal_id:=Conexion.ExecSQLScalar('select sucursal_id from sucursales where nombre=''Matriz''');
serie:=Conexion.ExecSQLScalar('select serie from folios_conceptos where concepto_id='+id_concepto.ToString+'');
consecutivo:=Conexion.ExecSQLScalar('select consecutivo from folios_conceptos where concepto_id='+id_concepto.ToString+'');
folio:= serie+ consecutivo.ToString;
clave_cliente:=Conexion.ExecSQLScalar('SELECT CLAVE_CLIENTE FROM CLAVES_CLIENTES WHERE CLIENTE_ID='''+id_cliente.ToString+'''');
try
id_cc:=Conexion.ExecSQLScalar('INSERT INTO DOCTOS_CC(DOCTO_CC_ID,CONCEPTO_CC_ID,FOLIO,NATURALEZA_CONCEPTO,SUCURSAL_ID,'+
                  'FECHA,CLAVE_CLIENTE,IMPORTE_COBRO,CLIENTE_ID,APLICADO,CONTABILIZADO_GYP,'+
                  'PCTJE_DSCTO_PPAG,SISTEMA_ORIGEN,ESTATUS,ESTATUS_ANT,ES_CFD,TIENE_ANTICIPO,'+
                  'CFDI_COBRO_DIFERIDO,ENVIADO,CFDI_CERTIFICADO,INTEG_BA,CONTABILIZADO_BA,DESCRIPCION) VALUES('+
                  '-1,'+id_concepto.ToString+','''+folio+''','''+naturaleza+''','+sucursal_id.ToString+','+
                  'CURRENT_DATE,'''+clave_cliente+''','+importe.ToString+','+id_cliente.ToString+',''S'',''N'','+
                  '0,''CC'',''N'',''N'',''N'',''N'',False,''N'',''N'',''N'',''N'','''+descripcion+''') RETURNING DOCTO_CC_ID');

for i := 1 to stgCargoAcreditados.RowCount-2 do
   begin
      if StrToFloat(stgCargoAcreditados.Cells[4,i]) > 0 then
      begin
        id_cc_acr:=Conexion.ExecSQLScalar('SELECT DOCTO_CC_ID FROM DOCTOS_CC WHERE FOLIO='''+stgCargoAcreditados.Cells[1,i]+'''');
        Conexion.ExecSQL('INSERT INTO IMPORTES_DOCTOS_CC(IMPTE_DOCTO_CC_ID,DOCTO_CC_ID,DOCTO_CC_ACR_ID,FECHA,APLICADO,ESTATUS,'+
                      'IMPORTE,IMPUESTO,IVA_RETENIDO,ISR_RETENIDO,DSCTO_PPAG,PCTJE_COMIS_COB,TIPO_IMPTE)'+
                      'VALUES(-1,'+id_cc.ToString+','+id_cc_acr.ToString+',CURRENT_DATE,''S'',''N'','+stgCargoAcreditados.Cells[4,i]+',0,0,0,0,0,''R'')');
      end;

   end;

consecutivo:=consecutivo+1;
Conexion.ExecSQL('Update folios_conceptos set consecutivo='+consecutivo.ToString+' where concepto_id='+id_concepto.ToString+'');


id_cliente_saldo:=Conexion.ExecSQLScalar('SELECT CLIENTE_ID FROM SALDOS_CC WHERE CLIENTE_ID='+id_cliente.ToString+' AND ANO='+a�o.ToString+' AND MES='+mes.ToString+' AND ULTIMO_DIA='+dia.ToString+'');

{if id_cliente_saldo <> 0 then
  begin
    saldo:=Conexion.ExecSQLScalar('SELECT CREDITOS_CXC FROM SALDOS_CC WHERE CLIENTE_ID='+id_cliente.ToString+' AND ANO='+a�o.ToString+' AND MES='+mes.ToString+' AND ULTIMO_DIA='+dia.ToString+'');
    saldo:=saldo+importe;
    Conexion.ExecSQL('UPDATE SALDOS_CC SET CREDITOS_CXC='+saldo.ToString+'WHERE CLIENTE_ID='+id_cliente.ToString+' AND ANO='+a�o.ToString+' AND MES='+mes.ToString+' AND ULTIMO_DIA='+dia.ToString+'');
  end
else
  begin
    Conexion.ExecSQL('INSERT INTO SALDOS_CC(CLIENTE_ID,ANO,MES,ULTIMO_DIA,CREDITOS_CXC,CARGOS_XACR,CREDITOS_CXC,'
                  +'CREDITOS_XACR,CARGOS_CXC_XLIB,CREDITOS_XACR_XLIB)'
                  +'VALUES('+id_cliente.ToString+','+a�o.ToString+','+mes.ToString+','+dia.ToString+','
                  +importe.ToString+',0,0,0,0,0)');
  end; }


ShowMessage('Cobro generado correctamente');

if folio_terreno='C' then Close
else if folio_terreno='N' then
  begin
    formatogrid;
    txtDescripcion.Text:='';
    txtImporte.Text:='';
    txtTotal.Text:='';
    txtSaldoCliente.Text:='';
    qryCliente.Refresh;
    qryFormaCobro.Refresh;
    qryConceptos.Refresh;
    PageControl1.ActivePage:=TabSheet1;
    CargarDatos('P',0);
    Totales;
    txtSaldoCliente.Text:=Conexion.ExecSQLScalar('EXECUTE PROCEDURE CALC_SALDO_CLIENTE('+qryCliente.FieldByName('CLIENTE_ID').AsString+',current_date,''N'')');
  end
else
  begin
    TabSheet1.Enabled:=False;
    TabSheet2.Enabled:=False;
  end;


Except
Conexion.Rollback;
ShowMessage('Ocurrio un error');
end;
end;
procedure TFormCobro.cboClienteChange(Sender: TObject);
begin
CargarDatos('P',0);
txtSaldoCliente.Text:=Conexion.ExecSQLScalar('EXECUTE PROCEDURE CALC_SALDO_CLIENTE('+qryCliente.FieldByName('CLIENTE_ID').AsString+',current_date,''N'')');
end;

end.
