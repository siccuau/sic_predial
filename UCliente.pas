unit UCliente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, Data.Bind.EngExt, Vcl.Bind.DBEngExt,
  System.Rtti, System.Bindings.Outputs, Vcl.Bind.Editors, Data.Bind.Components,UImportaClientes,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.Bind.DBScope,
  Vcl.StdCtrls, Vcl.Menus,UCampoNMA,UNuevoMunicipio,UCallesNuevo;

type
  TClienteForm = class(TForm)
    lblNombre: TLabel;
    lblApPatero: TLabel;
    lblApMaterno: TLabel;
    lblRepLegal: TLabel;
    lblTelefono: TLabel;
    lblMunicipio: TLabel;
    lblCampoNo: TLabel;
    lblCasaNo: TLabel;
    lblFraccionamiento: TLabel;
    lblCalle: TLabel;
    lblAvenida: TLabel;
    lblRfc: TLabel;
    txtRfc: TEdit;
    txtNombre: TEdit;
    txtApPaterno: TEdit;
    txtApMaterno: TEdit;
    txtTelefono: TEdit;
    txtRepLegal: TEdit;
    txtCasaNo: TEdit;
    txtFraccionamiento: TEdit;
    txtAvenida: TEdit;
    txtCalle: TEdit;
    cboCampoNo: TComboBox;
    btnGuardar: TButton;
    Conexion: TFDConnection;
    BindSourceDB1: TBindSourceDB;
    qryMunicipio: TFDQuery;
    qryCampos: TFDQuery;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    popCampo: TPopupMenu;
    Nuevo1: TMenuItem;
    BindSourceDB2: TBindSourceDB;
    LinkListControlToField2: TLinkListControlToField;
    chkCorredor: TCheckBox;
    popMunicipio: TPopupMenu;
    Nuevo2: TMenuItem;
    cboCalles: TComboBox;
    popCalles: TPopupMenu;
    Nuevo3: TMenuItem;
    qryCalles: TFDQuery;
    qryCallesID_CALLES: TIntegerField;
    qryCallesNOMBRE: TStringField;
    BindSourceDB3: TBindSourceDB;
    LinkListControlToField3: TLinkListControlToField;
    qryMunicipioCIUDAD_ID: TIntegerField;
    qryMunicipioNOMBRE: TStringField;
    qryMunicipioCLAVE_FISCAL: TStringField;
    qryMunicipioES_PREDET: TStringField;
    qryMunicipioESTADO_ID: TIntegerField;
    qryMunicipioUSUARIO_CREADOR: TStringField;
    qryMunicipioFECHA_HORA_CREACION: TSQLTimeStampField;
    qryMunicipioUSUARIO_AUT_CREACION: TStringField;
    qryMunicipioUSUARIO_ULT_MODIF: TStringField;
    qryMunicipioFECHA_HORA_ULT_MODIF: TSQLTimeStampField;
    qryMunicipioUSUARIO_AUT_MODIF: TStringField;
    cboMunicipio: TComboBox;
    qryCliente: TFDQuery;
    qryDireccionCliente: TFDQuery;
    Modificar1: TMenuItem;
    MainMenu1: TMainMenu;
    Herramientas1: TMenuItem;
    Importardesde1: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure Nuevo1Click(Sender: TObject);
    procedure Nuevo2Click(Sender: TObject);
    procedure Nuevo3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Modificar1Click(Sender: TObject);
    procedure chkCorredorClick(Sender: TObject);
    procedure Importardesde1Click(Sender: TObject);
  private
    { Private declarations }
  public
    cliente_id:Integer;
    { Public declarations }
  end;

var
  ClienteForm: TClienteForm;

implementation

{$R *.dfm}

procedure TClienteForm.btnGuardarClick(Sender: TObject);
var
nombreCompleto,rfc,contacto1,telefono,municipio,colonia,calle,fraccionamiento,calle_completo,corredor:string;
id_cliente,cond_pago_id:Integer;
begin
 nombreCompleto:=UpperCase(txtNombre.Text)+'('+UpperCase(txtRfc.Text)+')';
rfc:=UpperCase(txtRfc.Text);
contacto1:=UpperCase(txtRepLegal.Text);
telefono:=txtTelefono.Text;
municipio:=qryMunicipio.FieldByName('CIUDAD_ID').AsString;
colonia:=qryCampos.FieldByName('NOMBRE').AsString +'-'+txtFraccionamiento.Text;
if cboCalles.Text='' then calle:=txtAvenida.Text
else if txtAvenida.Text='' then calle:=cboCalles.Text
else if (cboCalles.Text='') and (txtAvenida.Text='')  then calle:=''
else calle:= cboCalles.Text+' y '+txtAvenida.Text;

calle_completo:=calle +' '+ txtCasaNo.Text + ' '+ colonia;
if chkCorredor.Checked then corredor:='CORREDOR'
else corredor:='';
if cliente_id<>0 then
  begin
    try
      Conexion.ExecSQL('UPDATE CLIENTES SET NOMBRE='''+nombreCompleto+''' , CONTACTO1='''+contacto1+''' WHERE CLIENTE_ID='+cliente_id.ToString+'');
      Conexion.ExecSQL('UPDATE DIRS_CLIENTES SET REFERENCIA='''+corredor+''' , TELEFONO1='''+telefono+''' , RFC_CURP='''+rfc+''' , CALLE='''+calle_completo+''' , NOMBRE_CALLE='''+calle+''' , COLONIA='''+colonia+''' , CIUDAD_ID='''+municipio+''' , NUM_EXTERIOR='''+txtCasaNo.Text+''' WHERE CLIENTE_ID='+cliente_id.ToString+' AND ES_DIR_PPAL=''S'' ');
      ShowMessage('Cliente modificado correctamente');
      Close;
    Except on E : Exception do

        ShowMessage(E.Message);
    end;
  end
else
  begin
    if (txtNombre.Text= '')  or (rfc= '') or (telefono= '') or (municipio= '')  then
    begin
      ShowMessage('Faltan campos por rellenar');
    end
    else
    begin
      try
        cond_pago_id:=Conexion.ExecSQLScalar('select cond_pago_id from condiciones_pago where upper(nombre)=''CONTADO''');
        id_cliente:=Conexion.ExecSQLScalar('INSERT INTO CLIENTES(CLIENTE_ID,MONEDA_ID,COND_PAGO_ID,NOMBRE,CONTACTO1) VALUES(-1,1,'''+inttostr(cond_pago_id)+''','''+nombreCompleto+''','''+contacto1+''') RETURNING CLIENTE_ID');
        Conexion.ExecSQL('INSERT INTO DIRS_CLIENTES(DIR_CLI_ID,ES_DIR_PPAL,REFERENCIA,TELEFONO1,RFC_CURP,CALLE,CLIENTE_ID,NOMBRE_CONSIG,NOMBRE_CALLE,COLONIA,CIUDAD_ID,NUM_EXTERIOR) VALUES(-1,''S'','''+corredor+''','''+telefono+''','''+rfc+''','''+calle_completo+''','+id_cliente.ToString+',''Direccion Principal'','''+calle+''','''+colonia+''','+municipio+','''+txtCasaNo.Text+''')');
        ShowMessage('Cliente agregado correctamente');
        Close;
      Except on E : Exception do

        ShowMessage(E.Message);
      end;
    end;
  end;



end;

procedure TClienteForm.chkCorredorClick(Sender: TObject);
begin
if chkCorredor.Checked then
  begin
    cboCalles.Text:='Corredor';
  end
else
  begin
    cboCalles.Text:='';
  end;
end;

procedure TClienteForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
qryMunicipio.Close;
qryCampos.Close;
qryCalles.Close;
end;

procedure TClienteForm.FormShow(Sender: TObject);
var
calle,colonia,replace,rfc,nombre,referencia:string;
col,call: TArray<string>;
begin
     //SIC_PREDIAL_CAMPOS
  conexion.ExecSQL('execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_PREDIAL_CAMPOS '')) then'+
    ' BEGIN'+
        ' execute statement ''CREATE TABLE SIC_PREDIAL_CAMPOS ('+
                                ' ID_CAMPOS          INTEGER,'+
                                ' NOMBRE       VARCHAR(50)'+
                            ' );'';'+
     ' execute statement ''CREATE GENERATOR SIC_ID_CAMPOS'';'+
        ' execute statement ''set GENERATOR SIC_ID_CAMPOS TO 0;'';'+
        ' execute statement ''CREATE OR ALTER TRIGGER SIC_PREDIAL_CAMPOS_BIO FOR SIC_PREDIAL_CAMPOS '+
                                ' ACTIVE BEFORE INSERT POSITION 0'+
                                ' AS'+
                                ' begin'+
                                  ' /* Genera un nuevo id */'+
                                  ' if (new.id_campos = -1) then'+
                                    ' new.id_campos = gen_id(sic_id_campos,1);'+
                                ' end'';'+
    ' END end');
qryMunicipio.Open();
qryCampos.Open();
qryCalles.Open();
//ShowMessage(cliente_id.ToString);

if cliente_id<>0 then
  begin
    qryCliente.SQL.Text:='SELECT * FROM CLIENTES WHERE CLIENTE_ID=:cliente_id';
    qryCliente.ParamByName('cliente_id').Value:=cliente_id;
    qryCliente.Open();
    qryDireccionCliente.SQL.Text:='SELECT * FROM DIRS_CLIENTES WHERE CLIENTE_ID=:cliente_id AND ES_DIR_PPAL=''S'' ';
    qryDireccionCliente.ParamByName('cliente_id').Value:=cliente_id;
    qryDireccionCliente.Open();
    nombre:=qryCliente.FieldByName('NOMBRE').Value;
    rfc:=qryDireccionCliente.FieldByName('RFC_CURP').Value;
    replace:= '('+qryDireccionCliente.FieldByName('RFC_CURP').Value+')';
    nombre:=StringReplace(nombre,replace,'',[rfReplaceAll, rfIgnoreCase]);


    txtNombre.Text:=nombre;
    txtRepLegal.Text:=qryCliente.FieldByName('CONTACTO1').Value;

    txtRfc.Text:=rfc;
    txtTelefono.Text:=qryDireccionCliente.FieldByName('TELEFONO1').Value;
    referencia:= qryDireccionCliente.FieldByName('REFERENCIA').Value;

    if referencia <> '' then chkCorredor.Checked:=True
    else chkCorredor.Checked:=False;

    calle:= qryDireccionCliente.FieldByName('NOMBRE_CALLE').Value;
    call:=calle.Split(['y']);
    if Length(call)>1 then
      begin
        cboCalles.Text:=call[0];
        txtAvenida.Text:=call[1];
      end
      else if calle='' then cboCalles.Text:='' else cboCalles.Text:=call[0];
    colonia:= qryDireccionCliente.FieldByName('COLONIA').Value;
    col:=colonia.Split(['-']);
    if Length(col)>1 then
      begin
        cboCampoNo.Text:=col[0];
        txtFraccionamiento.Text:=col[1];
      end
    else cboCampoNo.Text:=col[0];
    txtCasaNo.Text:=qryDireccionCliente.FieldByName('NUM_EXTERIOR').Value;


  end;
end;

procedure TClienteForm.Importardesde1Click(Sender: TObject);
var
FormExcel:TImportaClientes;
begin
  FormExcel:= TImportaClientes.Create(nil);
  FormExcel.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormExcel.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormExcel.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormExcel.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormExcel.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  FormExcel.ShowModal;
end;

procedure TClienteForm.Modificar1Click(Sender: TObject);
var
FormNuevaCalle:TFormCallesNuevo;
begin
  FormNuevaCalle:= TFormCallesNuevo.Create(nil);
  FormNuevaCalle.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormNuevaCalle.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormNuevaCalle.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormNuevaCalle.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormNuevaCalle.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  FormNuevaCalle.calle_id:=qryCalles.FieldByName('ID_CALLES').Value;
  //TFormCampoNMA.tipo:='Marca';
  FormNuevaCalle.ShowModal;
  qryCalles.Refresh;
end;

procedure TClienteForm.Nuevo1Click(Sender: TObject);
var
FormCampoNMA:TFormCampoNMA;
begin
  FormCampoNMA:= TFormCampoNMA.Create(nil);
  FormCampoNMA.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormCampoNMA.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormCampoNMA.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormCampoNMA.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormCampoNMA.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  //TFormCampoNMA.tipo:='Marca';
  FormCampoNMA.ShowModal;
  qryCampos.Refresh;
end;

procedure TClienteForm.Nuevo2Click(Sender: TObject);
var
FormNuevoMunicipio:TFormNuevoMunicipio;
begin
  FormNuevoMunicipio:= TFormNuevoMunicipio.Create(nil);
  FormNuevoMunicipio.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormNuevoMunicipio.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormNuevoMunicipio.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormNuevoMunicipio.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormNuevoMunicipio.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  //TFormCampoNMA.tipo:='Marca';
  FormNuevoMunicipio.ShowModal;
  qryMunicipio.Refresh;
end;

procedure TClienteForm.Nuevo3Click(Sender: TObject);
var
FormNuevaCalle:TFormCallesNuevo;
begin
  FormNuevaCalle:= TFormCallesNuevo.Create(nil);
  FormNuevaCalle.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormNuevaCalle.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormNuevaCalle.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormNuevaCalle.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormNuevaCalle.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  //TFormCampoNMA.tipo:='Marca';
  FormNuevaCalle.ShowModal;
  qryCalles.Refresh;

end;

end.
