object CambioFolio: TCambioFolio
  Left = 0
  Top = 0
  Caption = 'CambioFolio'
  ClientHeight = 264
  ClientWidth = 428
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 16
    Width = 71
    Height = 25
    Caption = 'Cliente:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 45
    Top = 63
    Width = 130
    Height = 25
    Caption = 'Folio anterior:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 149
    Top = 98
    Width = 96
    Height = 25
    Caption = 'Cambiar a'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 62
    Top = 129
    Width = 113
    Height = 25
    Caption = 'Folio nuevo:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object cbCliente: TDBLookupComboBox
    Left = 101
    Top = 16
    Width = 316
    Height = 27
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyField = 'CLIENTE_ID'
    ListField = 'NOMBRE'
    ListSource = dsClientes
    ParentFont = False
    TabOrder = 0
  end
  object txtFolio: TEdit
    Left = 181
    Top = 65
    Width = 188
    Height = 27
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Text = 'Edit1'
  end
  object btnAceptar: TButton
    Left = 56
    Top = 173
    Width = 113
    Height = 45
    Caption = 'Aceptar'
    TabOrder = 2
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 214
    Top = 173
    Width = 113
    Height = 45
    Caption = 'Cancelar'
    TabOrder = 3
    OnClick = btnCancelarClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 245
    Width = 428
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object txtFolioNuevo: TEdit
    Left = 181
    Top = 129
    Width = 188
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'Protocol=TCPIP'
      'Server=localhost'
      'CharacterSet=ISO8859_1'
      'RoleName=USUARIO_MICROSIP'
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    UpdateOptions.AssignedValues = [uvAutoCommitUpdates]
    UpdateOptions.AutoCommitUpdates = True
    LoginPrompt = False
    Left = 379
    Top = 144
  end
  object qryClientes: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from clientes')
    Left = 347
    Top = 192
  end
  object dsClientes: TDataSource
    DataSet = qryClientes
    Left = 347
    Top = 200
  end
end
