﻿object CatalogoImpuestos: TCatalogoImpuestos
  Left = 0
  Top = 0
  Caption = 'Catalogo Impuestos'
  ClientHeight = 189
  ClientWidth = 358
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblNombreImpuesto: TLabel
    Left = 16
    Top = 8
    Width = 41
    Height = 13
    Caption = 'Nombre:'
  end
  object lblAño: TLabel
    Left = 224
    Top = 8
    Width = 23
    Height = 13
    Caption = 'A'#241'o:'
  end
  object lblMedida: TLabel
    Left = 16
    Top = 64
    Width = 38
    Height = 13
    Caption = 'Medida:'
  end
  object lblValor: TLabel
    Left = 224
    Top = 64
    Width = 28
    Height = 13
    Caption = 'Valor:'
  end
  object txtAño: TEdit
    Left = 224
    Top = 27
    Width = 121
    Height = 21
    MaxLength = 4
    NumbersOnly = True
    TabOrder = 0
  end
  object txtValor: TEdit
    Left = 224
    Top = 83
    Width = 121
    Height = 21
    NumbersOnly = True
    TabOrder = 1
  end
  object btnGuardar: TButton
    Left = 270
    Top = 136
    Width = 75
    Height = 25
    Caption = 'Guardar'
    TabOrder = 2
    OnClick = btnGuardarClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 170
    Width = 358
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object txtNombre: TEdit
    Left = 16
    Top = 27
    Width = 145
    Height = 21
    TabOrder = 4
  end
  object cboMedida: TDBLookupComboBox
    Left = 16
    Top = 83
    Width = 145
    Height = 21
    KeyField = 'UNIDAD_VENTA_ID'
    ListField = 'UNIDAD_VENTA'
    ListSource = dsUnidades
    PopupMenu = popmnuConcepto
    TabOrder = 5
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    LoginPrompt = False
    Left = 16
    Top = 185
  end
  object qryUnidades: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from unidades_venta')
    Left = 72
    Top = 184
  end
  object dsUnidades: TDataSource
    DataSet = qryUnidades
    Left = 112
    Top = 184
  end
  object popmnuConcepto: TPopupMenu
    Left = 58
    Top = 117
    object Nuevo1: TMenuItem
      Caption = 'Nuevo'
      OnClick = Nuevo1Click
    end
  end
end
