unit UValoresTerrenos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  Vcl.StdCtrls, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids,DateUtils,
  System.Rtti, System.Bindings.Outputs, Vcl.Bind.Editors, Data.Bind.EngExt,
  Vcl.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope;

type
  TFormValoresTerrenos = class(TForm)
    lblTipoTerreno: TLabel;
    cboTipoTerreno: TComboBox;
    lblA�o: TLabel;
    txtA�o: TEdit;
    cboMedida: TComboBox;
    lblMedida: TLabel;
    lblValor: TLabel;
    txtValor: TEdit;
    btnGuardar: TButton;
    Conexion: TFDConnection;
    DBGrid1: TDBGrid;
    StatusBar1: TStatusBar;
    qryValores: TFDQuery;
    dsValores: TDataSource;
    dtpYear: TDateTimePicker;
    qryMunicipio: TFDQuery;
    cboMun: TComboBox;
    lblMunicipio: TLabel;
    qryMunicipioCIUDAD_ID: TIntegerField;
    qryMunicipioNOMBRE: TStringField;
    qryMunicipioCLAVE_FISCAL: TStringField;
    qryMunicipioES_PREDET: TStringField;
    qryMunicipioESTADO_ID: TIntegerField;
    qryMunicipioUSUARIO_CREADOR: TStringField;
    qryMunicipioFECHA_HORA_CREACION: TSQLTimeStampField;
    qryMunicipioUSUARIO_AUT_CREACION: TStringField;
    qryMunicipioUSUARIO_ULT_MODIF: TStringField;
    qryMunicipioFECHA_HORA_ULT_MODIF: TSQLTimeStampField;
    qryMunicipioUSUARIO_AUT_MODIF: TStringField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    qryValoresTIPO_TERRENO: TStringField;
    qryValoresANO: TStringField;
    qryValoresMEDIDA: TStringField;
    qryValoresVALOR: TIntegerField;
    qryValoresMUNICIPIO: TStringField;
    cboMunicipio: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure cboTipoTerrenoChange(Sender: TObject);
    procedure dtpYearChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormValoresTerrenos: TFormValoresTerrenos;

implementation

{$R *.dfm}

procedure TFormValoresTerrenos.btnGuardarClick(Sender: TObject);
var
valores_terrenos:Integer;
buttonSelected : Integer;
begin
if (txtA�o.Text='') or (txtValor.Text='') or (cboTipoTerreno.Text='')or (cboMedida.Text='')  then
  begin
    ShowMessage('Faltan campos');
  end
else
  begin
    try
    valores_terrenos:=Conexion.ExecSQLScalar('SELECT VALOR FROM SIC_PREDIAL_VALORES WHERE ANO='''+txtA�o.Text+''' AND TIPO_TERRENO='''+cboTipoTerreno.Text+''' AND MUNICIPIO='''+cboMunicipio.Text+''' ');
    if valores_terrenos = 0 then
      begin
        Conexion.ExecSQL('INSERT INTO SIC_PREDIAL_VALORES(ID_VALORES,TIPO_TERRENO,ANO,MEDIDA,VALOR,MUNICIPIO) VALUES(-1,'''+cboTipoTerreno.Text+''','''+txtA�o.Text+''','''+cboMedida.Text+''','+txtValor.Text+','''+cboMunicipio.Text+''')');
        ShowMessage('Valores agregados');
      end
    else
      begin
        //ShowMessage('Ya existe un valor registrado para '+cboTipoTerreno.Text+' del a�o '+txtA�o.Text+'');
        buttonSelected := messagedlg('Ya existe un valor registrado para '+cboTipoTerreno.Text+' del a�o '+txtA�o.Text+' y del municipio '+cboMunicipio.Text+' quiere actualizarlo?',mtError, mbOKCancel, 0);
        if buttonSelected = mrOK     then
        begin
          Conexion.ExecSQL('UPDATE SIC_PREDIAL_VALORES SET VALOR='''+txtValor.Text+''' WHERE ANO='''+txtA�o.Text+''' AND TIPO_TERRENO='''+cboTipoTerreno.Text+''' AND MUNICIPIO='''+cboMedida.Text+'''');
          ShowMessage('Valores actualizados');
          qryValores.Refresh;
        end;
        if buttonSelected = mrCancel then Close;
      end;
    
    Except
      ShowMessage('El campo valor debe ser numerico');
    end;
  end;

end;

procedure TFormValoresTerrenos.cboTipoTerrenoChange(Sender: TObject);
begin
if cboTipoTerreno.Text<>'' then
  begin
    qryValores.Filter := ' TIPO_TERRENO= '''+cboTipoTerreno.Text+'''';
    qryValores.Filtered := True;
  end;
end;

procedure TFormValoresTerrenos.dtpYearChange(Sender: TObject);
begin
txtA�o.Text:=YearOf(dtpYear.Date).ToString;
if txtA�o.Text<>'' then
  begin
    qryValores.Filter := ' ANO= '''+txtA�o.Text+'''';
    qryValores.Filtered := True;
  end;

end;

procedure TFormValoresTerrenos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
qryValores.Close;
qryMunicipio.Close;
end;

procedure TFormValoresTerrenos.FormShow(Sender: TObject);
begin
qryMunicipio.Open();
qryValores.Open();
txtA�o.Text:=YearOf(dtpYear.Date).ToString;

end;

end.
