unit UReportes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.ComCtrls,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.DBCtrls, frxClass,
  frxDBSet;

type
  TReportes = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    cbCliente: TDBLookupComboBox;
    Label1: TLabel;
    dsClientes: TDataSource;
    qryClientes: TFDQuery;
    qryClientesCLIENTE_ID: TIntegerField;
    qryClientesNOMBRE: TStringField;
    qryClientesCONTACTO1: TStringField;
    qryClientesCONTACTO2: TStringField;
    qryClientesESTATUS: TStringField;
    qryClientesCAUSA_SUSP: TStringField;
    qryClientesFECHA_SUSP: TDateField;
    qryClientesCOBRAR_IMPUESTOS: TStringField;
    qryClientesRETIENE_IMPUESTOS: TStringField;
    qryClientesSUJETO_IEPS: TStringField;
    qryClientesGENERAR_INTERESES: TStringField;
    qryClientesEMITIR_EDOCTA: TStringField;
    qryClientesDIFERIR_CFDI_COBROS: TBooleanField;
    qryClientesLIMITE_CREDITO: TFMTBCDField;
    qryClientesMONEDA_ID: TIntegerField;
    qryClientesCOND_PAGO_ID: TIntegerField;
    qryClientesTIPO_CLIENTE_ID: TIntegerField;
    qryClientesZONA_CLIENTE_ID: TIntegerField;
    qryClientesCOBRADOR_ID: TIntegerField;
    qryClientesVENDEDOR_ID: TIntegerField;
    qryClientesNOTAS: TMemoField;
    qryClientesCUENTA_CXC: TStringField;
    qryClientesCUENTA_ANTICIPOS: TStringField;
    qryClientesFORMATOS_EMAIL: TStringField;
    qryClientesRECEPTOR_CFD: TStringField;
    qryClientesNUM_PROV_CLIENTE: TStringField;
    qryClientesCAMPOS_ADDENDA: TMemoField;
    qryClientesUSUARIO_CREADOR: TStringField;
    qryClientesFECHA_HORA_CREACION: TSQLTimeStampField;
    qryClientesUSUARIO_AUT_CREACION: TStringField;
    qryClientesUSUARIO_ULT_MODIF: TStringField;
    qryClientesFECHA_HORA_ULT_MODIF: TSQLTimeStampField;
    qryClientesUSUARIO_AUT_MODIF: TStringField;
    GroupBox1: TGroupBox;
    DateTimePicker1: TDateTimePicker;
    Label2: TLabel;
    DateTimePicker2: TDateTimePicker;
    Label3: TLabel;
    btnHistMov: TButton;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    DateTimePicker3: TDateTimePicker;
    DateTimePicker4: TDateTimePicker;
    btnHistTrasp: TButton;
    GroupBox3: TGroupBox;
    btnConsolidado: TButton;
    GroupBox4: TGroupBox;
    btnPredialAnterior: TButton;
    GroupBox5: TGroupBox;
    Button1: TButton;
    RadioGroup1: TRadioGroup;
    cbZona: TDBLookupComboBox;
    GroupBox6: TGroupBox;
    Button2: TButton;
    qryHistorial: TFDQuery;
    qryHistorialID: TIntegerField;
    qryHistorialFOLIO: TStringField;
    qryHistorialCLIENTE_ID: TIntegerField;
    qryHistorialCONCEPTO: TStringField;
    qryHistorialVALOR_ANTERIOR: TStringField;
    qryHistorialVALOR_NUEVO: TStringField;
    qryHistorialMETROS: TFMTBCDField;
    qryHistorialUSUARIO: TStringField;
    qryHistorialFECHA_HORA: TSQLTimeStampField;
    qryHistorialNOMBRE: TStringField;
    dssHistorial: TDataSource;
    rptHistorial: TfrxReport;
    qryHistorialTraslados: TFDQuery;
    qryHistorialTrasladosID: TIntegerField;
    qryHistorialTrasladosFOLIO: TStringField;
    qryHistorialTrasladosCONCEPTO: TStringField;
    qryHistorialTrasladosCLIENTE1: TStringField;
    qryHistorialTrasladosCLIENTE2: TStringField;
    qryHistorialTrasladosNOMBRE: TStringField;
    qryHistorialTrasladosFECHA_HORA: TSQLTimeStampField;
    qryHistorialTrasladosMETROS: TFMTBCDField;
    rptHistorialTraslados: TfrxReport;
    dssHistorialTraslados: TDataSource;
    frxDBDataset1: TfrxDBDataset;
    frxDBDataset2: TfrxDBDataset;
    qryPredialPend: TFDQuery;
    rptPredialPend: TfrxReport;
    dsPredialPend: TDataSource;
    frxDBDataset3: TfrxDBDataset;
    qryCampos: TFDQuery;
    dsCampos: TDataSource;
    qryAuxiliar: TFDQuery;
    rptAuxiliar: TfrxReport;
    dsAuxiliar: TDataSource;
    frxDBDataset4: TfrxDBDataset;
    qryAuxiliarCLIENTE_ID: TIntegerField;
    qryAuxiliarDOCTO_CC_ID: TIntegerField;
    qryAuxiliarFECHA: TDateField;
    qryAuxiliarNOMBRE_CONCEP: TStringField;
    qryAuxiliarFOLIO: TStringField;
    qryAuxiliarDESCRIPCION: TStringField;
    qryAuxiliarIMPORTE_CARGO: TFMTBCDField;
    qryAuxiliarIMPORTE_CREDITO: TFMTBCDField;
    qryAuxiliarSALDO_FIN: TFMTBCDField;
    qryAuxiliarMOVTO_ESPECIAL: TStringField;
    qryAuxiliarCLIENTE_ID_1: TIntegerField;
    qryAuxiliarNOMBRE: TStringField;
    qryAuxiliarCONTACTO1: TStringField;
    qryAuxiliarCONTACTO2: TStringField;
    qryAuxiliarESTATUS: TStringField;
    qryAuxiliarCAUSA_SUSP: TStringField;
    qryAuxiliarFECHA_SUSP: TDateField;
    qryAuxiliarCOBRAR_IMPUESTOS: TStringField;
    qryAuxiliarRETIENE_IMPUESTOS: TStringField;
    qryAuxiliarSUJETO_IEPS: TStringField;
    qryAuxiliarGENERAR_INTERESES: TStringField;
    qryAuxiliarEMITIR_EDOCTA: TStringField;
    qryAuxiliarDIFERIR_CFDI_COBROS: TBooleanField;
    qryAuxiliarLIMITE_CREDITO: TFMTBCDField;
    qryAuxiliarMONEDA_ID: TIntegerField;
    qryAuxiliarCOND_PAGO_ID: TIntegerField;
    qryAuxiliarTIPO_CLIENTE_ID: TIntegerField;
    qryAuxiliarZONA_CLIENTE_ID: TIntegerField;
    qryAuxiliarCOBRADOR_ID: TIntegerField;
    qryAuxiliarVENDEDOR_ID: TIntegerField;
    qryAuxiliarNOTAS: TMemoField;
    qryAuxiliarCUENTA_CXC: TStringField;
    qryAuxiliarCUENTA_ANTICIPOS: TStringField;
    qryAuxiliarFORMATOS_EMAIL: TStringField;
    qryAuxiliarRECEPTOR_CFD: TStringField;
    qryAuxiliarNUM_PROV_CLIENTE: TStringField;
    qryAuxiliarCAMPOS_ADDENDA: TMemoField;
    qryAuxiliarUSUARIO_CREADOR: TStringField;
    qryAuxiliarFECHA_HORA_CREACION: TSQLTimeStampField;
    qryAuxiliarUSUARIO_AUT_CREACION: TStringField;
    qryAuxiliarUSUARIO_ULT_MODIF: TStringField;
    qryAuxiliarFECHA_HORA_ULT_MODIF: TSQLTimeStampField;
    qryAuxiliarUSUARIO_AUT_MODIF: TStringField;
    cbA�o: TComboBox;
    qryAuxiliar2: TFDQuery;
    dsAuxiliar2: TDataSource;
    frxDBDataset5: TfrxDBDataset;
    qryAuxiliar2CLIENTE_ID: TIntegerField;
    qryAuxiliar2DOCTO_CC_ID: TIntegerField;
    qryAuxiliar2FECHA: TDateField;
    qryAuxiliar2NOMBRE_CONCEP: TStringField;
    qryAuxiliar2FOLIO: TStringField;
    qryAuxiliar2DESCRIPCION: TStringField;
    qryAuxiliar2IMPORTE_CARGO: TFMTBCDField;
    qryAuxiliar2IMPORTE_CREDITO: TFMTBCDField;
    qryAuxiliar2SALDO_FIN: TFMTBCDField;
    qryAuxiliar2MOVTO_ESPECIAL: TStringField;
    qryAuxiliar2CLIENTE_ID_1: TIntegerField;
    qryAuxiliar2NOMBRE: TStringField;
    qryAuxiliar2CONTACTO1: TStringField;
    qryAuxiliar2CONTACTO2: TStringField;
    qryAuxiliar2ESTATUS: TStringField;
    qryAuxiliar2CAUSA_SUSP: TStringField;
    qryAuxiliar2FECHA_SUSP: TDateField;
    qryAuxiliar2COBRAR_IMPUESTOS: TStringField;
    qryAuxiliar2RETIENE_IMPUESTOS: TStringField;
    qryAuxiliar2SUJETO_IEPS: TStringField;
    qryAuxiliar2GENERAR_INTERESES: TStringField;
    qryAuxiliar2EMITIR_EDOCTA: TStringField;
    qryAuxiliar2DIFERIR_CFDI_COBROS: TBooleanField;
    qryAuxiliar2LIMITE_CREDITO: TFMTBCDField;
    qryAuxiliar2MONEDA_ID: TIntegerField;
    qryAuxiliar2COND_PAGO_ID: TIntegerField;
    qryAuxiliar2TIPO_CLIENTE_ID: TIntegerField;
    qryAuxiliar2ZONA_CLIENTE_ID: TIntegerField;
    qryAuxiliar2COBRADOR_ID: TIntegerField;
    qryAuxiliar2VENDEDOR_ID: TIntegerField;
    qryAuxiliar2NOTAS: TMemoField;
    qryAuxiliar2CUENTA_CXC: TStringField;
    qryAuxiliar2CUENTA_ANTICIPOS: TStringField;
    qryAuxiliar2FORMATOS_EMAIL: TStringField;
    qryAuxiliar2RECEPTOR_CFD: TStringField;
    qryAuxiliar2NUM_PROV_CLIENTE: TStringField;
    qryAuxiliar2CAMPOS_ADDENDA: TMemoField;
    qryAuxiliar2USUARIO_CREADOR: TStringField;
    qryAuxiliar2FECHA_HORA_CREACION: TSQLTimeStampField;
    qryAuxiliar2USUARIO_AUT_CREACION: TStringField;
    qryAuxiliar2USUARIO_ULT_MODIF: TStringField;
    qryAuxiliar2FECHA_HORA_ULT_MODIF: TSQLTimeStampField;
    qryAuxiliar2USUARIO_AUT_MODIF: TStringField;
    rpConsolidado: TfrxReport;
    dssConsolidado: TDataSource;
    qryConsolidado: TFDQuery;
    qryConsolidadoID_TERRENO: TIntegerField;
    qryConsolidadoTIPO: TStringField;
    qryConsolidadoCLIENTE_ID: TIntegerField;
    qryConsolidadoFOLIO: TStringField;
    qryConsolidadoZONA: TStringField;
    qryConsolidadoMTS_ESTE: TFMTBCDField;
    qryConsolidadoMTS_NORTE: TFMTBCDField;
    qryConsolidadoNUMERO: TIntegerField;
    qryConsolidadoMETROS: TFMTBCDField;
    qryConsolidadoACRES: TBCDField;
    qryConsolidadoCAMPO: TStringField;
    qryConsolidadoDIRECCION: TStringField;
    qryConsolidadoMUNICIPIO: TStringField;
    qryConsolidadoTRASPASO: TStringField;
    qryConsolidadoNOMBRE: TStringField;
    qryConsolidadoRFC_CURP: TStringField;
    qryConsolidadoTELEFONO1: TStringField;
    qryConsolidadoFECHA: TDateField;
    qryConsolidadoNOTAS: TMemoField;
    dsConsolidado: TfrxDBDataset;
    frxReport1: TfrxReport;
    procedure FormShow(Sender: TObject);
    procedure btnHistMovClick(Sender: TObject);
    procedure btnHistTraspClick(Sender: TObject);
    procedure btnConsolidadoClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure btnPredialAnteriorClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Reportes: TReportes;

implementation

{$R *.dfm}

procedure TReportes.btnConsolidadoClick(Sender: TObject);
begin
 qryConsolidado.ParamByName('cliente').Value:=cbCliente.KeyValue;
 qryConsolidado.Open();
 qryConsolidado.Refresh;
 rpConsolidado.PrepareReport;
 rpConsolidado.ShowPreparedReport;
end;

procedure TReportes.btnHistMovClick(Sender: TObject);
begin
qryHistorial.ParamByName('cli').Value:=cbCliente.KeyValue;
qryHistorial.ParamByName('f_fini').Value:=FormatDateTime('mm/dd/yyyy',DateTimePicker1.Date);
qryHistorial.ParamByName('f_ffin').Value:=FormatDateTime('mm/dd/yyyy',DateTimePicker2.Date);
qryHistorial.Open();
qryHistorial.Refresh;
rptHistorial.PrepareReport;
rptHistorial.ShowPreparedReport;
end;

procedure TReportes.btnHistTraspClick(Sender: TObject);
begin
qryHistorialTraslados.ParamByName('cli').Value:=cbCliente.KeyValue;
qryHistorialTraslados.ParamByName('F_FINI').Value:=FormatDateTime('mm/dd/yyyy',DateTimePicker3.Date);
qryHistorialTraslados.ParamByName('F_FFIN').Value:=FormatDateTime('mm/dd/yyyy',DateTimePicker4.Date);
qryHistorialTraslados.Open();
qryHistorialTraslados.Refresh;
rptHistorialTraslados.PrepareReport;
rptHistorialTraslados.ShowPreparedReport;
end;

procedure TReportes.btnPredialAnteriorClick(Sender: TObject);
begin
  qryPredialPend.SQL.Text:='select cc.*,c.nombre,dc.colonia from get_cargos_cc(current_date, ''P'',0,''S'',''S'',''N'') cc'+
' join conceptos_cc c on c.concepto_cc_id=cc.concepto_cc_id'+
' join dirs_clientes dc on dc.cliente_id=cc.cliente_id'+
' where UPPER(c.nombre)=''PREDIAL'' and cc.cliente_id=:cid and fecha<=:F_FECHA';
  qryPredialPend.ParamByName('cid').Value:=cbCliente.KeyValue;
  qryPredialPend.ParamByName('F_FECHA').Value:='12/31/'+cbA�o.Text;
  qryPredialPend.Open();
  qryPredialPend.Refresh;
  rptPredialPend.PrepareReport;
  rptPredialPend.ShowPreparedReport;
end;

procedure TReportes.Button1Click(Sender: TObject);
begin
if RadioGroup1.ItemIndex=0 then
  begin
  qryPredialPend.Open();
  qryPredialPend.Refresh;
  rptPredialPend.PrepareReport;
  rptPredialPend.ShowPreparedReport;
  end;

  if RadioGroup1.ItemIndex=1 then
  begin
  qryPredialPend.SQL.Text:='select cc.*,z.nombre as zona,c.nombre,dc.colonia from get_cargos_cc(current_date, ''P'',0,''S'',''S'',''N'') cc'+
' join zonas_clientes z on z.zona_cliente_id=cc.zona_cliente_id'+
' join conceptos_cc c on c.concepto_cc_id=cc.concepto_cc_id'+
' join dirs_clientes dc on dc.cliente_id=cc.cliente_id'+
' where UPPER(c.nombre)=''PREDIAL'' and cc.cliente_id=:cid';
  qryPredialPend.ParamByName('cid').Value:=cbCliente.KeyValue;
  qryPredialPend.Open();
  qryPredialPend.Refresh;
  rptPredialPend.PrepareReport;
  rptPredialPend.ShowPreparedReport;
  end;

    if RadioGroup1.ItemIndex=2 then
  begin
  qryPredialPend.SQL.Text:='select cc.*,z.nombre as zona,c.nombre,dc.colonia from get_cargos_cc(current_date, ''P'',0,''S'',''S'',''N'') cc'+
  ' join zonas_clientes z on z.zona_cliente_id=cc.zona_cliente_id'+
  ' join conceptos_cc c on c.concepto_cc_id=cc.concepto_cc_id'+
  ' join dirs_clientes dc on dc.cliente_id=cc.cliente_id'+
  ' where UPPER(c.nombre)=''PREDIAL'' and dc.colonia=:col';
  qryPredialPend.ParamByName('col').Value:=cbZona.Text;
  qryPredialPend.Open();
  qryPredialPend.Refresh;
  rptPredialPend.PrepareReport;
  rptPredialPend.ShowPreparedReport;
  end;


end;

procedure TReportes.Button2Click(Sender: TObject);
begin
qryAuxiliar.ParamByName('cli').Value:=cbCliente.KeyValue;
  qryAuxiliar.Open();
  qryAuxiliar2.ParamByName('cli').Value:=cbCliente.KeyValue;
  qryAuxiliar2.Open();
  qryAuxiliar.Refresh;
  rptAuxiliar.PrepareReport;
  rptAuxiliar.ShowPreparedReport;
end;

procedure TReportes.FormShow(Sender: TObject);
begin
qryClientes.Open();
qryCampos.Open();
end;

procedure TReportes.RadioGroup1Click(Sender: TObject);
begin
 if RadioGroup1.ItemIndex=2 then
  begin
   cbZona.Visible:=true;
  end  else cbZona.Visible:=false;
end;

end.
