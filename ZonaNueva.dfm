object UZonaNueva: TUZonaNueva
  Left = 0
  Top = 0
  Caption = 'Nueva Zona'
  ClientHeight = 110
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 16
    Width = 81
    Height = 25
    Caption = 'Nombre:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 91
    Width = 527
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object btnGuardar: TButton
    Left = 192
    Top = 51
    Width = 89
    Height = 33
    Caption = 'Guardar'
    TabOrder = 1
    OnClick = btnGuardarClick
  end
  object txtZona: TEdit
    Left = 111
    Top = 18
    Width = 394
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'Protocol=TCPIP'
      'Server=localhost'
      'CharacterSet=ISO8859_1'
      'RoleName=USUARIO_MICROSIP'
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    UpdateOptions.AssignedValues = [uvAutoCommitUpdates]
    UpdateOptions.AutoCommitUpdates = True
    LoginPrompt = False
    Left = 477
    Top = 168
  end
end
