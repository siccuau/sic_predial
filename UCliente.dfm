object ClienteForm: TClienteForm
  Left = 0
  Top = 0
  Caption = 'Cliente'
  ClientHeight = 294
  ClientWidth = 633
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblNombre: TLabel
    Left = 215
    Top = 18
    Width = 99
    Height = 13
    Caption = 'NOMBRE COMPLETO'
  end
  object lblApPatero: TLabel
    Left = 239
    Top = 419
    Width = 98
    Height = 13
    Caption = 'APELLIDO PATERNO'
    Visible = False
  end
  object lblApMaterno: TLabel
    Left = 456
    Top = 419
    Width = 100
    Height = 13
    Caption = 'APELLIDO MATERNO'
    Visible = False
  end
  object lblRepLegal: TLabel
    Left = 16
    Top = 64
    Width = 116
    Height = 13
    Caption = 'REPRESENTANTE LEGAL'
  end
  object lblTelefono: TLabel
    Left = 215
    Top = 64
    Width = 52
    Height = 13
    Caption = 'TELEFONO'
  end
  object lblMunicipio: TLabel
    Left = 432
    Top = 67
    Width = 55
    Height = 13
    Caption = 'MUNICIPIO'
  end
  object lblCampoNo: TLabel
    Left = 16
    Top = 128
    Width = 82
    Height = 13
    Caption = 'CAMPO NUMERO'
  end
  object lblCasaNo: TLabel
    Left = 215
    Top = 128
    Width = 73
    Height = 13
    Caption = 'CASA NUMERO'
  end
  object lblFraccionamiento: TLabel
    Left = 16
    Top = 192
    Width = 99
    Height = 13
    Caption = 'FRACCIONAMIENTO'
  end
  object lblCalle: TLabel
    Left = 215
    Top = 192
    Width = 30
    Height = 13
    Caption = 'CALLE'
  end
  object lblAvenida: TLabel
    Left = 432
    Top = 192
    Width = 44
    Height = 13
    Caption = 'AVENIDA'
  end
  object lblRfc: TLabel
    Left = 16
    Top = 18
    Width = 20
    Height = 13
    Caption = 'RFC'
  end
  object txtRfc: TEdit
    Left = 16
    Top = 37
    Width = 170
    Height = 21
    MaxLength = 20
    TabOrder = 0
  end
  object txtNombre: TEdit
    Left = 215
    Top = 37
    Width = 387
    Height = 21
    TabOrder = 1
  end
  object txtApPaterno: TEdit
    Left = 239
    Top = 438
    Width = 188
    Height = 21
    TabOrder = 12
    Visible = False
  end
  object txtApMaterno: TEdit
    Left = 456
    Top = 438
    Width = 170
    Height = 21
    TabOrder = 13
    Visible = False
  end
  object txtTelefono: TEdit
    Left = 215
    Top = 83
    Width = 188
    Height = 21
    MaxLength = 20
    TabOrder = 3
  end
  object txtRepLegal: TEdit
    Left = 16
    Top = 87
    Width = 170
    Height = 21
    TabOrder = 2
  end
  object txtCasaNo: TEdit
    Left = 215
    Top = 147
    Width = 188
    Height = 21
    TabOrder = 6
  end
  object txtFraccionamiento: TEdit
    Left = 16
    Top = 211
    Width = 170
    Height = 21
    TabOrder = 8
  end
  object txtAvenida: TEdit
    Left = 432
    Top = 211
    Width = 170
    Height = 21
    TabOrder = 10
  end
  object txtCalle: TEdit
    Left = 215
    Top = 211
    Width = 188
    Height = 21
    TabOrder = 14
  end
  object cboMunicipio: TComboBox
    Left = 432
    Top = 87
    Width = 170
    Height = 21
    PopupMenu = popMunicipio
    TabOrder = 4
  end
  object cboCampoNo: TComboBox
    Left = 16
    Top = 147
    Width = 170
    Height = 21
    PopupMenu = popCampo
    TabOrder = 5
    Text = 'ComboBox1'
  end
  object btnGuardar: TButton
    Left = 527
    Top = 256
    Width = 75
    Height = 25
    Caption = 'Guardar'
    TabOrder = 11
    OnClick = btnGuardarClick
  end
  object chkCorredor: TCheckBox
    Left = 432
    Top = 147
    Width = 97
    Height = 17
    Caption = 'CORREDOR'
    TabOrder = 7
    OnClick = chkCorredorClick
  end
  object cboCalles: TComboBox
    Left = 215
    Top = 211
    Width = 188
    Height = 21
    PopupMenu = popCalles
    TabOrder = 9
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    LoginPrompt = False
    Left = 428
    Top = 344
  end
  object BindSourceDB1: TBindSourceDB
    DataSet = qryMunicipio
    ScopeMappings = <>
    Left = 536
    Top = 344
  end
  object qryMunicipio: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from ciudades')
    Left = 368
    Top = 304
    object qryMunicipioCIUDAD_ID: TIntegerField
      FieldName = 'CIUDAD_ID'
      Origin = 'CIUDAD_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryMunicipioNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Required = True
      Size = 50
    end
    object qryMunicipioCLAVE_FISCAL: TStringField
      FieldName = 'CLAVE_FISCAL'
      Origin = 'CLAVE_FISCAL'
      Size = 3
    end
    object qryMunicipioES_PREDET: TStringField
      FieldName = 'ES_PREDET'
      Origin = 'ES_PREDET'
      FixedChar = True
      Size = 1
    end
    object qryMunicipioESTADO_ID: TIntegerField
      FieldName = 'ESTADO_ID'
      Origin = 'ESTADO_ID'
      Required = True
    end
    object qryMunicipioUSUARIO_CREADOR: TStringField
      FieldName = 'USUARIO_CREADOR'
      Origin = 'USUARIO_CREADOR'
      Size = 31
    end
    object qryMunicipioFECHA_HORA_CREACION: TSQLTimeStampField
      FieldName = 'FECHA_HORA_CREACION'
      Origin = 'FECHA_HORA_CREACION'
    end
    object qryMunicipioUSUARIO_AUT_CREACION: TStringField
      FieldName = 'USUARIO_AUT_CREACION'
      Origin = 'USUARIO_AUT_CREACION'
      Size = 31
    end
    object qryMunicipioUSUARIO_ULT_MODIF: TStringField
      FieldName = 'USUARIO_ULT_MODIF'
      Origin = 'USUARIO_ULT_MODIF'
      Size = 31
    end
    object qryMunicipioFECHA_HORA_ULT_MODIF: TSQLTimeStampField
      FieldName = 'FECHA_HORA_ULT_MODIF'
      Origin = 'FECHA_HORA_ULT_MODIF'
    end
    object qryMunicipioUSUARIO_AUT_MODIF: TStringField
      FieldName = 'USUARIO_AUT_MODIF'
      Origin = 'USUARIO_AUT_MODIF'
      Size = 31
    end
  end
  object qryCampos: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from SIC_PREDIAL_CAMPOS')
    Left = 536
    Top = 400
  end
  object BindingsList1: TBindingsList
    Methods = <>
    OutputConverters = <>
    Left = 476
    Top = 389
    object LinkListControlToField1: TLinkListControlToField
      Category = 'Quick Bindings'
      DataSource = BindSourceDB1
      FieldName = 'NOMBRE'
      Control = cboMunicipio
      FillExpressions = <>
      FillHeaderExpressions = <>
      FillBreakGroups = <>
    end
    object LinkListControlToField2: TLinkListControlToField
      Category = 'Quick Bindings'
      DataSource = BindSourceDB2
      FieldName = 'NOMBRE'
      Control = cboCampoNo
      FillExpressions = <>
      FillHeaderExpressions = <>
      FillBreakGroups = <>
    end
    object LinkListControlToField3: TLinkListControlToField
      Category = 'Quick Bindings'
      DataSource = BindSourceDB3
      FieldName = 'NOMBRE'
      Control = cboCalles
      FillExpressions = <>
      FillHeaderExpressions = <>
      FillBreakGroups = <>
    end
  end
  object popCampo: TPopupMenu
    Left = 96
    Top = 360
    object Nuevo1: TMenuItem
      Caption = 'Nuevo'
      OnClick = Nuevo1Click
    end
  end
  object BindSourceDB2: TBindSourceDB
    DataSet = qryCampos
    ScopeMappings = <>
    Left = 336
    Top = 360
  end
  object popMunicipio: TPopupMenu
    Left = 280
    Top = 296
    object Nuevo2: TMenuItem
      Caption = 'Nuevo'
      OnClick = Nuevo2Click
    end
  end
  object popCalles: TPopupMenu
    Left = 64
    Top = 248
    object Nuevo3: TMenuItem
      Caption = 'Nuevo'
      OnClick = Nuevo3Click
    end
    object Modificar1: TMenuItem
      Caption = 'Modificar'
      OnClick = Modificar1Click
    end
  end
  object qryCalles: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from SIC_PREDIAL_CALLES')
    Left = 168
    Top = 312
    object qryCallesID_CALLES: TIntegerField
      FieldName = 'ID_CALLES'
      Origin = 'ID_CALLES'
    end
    object qryCallesNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 50
    end
  end
  object BindSourceDB3: TBindSourceDB
    DataSet = qryCalles
    ScopeMappings = <>
    Left = 248
    Top = 352
  end
  object qryCliente: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from SIC_PREDIAL_CAMPOS')
    Left = 577
    Top = 408
  end
  object qryDireccionCliente: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from SIC_PREDIAL_CAMPOS')
    Left = 473
    Top = 304
  end
  object MainMenu1: TMainMenu
    Left = 216
    Top = 240
    object Herramientas1: TMenuItem
      Caption = 'Herramientas'
      object Importardesde1: TMenuItem
        Caption = 'Importar archivo Excel'
        OnClick = Importardesde1Click
      end
    end
  end
end
