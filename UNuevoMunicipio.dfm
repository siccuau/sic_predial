object FormNuevoMunicipio: TFormNuevoMunicipio
  Left = 0
  Top = 0
  Caption = 'Municipio'
  ClientHeight = 207
  ClientWidth = 234
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblNombre: TLabel
    Left = 16
    Top = 8
    Width = 42
    Height = 13
    Caption = 'NOMBRE'
  end
  object lblEstado: TLabel
    Left = 16
    Top = 77
    Width = 40
    Height = 13
    Caption = 'ESTADO'
  end
  object txtNombre: TEdit
    Left = 16
    Top = 40
    Width = 145
    Height = 21
    TabOrder = 0
    Text = 'Nombre'
  end
  object btnGuardar: TButton
    Left = 86
    Top = 168
    Width = 75
    Height = 25
    Caption = 'Guardar'
    TabOrder = 1
    OnClick = btnGuardarClick
  end
  object cboEstado: TComboBox
    Left = 16
    Top = 96
    Width = 145
    Height = 21
    Enabled = False
    TabOrder = 2
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    LoginPrompt = False
    Left = 175
    Top = 159
  end
  object qryEstado: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from estados where upper(nombre)='#39'CHIHUAHUA'#39)
    Left = 176
    Top = 64
  end
  object BindSourceDB1: TBindSourceDB
    DataSet = qryEstado
    ScopeMappings = <>
    Left = 176
    Top = 104
  end
  object BindingsList1: TBindingsList
    Methods = <>
    OutputConverters = <>
    Left = 180
    Top = 5
    object LinkListControlToField1: TLinkListControlToField
      Category = 'Quick Bindings'
      DataSource = BindSourceDB1
      FieldName = 'NOMBRE'
      Control = cboEstado
      FillExpressions = <>
      FillHeaderExpressions = <>
      FillBreakGroups = <>
    end
  end
end
