object FormImpuestos: TFormImpuestos
  Left = 0
  Top = 0
  Caption = 'Otros Impuestos'
  ClientHeight = 468
  ClientWidth = 847
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 71
    Height = 25
    Caption = 'Cliente:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 288
    Top = 49
    Width = 97
    Height = 25
    Caption = 'Impuestos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object cbCliente: TDBLookupComboBox
    Left = 93
    Top = 16
    Width = 420
    Height = 27
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyField = 'CLIENTE_ID'
    ListField = 'NOMBRE'
    ListSource = dsClientes
    ParentFont = False
    TabOrder = 0
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 449
    Width = 847
    Height = 19
    Panels = <
      item
        Width = 50
      end>
    ExplicitWidth = 731
  end
  object sgImpuestos: TStringGrid
    Left = 16
    Top = 75
    Width = 817
    Height = 331
    FixedCols = 0
    RowCount = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goFixedRowDefAlign]
    ParentFont = False
    PopupMenu = PopupMenu1
    TabOrder = 2
    OnKeyDown = sgImpuestosKeyDown
    OnSelectCell = sgImpuestosSelectCell
  end
  object btnGuardar: TButton
    Left = 384
    Top = 407
    Width = 89
    Height = 36
    Caption = 'Guardar'
    TabOrder = 3
    OnClick = btnGuardarClick
  end
  object ComboBox1: TComboBox
    Left = 18
    Top = 103
    Width = 280
    Height = 21
    PopupMenu = PopupMenu1
    TabOrder = 4
    Text = 'ComboBox1'
    Visible = False
    OnChange = ComboBox1Change
    OnKeyDown = ComboBox1KeyDown
  end
  object qryClientes: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from clientes')
    Left = 472
    Top = 48
  end
  object dsClientes: TDataSource
    DataSet = qryClientes
    Left = 464
    Top = 104
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'Protocol=TCPIP'
      'Server=localhost'
      'CharacterSet=ISO8859_1'
      'RoleName=USUARIO_MICROSIP'
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    UpdateOptions.AssignedValues = [uvAutoCommitUpdates]
    UpdateOptions.AutoCommitUpdates = True
    LoginPrompt = False
    Left = 469
    Top = 160
  end
  object PopupMenu1: TPopupMenu
    Left = 457
    Top = 224
    object NuevoImpuesto: TMenuItem
      Caption = 'Nuevo Impuesto'
      OnClick = NuevoImpuestoClick
    end
  end
  object dsImpuestos: TDataSource
    DataSet = qryImpuestos
    Left = 408
    Top = 112
  end
  object qryImpuestos: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from sic_predial_imp order by ano DESC')
    Left = 408
    Top = 56
  end
  object qryImpCli: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from sic_predial_imp_cli ic'
      'join sic_predial_imp i on ic.impuesto_id=i.impuesto_id'
      'where cliente_id=:cli')
    Left = 312
    Top = 200
    ParamData = <
      item
        Name = 'CLI'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object dsImpCli: TDataSource
    DataSet = qryImpCli
    Left = 312
    Top = 272
  end
end
