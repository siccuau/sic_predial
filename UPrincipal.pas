unit UPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Vcl.ComCtrls, Data.DB,
  FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.Grids, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Vcl.DBCtrls,
  FireDAC.Comp.DataSet, Vcl.Menus,Zona, System.Rtti, System.Bindings.Outputs,
  Vcl.Bind.Editors, Data.Bind.EngExt, Vcl.Bind.DBEngExt, Data.Bind.Components,
  Data.Bind.DBScope,ZonaNueva, frxClass, frxDBSet, frxTableObject,UTraspaso,UImpuestos,UCambioFolio,UPropietarios;

type
  TPrincipal = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    Label1: TLabel;
    Label2: TLabel;
    cbTipo: TComboBox;
    sgTerreno: TStringGrid;
    btnGuardar: TButton;
    qryClientes: TFDQuery;
    cbCliente: TDBLookupComboBox;
    dsClientes: TDataSource;
    qryTerrenos: TFDQuery;
    PopupMenu1: TPopupMenu;
    NuevaZona1: TMenuItem;
    qryCampos: TFDQuery;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    rpConsolidado: TfrxReport;
    qryConsolidado: TFDQuery;
    Label3: TLabel;
    txtFechaCreacion: TEdit;
    Label4: TLabel;
    txtFechaModificacion: TEdit;
    txtAcres: TEdit;
    txtMetros: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    qryHistorial: TFDQuery;
    dssHistorial: TDataSource;
    rptHistorial: TfrxReport;
    frxDBDataset1: TfrxDBDataset;
    PopupMenu2: TPopupMenu;
    Traspasar: TMenuItem;
    rptHistorialTraslados: TfrxReport;
    dssHistorialTraslados: TDataSource;
    qryHistorialTraslados: TFDQuery;
    frxDBDataset2: TfrxDBDataset;
    btnImpuestos: TButton;
    cbCampo: TDBLookupComboBox;
    dsCampos: TDataSource;
    CambiarFolio1: TMenuItem;
    cbMunicipio: TComboBox;
    AsignarPropietarios1: TMenuItem;
    MainMenu1: TMainMenu;
    Consolidado1: TMenuItem;
    Consolidado2: TMenuItem;
    HistorialdeMovimientos1: TMenuItem;
    HistorialTraspasos1: TMenuItem;
    Impuestos1: TMenuItem;
    dsConsolidado: TDataSource;
    dssConsolidado: TfrxDBDataset;
    rpImpuestos: TfrxReport;
    dsImpuestos: TDataSource;
    qryImpuestos: TFDQuery;
    dssImpuestos: TfrxDBDataset;
    procedure FormShow(Sender: TObject);
    procedure sgTerrenoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnGuardarClick(Sender: TObject);
    procedure Cargardatos();
    procedure cbTipoChange(Sender: TObject);
    procedure formatogrid();
    procedure cbClienteCloseUp(Sender: TObject);
    procedure NuevaZona1Click(Sender: TObject);
    procedure sgTerrenoSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure cbZonaChange(Sender: TObject);
    procedure sgTerrenoDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure cbZonaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnHistorialClick(Sender: TObject);
    procedure TraspasarClick(Sender: TObject);
    procedure btnHistTrasladosClick(Sender: TObject);
    procedure btnImpuestosClick(Sender: TObject);
    procedure DeleteRow(Grid: TStringGrid; ARow: Integer);
    procedure cbCampoCloseUp(Sender: TObject);
    procedure cbCampoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CambiarFolio1Click(Sender: TObject);
    procedure cbMunicipioCloseUp(Sender: TObject);
    procedure cbMunicipioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure AsignarPropietarios1Click(Sender: TObject);
    procedure Consolidado2Click(Sender: TObject);
    procedure HistorialdeMovimientos1Click(Sender: TObject);
    procedure HistorialTraspasos1Click(Sender: TObject);
    procedure Impuestos1Click(Sender: TObject);
  private
    { Private declarations }
  public
  cliente_id:integer;
    { Public declarations }
  end;

var
  Principal: TPrincipal;

implementation

{$R *.dfm}

procedure TPrincipal.AsignarPropietarios1Click(Sender: TObject);
var
FormPropietarios:TFormPropietarios;
terreno_id:integer;
begin
  FormPropietarios:= TFormPropietarios.Create(nil);
  FormPropietarios.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormPropietarios.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormPropietarios.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormPropietarios.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormPropietarios.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  FormPropietarios.StatusBar1.Panels.Items[0].Text := StatusBar1.Panels.Items[0].Text;
  terreno_id:=Conexion.ExecSQLScalar('SELECT ID_TERRENO FROM SIC_PREDIAL_TERRENOS WHERE FOLIO='''+sgTerreno.Cells[0,sgTerreno.Row]+''' AND CLIENTE_ID='+qryClientes.FieldByName('CLIENTE_ID').AsString+' AND TIPO='''+UpperCase(cbTipo.Text) +''' ');
  FormPropietarios.folio_terreno:=sgTerreno.Cells[0,sgTerreno.Row];
  FormPropietarios.terreno_id:=terreno_id;
  FormPropietarios.cliente_id:=qryClientes.FieldByName('CLIENTE_ID').AsInteger;
  FormPropietarios.ShowModal;
end;


procedure TPrincipal.btnGuardarClick(Sender: TObject);
var tipo,folio,zona,campo,direccion,municipio:string;
este,norte,metros,acres,costo:double;
numero,i,a�o:integer;
bandera:bool;
begin
  if cbTipo.Text<>'' then
  begin
    if (sgTerreno.Cells[0,sgTerreno.Row-1]<>'') and (sgTerreno.Cells[1,sgTerreno.Row-1]<>'') and (sgTerreno.Cells[2,sgTerreno.Row-1]<>'')
    and (sgTerreno.Cells[3,sgTerreno.Row-1]<>'') and (sgTerreno.Cells[4,sgTerreno.Row-1]<>'') and (sgTerreno.Cells[5,sgTerreno.Row-1]<>'')
    and (sgTerreno.Cells[6,sgTerreno.Row-1]<>'') then
    begin
         case cbTipo.ItemIndex of
        0:tipo:='LOTE';
        1:tipo:='TEMPORAL';
        2:tipo:='RIEGO';
        3:tipo:='CASA';
        4:tipo:='CORREDOR';
       end;

    for i := 1 to sgTerreno.RowCount-2 do
    begin
     try
       folio:=sgTerreno.Cells[0,i];
       if folio<>'' then
        begin
         campo:=sgTerreno.Cells[1,i];
         direccion:=sgTerreno.Cells[2,i];
         municipio:=sgTerreno.Cells[3,i];
         este:=sgTerreno.Cells[4,i].ToDouble;
         norte:=sgTerreno.Cells[5,i].ToDouble;
         numero:=sgTerreno.Cells[6,i].ToInteger;
         metros:=sgTerreno.Cells[7,i].ToDouble;
         acres:=sgTerreno.Cells[8,i].ToDouble;
         zona:=sgTerreno.Cells[10,i];
        //FECHAS
       if Conexion.ExecSQLScalar('select count(cliente_id) from sic_predial_fechas where cliente_id=:cli',[cbCliente.KeyValue])=0 then
         begin
         Conexion.ExecSQL('insert into sic_predial_fechas(cliente_id,fecha_creacion,fecha_modificacion) values(:cli,:fec,:femo)',[cbCliente.KeyValue,Now,Now]);
         end
         else
         begin
         Conexion.ExecSQL('update sic_predial_fechas set fecha_modificacion=:mod where cliente_id=:cli',[Now,cbCliente.KeyValue]);
         end;


        if Conexion.ExecSQLScalar('select count(folio) from sic_predial_terrenos where cliente_id=:cli and folio=:folio',[cbCliente.KeyValue,folio])>0 then
         begin    //ACTUALIZAR
           Conexion.ExecSQL('update sic_predial_terrenos set '+
          'ZONA=:Z,MTS_ESTE=:ME,MTS_NORTE=:MN,NUMERO=:NUM,METROS=:METR,ACRES=:A,CAMPO=:CAMP,DIRECCION=:DIR,MUNICIPIO=:MUNI where cliente_id=:cli and folio=:folio',[zona,este,norte,numero,metros,acres,campo,direccion,municipio,cliente_id,folio]);
         end
         else
         begin
           //INSERTAR
           Conexion.ExecSQL('Insert into sic_predial_terrenos(ID_TERRENO,TIPO,CLIENTE_ID,FOLIO,ZONA,MTS_ESTE,MTS_NORTE,NUMERO,METROS,ACRES,CAMPO,DIRECCION,MUNICIPIO,TRASPASO) '+
           'VALUES(-1,:tipo,:cliente_id,:folio,:zona,:este,:norte,:numero,:metros,:acres,:campo,:dir,:muni,''N'')',[tipo,cbCliente.KeyValue,folio,zona,este,norte,numero,metros,acres,campo,direccion,municipio]);
           Conexion.ExecSQL('INSERT INTO sic_predial_bitacora VALUES(gen_id(SIC_ID_BITACORA,1),:folio,:cli,''TERRENO NUEVO'',''0'',:metros,null,current_user, CURRENT_TIMESTAMP)',[folio,cbCliente.KeyValue,metros]);
         end;
        bandera:=true;
        end
        else
        begin
        ShowMessage('Folio no puede estar en blanco.');
        bandera:=false;
        break;
        end;
     except
     ShowMessage('ERROR al guardar los datos.');
     end;
    end;
    if bandera=true then ShowMessage('Se guardo correctamente.');
    cargardatos;
   end else ShowMessage('No se puede guardar si no hay datos.');
 end else ShowMessage('El tipo de terreno no puede estar vacio.');
end;

procedure TPrincipal.btnHistorialClick(Sender: TObject);
begin
qryHistorial.ParamByName('cli').Value:=cbCliente.KeyValue;
qryHistorial.Open();
qryHistorial.Refresh;
rptHistorial.PrepareReport;
rptHistorial.ShowPreparedReport;
end;

procedure TPrincipal.FormShow(Sender: TObject);
begin


//FORMATO DEL STRINGGRID
formatogrid;

//ABRIR QUERYS
qryCampos.Open();
qryClientes.Open();
cbCliente.KeyValue:=cliente_id;

//CARGAR DATOS
Cargardatos;

end;

procedure TPrincipal.HistorialdeMovimientos1Click(Sender: TObject);
begin
qryHistorial.ParamByName('cli').Value:=cbCliente.KeyValue;
qryHistorial.Open();
qryHistorial.Refresh;
rptHistorial.PrepareReport;
rptHistorial.ShowPreparedReport;
end;

procedure TPrincipal.HistorialTraspasos1Click(Sender: TObject);
begin
qryHistorialTraslados.ParamByName('cli').Value:=cbCliente.KeyValue;
qryHistorialTraslados.Open();
qryHistorialTraslados.Refresh;
rptHistorialTraslados.PrepareReport;
rptHistorialTraslados.ShowPreparedReport;
end;

procedure TPrincipal.Impuestos1Click(Sender: TObject);
begin
 qryImpuestos.ParamByName('cliente').Value:=cbCliente.KeyValue;
 qryImpuestos.Open();
 qryImpuestos.Refresh;
 qryConsolidado.ParamByName('cliente').Value:=cbCliente.KeyValue;
 qryConsolidado.Open();
 qryConsolidado.Refresh;
 rpImpuestos.PrepareReport;
 rpImpuestos.ShowPreparedReport;
end;

procedure TPrincipal.NuevaZona1Click(Sender: TObject);
var
  FormArticuloNuevo:TUZonaNueva;
  i: Integer;
begin
  FormArticuloNuevo := TUZonaNueva.Create(nil);
  FormArticuloNuevo.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormArticuloNuevo.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormArticuloNuevo.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormArticuloNuevo.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormArticuloNuevo.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
 // FormArticuloNuevo.cliente_id:=cbCliente.KeyValue;
  if FormArticuloNuevo.ShowModal=MrOk then
 begin
  qryCampos.Refresh;
  end;

 { if FormArticuloNuevo.ShowModal=MrOk then
  begin
   for i := 1 to sgTerreno.RowCount-1 do
     begin
       if sgTerreno.Cells[0,i]<>'' then
        begin
          sgTerreno.Cells[1,i]:=Conexion.ExecSQLScalar('select zc.nombre from clientes c join zonas_clientes zc'+
         ' on c.zona_cliente_id=zc.zona_cliente_id where cliente_id=:cli',[cbCliente.KeyValue]);
        end;
     end;
  end; }
end;

procedure TPrincipal.sgTerrenoDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
  var
  S: string;
  LDelta,i : integer;
  SavedAlign: word;
begin
 { if (ACol = 1) and (ARow<>0) then
  begin  // ACol is zero based
    S := sgTerreno.Cells[ACol, ARow]; // cell contents
    SavedAlign := SetTextAlign(sgTerreno.Canvas.Handle, TA_CENTER);
    sgTerreno.Canvas.TextRect(Rect,
      Rect.Left + (Rect.Right - Rect.Left) div 2, Rect.Top + 2, S);
    SetTextAlign(sgTerreno.Canvas.Handle, SavedAlign);
  end;


    if (ACol = 4) and (ARow<>0) then
  begin  // ACol is zero based
    S := sgTerreno.Cells[ACol, ARow]; // cell contents
    SavedAlign := SetTextAlign(sgTerreno.Canvas.Handle, TA_CENTER);
    sgTerreno.Canvas.TextRect(Rect,
      Rect.Left + (Rect.Right - Rect.Left) div 2, Rect.Top + 2, S);
    SetTextAlign(sgTerreno.Canvas.Handle, SavedAlign);
  end; }

    if (ACol=6) or (ACol=10) then  //CENTRO
    begin
    sgTerreno.Canvas.Brush.Color := clWhite;
    sgTerreno.Canvas.FillRect(Rect);
    sgTerreno.Canvas.TextOut(Rect.Left+(Rect.Right - Rect.Left) div 2,Rect.Top,sgTerreno.Cells[ACol, ARow]);
    end
    else if (ACol=0) or (ACol=1) or (ACol=2) or (ACol=3) then      //IZQUIERDA
    begin
    sgTerreno.Canvas.Brush.Color := clWhite;
    sgTerreno.Canvas.FillRect(Rect);
    sgTerreno.Canvas.TextOut(Rect.Left,Rect.Top,sgTerreno.Cells[ACol, ARow]);
    end
    else
    begin   //DERECHA
    sgTerreno.Canvas.Brush.Color := clWhite;
    sgTerreno.Canvas.FillRect(Rect);
    sgTerreno.Canvas.TextOut(Rect.Right - sgTerreno.Canvas.TextWidth(sgTerreno.Cells[ACol, ARow]),Rect.Top,sgTerreno.Cells[ACol, ARow]);
    end;


    if sgTerreno.Cells[9,Arow]='S' then
     begin
        sgTerreno.Canvas.Brush.Color := clRed;
        sgTerreno.Canvas.FillRect(Rect);
        if (ACol=6) or (ACol=10) then
        sgTerreno.Canvas.TextOut(Rect.Left+(Rect.Right - Rect.Left) div 2,Rect.Top,sgTerreno.Cells[ACol, ARow])
        else
        if (ACol=0) or (ACol=1) or (ACol=2) or (ACol=3) then
        begin
        sgTerreno.Canvas.TextOut(Rect.Left,Rect.Top,sgTerreno.Cells[ACol, ARow]);
        end
        else
        sgTerreno.Canvas.TextOut(Rect.Right - sgTerreno.Canvas.TextWidth(sgTerreno.Cells[ACol, ARow]),Rect.Top,sgTerreno.Cells[ACol, ARow]);
     end;
   end;

procedure TPrincipal.sgTerrenoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var unidades,MetrosTotal,AcresTotal,metros:double;
  i,consecutivo,buttonselected:integer;
  nuevo_folio,tipo:string;
begin
 if Key=VK_RETURN then
  begin
    //SI ES COLUMNA DE SOLO NUMEROS
  if (sgTerreno.Col=4) or (sgTerreno.Col=5) or (sgTerreno.Col=6) or (sgTerreno.Col=7) or (sgTerreno.Col=8) then
    begin
      if TryStrToFloat(sgTerreno.Cells[sgTerreno.Col,sgTerreno.Row],unidades)=False then
      begin
      sgTerreno.Cells[sgTerreno.Col,sgTerreno.Row]:='1';
      ShowMessage('Esta celda solo puede contener numeros.');
      end
    else
      begin
            if sgTerreno.Col=7 then
       begin
       sgTerreno.Cells[sgTerreno.Col+1,sgTerreno.Row]:=formatfloat('0.####',((sgTerreno.Cells[sgTerreno.Col,sgTerreno.Row].ToDouble)/(4046.86)));
          //LLENAR TOTALES
       MetrosTotal:=0;
       AcresTotal:=0;
       for i := 1 to sgTerreno.RowCount-2 do
       begin
        MetrosTotal:=MetrosTotal+strtofloat(sgTerreno.Cells[7,i]);
        AcresTotal:=AcresTotal+strtofloat(sgTerreno.Cells[8,i]);
       end;
       txtMetros.Text:=floattostr(MetrosTotal);
       txtAcres.Text:=floattostr(AcresTotal);
       end;

      if sgTerreno.Col=8 then //ACRES
       begin

       sgTerreno.Cells[sgTerreno.Col-1,sgTerreno.Row]:=formatfloat('0.####',((sgTerreno.Cells[sgTerreno.Col,sgTerreno.Row].ToDouble)*(4046.86)));
               sgTerreno.Cells[10,sgTerreno.Row]:='13R';
        sgTerreno.Col:=0;
        if sgTerreno.Row=sgTerreno.RowCount-1 then
        begin
        sgTerreno.RowCount:=sgTerreno.RowCount+1;
        end;
        sgTerreno.Row:=sgTerreno.Row+1;
                     //LLENAR TOTALES
       MetrosTotal:=0;
       AcresTotal:=0;
       for i := 1 to sgTerreno.RowCount-2 do
         begin
        MetrosTotal:=MetrosTotal+strtofloat(sgTerreno.Cells[7,i]);
        AcresTotal:=AcresTotal+strtofloat(sgTerreno.Cells[8,i]);
        end;
         txtMetros.Text:=floattostr(MetrosTotal);
         txtAcres.Text:=floattostr(AcresTotal);
       // sgTerreno.Col:=sgTerreno.Col+2;


       end
      else sgTerreno.Col:=sgTerreno.Col+1;
     end;
    end
    else
    begin
          //SI ES COLUMNA FOLIO
        if (sgTerreno.Col=0) then
      begin
      if sgTerreno.Cells[0,sgTerreno.Row]='0' then
       begin
       consecutivo:=Conexion.ExecSQLScalar('select consecutivo from folios_conceptos where sistema=''IN'' and serie=''SN''');
       nuevo_folio:='SN'+Format('%.*d',[7, consecutivo]);
       sgTerreno.Cells[0,sgTerreno.Row]:=nuevo_folio;
       //ACTUALIZAR FOLIO
       Conexion.ExecSQL('update folios_conceptos set consecutivo=:consec where sistema=''IN'' and Serie=''SN''',[consecutivo+1]);
       end;

      sgTerreno.Col:=sgTerreno.Col+1;
      end else sgTerreno.Col:=sgTerreno.Col+1;
    end;
  end;

      If (Shift = [ssctrl]) and (key = VK_DELETE) then
  begin
  if sgTerreno.Cells[0,sgTerreno.Row]<>'' then
   begin
          case cbTipo.ItemIndex of
      0:tipo:='LOTE';
      1:tipo:='TEMPORAL';
      2:tipo:='RIEGO';
      3:tipo:='CASA';
      4:tipo:='CORREDOR';
     end;
    buttonSelected := messagedlg('Desea eliminar este terreno "'+sgTerreno.Cells[0,sgTerreno.Row]+'"?',mtCustom,[mbYes,mbCancel], 0);
      if buttonSelected = mrYes    then
      begin
      //ELIMINAR DE BD
             metros:=sgTerreno.Cells[5,sgTerreno.Row].ToDouble;
      Conexion.ExecSQL('delete from sic_predial_terrenos where cliente_id=:cli and tipo=:tip and folio=:fol',[cbCliente.KeyValue,tipo,sgTerreno.Cells[0,sgTerreno.Row]]);
       Conexion.ExecSQL('INSERT INTO sic_predial_bitacora VALUES(gen_id(SIC_ID_BITACORA,1),:folio,:cli,''ELIMINO TERRENO'',:metros,''0'',null,current_user, CURRENT_TIMESTAMP)',[sgTerreno.Cells[0,sgTerreno.Row],cbCliente.KeyValue,metros]);
      //ELIMINAR LINEA
       DeleteRow(sgTerreno, sgTerreno.Row);
      end;
   end;
  end;
end;

procedure TPrincipal.sgTerrenoSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
var
R: TRect;
begin
if (ACol = 1) and (ARow <> 0) then
  begin
  R := sgTerreno.CellRect(ACol, ARow);
  R.Left := R.Left + sgTerreno.Left;
  R.Right := R.Right + sgTerreno.Left;
  R.Top := R.Top + sgTerreno.Top;
  R.Bottom := R.Bottom + sgTerreno.Top;
    with cbCampo do
    begin
    Left := R.Left + 1;
    Top := R.Top + 2;
    Width := (R.Right + 1) - R.Left;
    Height := (R.Bottom + 1) - R.Top;
    Visible := True;
    SetFocus;
    end;

 end
  else
  if (ACol = 3) and (ARow <> 0) then
 begin
  R := sgTerreno.CellRect(ACol, ARow);
  R.Left := R.Left + sgTerreno.Left;
  R.Right := R.Right + sgTerreno.Left;
  R.Top := R.Top + sgTerreno.Top;
  R.Bottom := R.Bottom + sgTerreno.Top;
    with cbMunicipio do
    begin
    Left := R.Left + 1;
    Top := R.Top + 2;
    Width := (R.Right + 1) - R.Left;
    Height := (R.Bottom + 1) - R.Top;
    Visible := True;
    SetFocus;
    end;

 end
 else
  begin
  cbMunicipio.Visible:=false;
  cbCampo.Visible:=false;
  end;
CanSelect := True;

if (ACol=0) and (ARow<>sgTerreno.RowCount-1) then
 begin
   sgTerreno.Options := sgTerreno.Options - [goEditing];
 end
 else  sgTerreno.Options := sgTerreno.Options + [goEditing];

end;


procedure TPrincipal.TraspasarClick(Sender: TObject);
var
  FormTraspaso:TTraspaso;
  i: Integer;
  metros:double;
  tipo:string;
begin
if sgTerreno.Cells[sgTerreno.Col,sgTerreno.Row]<>'' then
  begin
  metros:=strtofloat(sgTerreno.Cells[7,sgTerreno.Row]);
  if metros<>0 then
   begin
    FormTraspaso := TTraspaso.Create(nil);
    FormTraspaso.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
    FormTraspaso.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
    FormTraspaso.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
    FormTraspaso.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
    FormTraspaso.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
    FormTraspaso.cliente_id:=cbCliente.KeyValue;
    FormTraspaso.folio:=sgTerreno.Cells[0,sgTerreno.Row];
    FormTraspaso.metros:=metros;
    case cbTipo.ItemIndex of
      0:tipo:='LOTE';
      1:tipo:='TEMPORAL';
      2:tipo:='RIEGO';
      3:tipo:='CASA';
      4:tipo:='CORREDOR';
     end;
    FormTraspaso.tipo:=tipo;
    FormTraspaso.cbCliente.KeyValue:=cbCliente.KeyValue;
    if FormTraspaso.ShowModal=MrOk then
     begin
      Cargardatos;
     end;
   end
   else ShowMessage('Terreno ya se traspaso anteriormente.');
  end
  else ShowMessage('No se puede traspasar terreno vacio.');
end;

procedure TPrincipal.CambiarFolio1Click(Sender: TObject);
var
  FormCambioFolio:TCambioFolio;
  i: Integer;
  metros:double;
  tipo,folio:string;
begin
if sgTerreno.Cells[sgTerreno.Col,sgTerreno.Row]<>'' then
  begin
  folio:=sgTerreno.Cells[0,sgTerreno.Row];
    if (folio[1]='S') and (folio[2]='N') then
   begin
      metros:=strtofloat(sgTerreno.Cells[7,sgTerreno.Row]);
      if metros<>0 then
       begin
        FormCambioFolio := TCambioFolio.Create(nil);
        FormCambioFolio.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
        FormCambioFolio.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
        FormCambioFolio.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
        FormCambioFolio.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
        FormCambioFolio.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
        FormCambioFolio.cliente_id:=cbCliente.KeyValue;
        FormCambioFolio.folio:=sgTerreno.Cells[0,sgTerreno.Row];
             case cbTipo.ItemIndex of
          0:tipo:='LOTE';
          1:tipo:='TEMPORAL';
          2:tipo:='RIEGO';
          3:tipo:='CASA';
          4:tipo:='CORREDOR';
         end;
        FormCambioFolio.tipo:=tipo;
        FormCambioFolio.cbCliente.KeyValue:=cbCliente.KeyValue;
        if FormCambioFolio.ShowModal=MrOk then
         begin
          Cargardatos;
         end;
       end
       else ShowMessage('Se cambio el folio del terreno correctamente.');
   end else
       begin
       ShowMessage('Este folio no puede ser modificado.');
       end;
  end
  else ShowMessage('No se puede cambiar folio a terreno vacio.');
end;

procedure TPrincipal.Cargardatos();
var tipo,folio,zona,traspaso,campo,direccion,municipio,numero:string;
i,a�o:integer;
este,norte,metros,acres,MetrosTotal,AcresTotal,costo:double;
begin
     case cbTipo.ItemIndex of
      0:tipo:='LOTE';
      1:tipo:='TEMPORAL';
      2:tipo:='RIEGO';
      3:tipo:='CASA';
      4:tipo:='CORREDOR';
     end;

     //LLENAR FECHAS
  if Conexion.ExecSQLScalar('select count(cliente_id) from sic_predial_fechas where cliente_id=:cli',[cbCliente.KeyValue])=0 then
     begin
     txtFechaCreacion.Text:='';
     txtFechaModificacion.Text:='';
     end
     else
     begin
     txtFechaCreacion.Text:=Conexion.ExecSQLScalar('select fecha_creacion from sic_predial_fechas where cliente_id=:cli',[cbCliente.KeyValue]);
     txtFechaModificacion.Text:=Conexion.ExecSQLScalar('select fecha_modificacion from sic_predial_fechas where cliente_id=:cli',[cbCliente.KeyValue]);
     end;

if Conexion.ExecSQLScalar('select count(id_terreno) from sic_predial_terrenos where cliente_id=:cliente and tipo=:tipo',[cbCliente.keyvalue,tipo])>0 then
 begin
   formatogrid;
   i:=1;
   qryTerrenos.SQL.Text:='select * from sic_predial_terrenos where cliente_id=:cliente and tipo=:tipo';
   qryTerrenos.ParamByName('cliente').Value:=cbCliente.keyvalue;
   qryTerrenos.ParamByName('tipo').Value:=tipo;
   qryTerrenos.Open();
   qryTerrenos.First;
   while not qryTerrenos.Eof do
   begin
   folio:=qryTerrenos.FieldByName('folio').Value;
   if qryTerrenos.FieldByName('campo').Value<>null then
   campo:=qryTerrenos.FieldByName('campo').Value
   else campo:='';
      if qryTerrenos.FieldByName('direccion').Value<>null then
   direccion:=qryTerrenos.FieldByName('direccion').Value
   else direccion:='';
      if qryTerrenos.FieldByName('municipio').Value<>null then
   municipio:=qryTerrenos.FieldByName('municipio').Value
   else municipio:='';
   este:=qryTerrenos.FieldByName('mts_este').Value;
   norte:=qryTerrenos.FieldByName('mts_norte').Value;
   numero:=qryTerrenos.FieldByName('numero').Value;
   metros:=qryTerrenos.FieldByName('metros').Value;
   acres:=qryTerrenos.FieldByName('acres').Value;
   traspaso:=qryTerrenos.FieldByName('traspaso').Value;
   zona:=qryTerrenos.FieldByName('zona').Value;
    sgTerreno.Cells[0,i]:=folio;
    sgTerreno.Cells[1,i]:=campo;
    sgTerreno.Cells[2,i]:=direccion;
    sgTerreno.Cells[3,i]:=municipio;
    sgTerreno.Cells[4,i]:=floattostr(este);
    sgTerreno.Cells[5,i]:=floattostr(norte);
    sgTerreno.Cells[6,i]:=numero;
    sgTerreno.Cells[7,i]:=floattostr(metros);
    sgTerreno.Cells[8,i]:=floattostr(acres);
    sgTerreno.Cells[9,i]:=traspaso;
    sgTerreno.Cells[10,i]:=zona;
    i:=i+1;
    sgTerreno.RowCount:=sgTerreno.RowCount+1;
    qryTerrenos.Next;
   end;


   //LLENAR TOTALES
   MetrosTotal:=0;
   AcresTotal:=0;
   for i := 1 to sgTerreno.RowCount-2 do
   begin
    MetrosTotal:=MetrosTotal+strtofloat(sgTerreno.Cells[7,i]);
    AcresTotal:=AcresTotal+strtofloat(sgTerreno.Cells[5,i]);
   end;
   txtMetros.Text:=floattostr(MetrosTotal);
   txtAcres.Text:=floattostr(AcresTotal);

 end
 else formatogrid;
end;





procedure TPrincipal.cbCampoCloseUp(Sender: TObject);
begin
 sgTerreno.Cells[sgTerreno.Col,sgTerreno.Row]:=cbCampo.Text;
end;

procedure TPrincipal.cbCampoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if Key=VK_RETURN then
  begin
   sgTerreno.Cells[sgTerreno.Col,sgTerreno.Row]:=cbCampo.Text;
   sgTerreno.Col:=sgTerreno.Col+1;
   sgTerreno.SetFocus;
  end;
end;

procedure TPrincipal.cbClienteCloseUp(Sender: TObject);
begin
Cargardatos;
end;

procedure TPrincipal.cbMunicipioCloseUp(Sender: TObject);
begin
 sgTerreno.Cells[sgTerreno.Col,sgTerreno.Row]:=cbMunicipio.Text;
end;

procedure TPrincipal.cbMunicipioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if Key=VK_RETURN then
  begin
   sgTerreno.Cells[sgTerreno.Col,sgTerreno.Row]:=cbMunicipio.Text;
   sgTerreno.Col:=sgTerreno.Col+1;
   sgTerreno.SetFocus;
  end;
end;

procedure TPrincipal.cbTipoChange(Sender: TObject);
begin
Cargardatos;
end;



procedure TPrincipal.cbZonaChange(Sender: TObject);
begin
 //sgTerreno.Cells[sgTerreno.Col,sgTerreno.Row]:=cbZona.Text;
end;

procedure TPrincipal.cbZonaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 {if Key=VK_RETURN then
  begin
   sgTerreno.Cells[sgTerreno.Col,sgTerreno.Row]:=cbZona.Text;
   sgTerreno.Col:=sgTerreno.Col+1;
   sgTerreno.SetFocus;
  end; }
end;

procedure TPrincipal.Consolidado2Click(Sender: TObject);
begin
 qryConsolidado.ParamByName('cliente').Value:=cbCliente.KeyValue;
 qryConsolidado.Open();
 qryConsolidado.Refresh;
 rpConsolidado.PrepareReport;
 rpConsolidado.ShowPreparedReport;
end;

procedure TPrincipal.formatogrid();
begin
sgTerreno.Cols[0].Text:='Folio No.';
sgTerreno.ColWidths[0]:=180;
sgTerreno.Cols[1].Text:='Campo';
sgTerreno.ColWidths[1]:=70;
sgTerreno.Cols[2].Text:='Direccion';
sgTerreno.ColWidths[2]:=250;
sgTerreno.Cols[3].Text:='Municipio';
sgTerreno.ColWidths[3]:=150;
sgTerreno.Cols[4].Text:='Este mts.';
sgTerreno.ColWidths[4]:=140;
sgTerreno.Cols[5].Text:='Norte mts.';
sgTerreno.ColWidths[5]:=180;
sgTerreno.Cols[6].Text:='Numero';
sgTerreno.ColWidths[6]:=130;
sgTerreno.Cols[7].Text:='Metros M�';
sgTerreno.ColWidths[7]:=160;
sgTerreno.Cols[8].Text:='Acres';
sgTerreno.ColWidths[8]:=120;
sgTerreno.Cols[9].Text:=' ';  //traspaso
sgTerreno.ColWidths[9]:=0;
sgTerreno.Cols[10].Text:='Zona';
sgTerreno.ColWidths[10]:=110;
sgTerreno.RowCount:=2;
sgTerreno.Cells[0,1]:='';
sgTerreno.Cells[1,1]:='';
sgTerreno.Cells[2,1]:='';
sgTerreno.Cells[3,1]:='';
sgTerreno.Cells[4,1]:='';
sgTerreno.Cells[5,1]:='';
sgTerreno.Cells[6,1]:='';
sgTerreno.Cells[7,1]:='';
sgTerreno.Cells[8,1]:='';
sgTerreno.Cells[9,1]:='';
sgTerreno.Cells[10,1]:='';
end;

procedure TPrincipal.btnHistTrasladosClick(Sender: TObject);
begin
qryHistorialTraslados.ParamByName('cli').Value:=cbCliente.KeyValue;
qryHistorialTraslados.Open();
qryHistorialTraslados.Refresh;
rptHistorialTraslados.PrepareReport;
rptHistorialTraslados.ShowPreparedReport;
end;

procedure TPrincipal.btnImpuestosClick(Sender: TObject);
var
FormOtrosImpuestos:TFormImpuestos;
begin
  FormOtrosImpuestos:= TFormImpuestos.Create(nil);
  FormOtrosImpuestos.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormOtrosImpuestos.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormOtrosImpuestos.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormOtrosImpuestos.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormOtrosImpuestos.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  FormOtrosImpuestos.cbCliente.KeyValue:=cbCliente.KeyValue;
  FormOtrosImpuestos.ShowModal;
end;

procedure TPrincipal.DeleteRow(Grid: TStringGrid; ARow: Integer);
var
  i: Integer;
begin
  for i := ARow to Grid.RowCount - 2 do
    Grid.Rows[i].Assign(Grid.Rows[i + 1]);
  Grid.RowCount := Grid.RowCount - 1;
end;

end.
