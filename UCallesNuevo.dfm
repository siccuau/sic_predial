object FormCallesNuevo: TFormCallesNuevo
  Left = 0
  Top = 0
  Caption = 'Calles'
  ClientHeight = 164
  ClientWidth = 244
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblNombre: TLabel
    Left = 16
    Top = 32
    Width = 42
    Height = 13
    Caption = 'NOMBRE'
  end
  object btnGuardar: TButton
    Left = 152
    Top = 104
    Width = 75
    Height = 25
    Caption = 'Guardar'
    TabOrder = 0
    OnClick = btnGuardarClick
  end
  object txtNombre: TEdit
    Left = 16
    Top = 64
    Width = 211
    Height = 21
    TabOrder = 1
  end
  object btnEliminar: TButton
    Left = 170
    Top = -1
    Width = 75
    Height = 25
    Caption = 'ELIMINAR'
    TabOrder = 2
    OnClick = btnEliminarClick
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    LoginPrompt = False
    Left = 15
    Top = 111
  end
end
