unit UPropietariosTraspasos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Vcl.DBCtrls, Vcl.StdCtrls, Data.DB,
  FireDAC.Comp.Client, Vcl.ComCtrls, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, Vcl.Grids, Vcl.DBGrids,UCliente,
  Vcl.Menus;

type
  TPropietariosTraspasos = class(TForm)
    StatusBar1: TStatusBar;
    Conexion: TFDConnection;
    Label1: TLabel;
    cbCliente: TDBLookupComboBox;
    qryClientes: TFDQuery;
    dsClientes: TDataSource;
    Label2: TLabel;
    Label3: TLabel;
    lblBuscar: TLabel;
    dbgListaClientes: TDBGrid;
    cbAll: TCheckBox;
    CheckBox1: TCheckBox;
    txtBucar: TEdit;
    lbPropietarios: TListBox;
    btnGuardar: TButton;
    dsListaClientes: TDataSource;
    qryListaClientes: TFDQuery;
    qryListaClientesSELECCIONADO: TStringField;
    qryListaClientesNOMBRE: TStringField;
    qryListaClientesCLIENTE_ID: TIntegerField;
    qryPropietarios: TFDQuery;
    MainMenu1: TMainMenu;
    NuevoCliente1: TMenuItem;
    procedure cbAllClick(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure txtBucarChange(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dbgListaClientesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure NuevoCliente1Click(Sender: TObject);
  private
    { Private declarations }
  public
  id_terreno,cliente_id:integer;
  tipo:string;
    { Public declarations }
  end;

var
  PropietariosTraspasos: TPropietariosTraspasos;

implementation

{$R *.dfm}

procedure TPropietariosTraspasos.btnGuardarClick(Sender: TObject);
var
I,id_cliente,buttonSelected:Integer;
strA,separador,mensaje: string;
strArray: TArray<string>;
bandera:Boolean;
begin
 if lbPropietarios.Items.Count=0 then
    begin
      buttonSelected := messagedlg('La lista de propietarios esta vacia desea guardarla',mtError, mbOKCancel, 0);
      mensaje:='';

    end
  else
    begin
      buttonSelected:=1;
    end;
    if buttonSelected = mrOK then
        begin

        Conexion.ExecSQL('delete  from sic_predial_propietario where id_terreno='+id_terreno.ToString+'');
        for I := 0 to lbPropietarios.Items.Count-1 do
          begin
            strA:=lbPropietarios.Items[I];
            try
              id_cliente:=Conexion.ExecSQLScalar('select cliente_id from clientes where upper(nombre)=:nom',[uppercase(strA)]);
              Conexion.ExecSQL('INSERT INTO SIC_PREDIAL_PROPIETARIO(ID_PROPIETARIO,ID_TERRENO,ID_CLIENTE) VALUES(-1,'+id_terreno.ToString+','+id_cliente.ToString+')');
               bandera:=True;
               mensaje:='Se han agregado los propietarios';
            Except on E : Exception do
              begin
              mensaje:=E.Message;
              bandera:=False;
              Conexion.Rollback;
              end;

            end;
          end;

          if bandera then
            begin
            ShowMessage(mensaje);
              ModalResult:=MrOk;
            end
          else if mensaje='' then Close
          else ShowMessage(mensaje);
      end;

end;

procedure TPropietariosTraspasos.cbAllClick(Sender: TObject);
var
I,posicion,k:Integer;
bandera:Boolean;
begin

 qryListaClientes.DisableControls;
  //qryListaClientes.IndexFieldNames:='SELECCIONADO:D';
  qryListaClientes.First;
  while not qryListaClientes.Eof do
  begin

          if cbAll.Checked then
          begin
          qryListaClientes.Edit;
          qryListaClientesSELECCIONADO.Value := 'S';
          qryListaClientes.Post;
            bandera:=False;
           for I := 0 to lbPropietarios.Items.Count-1 do
            begin
               //if AnsiPos(qryListaClientes.Fields[1].AsString,lbPropietarios.Items[I])=1 then
               if lbPropietarios.Items[I] = qryListaClientes.Fields[1].AsString then
                begin
                  bandera:=True;
                end;
            end;
           if bandera <> True then lbPropietarios.Items.Add(qryListaClientes.Fields[1].AsString);

          qryListaClientes.Next;
          end
           else
           begin
            qryListaClientes.Edit;
            qryListaClientesSELECCIONADO.Value := 'N';
            qryListaClientes.Post;
             bandera:=False;
            for I := 0 to lbPropietarios.Items.Count-1 do
            begin
               //if AnsiPos(qryListaClientes.Fields[1].AsString,lbPropietarios.Items[I])=1 then
               if lbPropietarios.Items[I] = qryListaClientes.Fields[1].AsString then
                begin
                  bandera:=True;
                  posicion:=I;
                end;
            end;
           if bandera = True then lbPropietarios.Items.Delete(posicion);
            qryListaClientes.Next;
           end;
  end;
  qryListaClientes.First;
  qryListaClientes.EnableControls;
  CheckBox1.Checked := qryListaClientesSELECCIONADO.Value = 'S';
end;

procedure TPropietariosTraspasos.CheckBox1Click(Sender: TObject);
var
Itm: TListItem;
I,posicion:Integer;
bandera:Boolean;
begin
 qryListaClientes.Edit;
 bandera:=False;
  if CheckBox1.Checked then
    begin
        qryListaClientesSELECCIONADO.Value := 'S';
         for I := 0 to lbPropietarios.Items.Count-1 do
          begin
             //if AnsiPos(qryListaClientes.Fields[1].AsString,lbPropietarios.Items[I])=1 then
             if lbPropietarios.Items[I] = qryListaClientes.Fields[1].AsString then
              begin
                bandera:=True;
              end;
          end;
         if bandera <> True then lbPropietarios.Items.Add(qryListaClientes.Fields[1].AsString);


    end
  else
    begin
      qryListaClientesSELECCIONADO.Value := 'N';

          for I := 0 to lbPropietarios.Items.Count-1 do
          begin
             //if AnsiPos(qryListaClientes.Fields[1].AsString,lbPropietarios.Items[I])=1 then
             if lbPropietarios.Items[I] = qryListaClientes.Fields[1].AsString then
              begin
                bandera:=True;
                posicion:=I;
              end;
          end;
         if bandera = True then lbPropietarios.Items.Delete(posicion);



    end;


  //qryListaClientes.IndexFieldNames:='SELECCIONADO:D';
  qryListaClientes.Post;
end;

procedure TPropietariosTraspasos.dbgListaClientesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
   const IsChecked : array[Boolean] of Integer =
 (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED);
 var
 DrawState: Integer;
 DrawRect: TRect;
begin
if Column.FieldName.ToUpper = 'SELECCIONADO' then
  begin
    if (gdSelected in State) and (qryListaClientes.RecordCount > 0) then
    begin
      with CheckBox1 do
      begin
        Left := Rect.Left + dbgListaClientes.Left + 2;
        Top := Rect.Top + dbgListaClientes.Top + 2;
        Height := Rect.Height;
        Width := Rect.Width;
        Color := dbgListaClientes.Canvas.Brush.Color;
        Visible := true;
        Checked := qryListaClientesSELECCIONADO.Value = 'S';
      end;
    end
    else
    begin

      DrawRect:=Rect;
     InflateRect(DrawRect,-1,-1);

     DrawState := ISChecked[Column.Field.AsString = 'S'];
     dbgListaClientes.Canvas.FillRect(Rect);
     DrawFrameControl(dbgListaClientes.Canvas.Handle, DrawRect,
     DFC_BUTTON, DrawState);
    end;
  end;
end;

procedure TPropietariosTraspasos.FormShow(Sender: TObject);
var
I:Integer;
bandera:Boolean;
valor:Boolean;
begin
qryClientes.Open();
qryListaClientes.Open();
qryListaClientes.Filter := 'NOT(CLIENTE_ID='+cliente_id.ToString+') ';
qryListaClientes.Filtered := True;

if Conexion.ExecSQLScalar('SELECT COUNT(ID_PROPIETARIO) FROM SIC_PREDIAL_PROPIETARIO WHERE ID_TERRENO='+id_terreno.ToString+'')>0 then
 begin


     qryPropietarios.SQL.Text:='SELECT cli.nombre,cli.cliente_id FROM SIC_PREDIAL_PROPIETARIO spp left join CLIENTES cli on spp.id_cliente=cli.cliente_id WHERE spp.ID_TERRENO=:terreno_id';
     qryPropietarios.ParamByName('terreno_id').Value:=id_terreno;

   qryPropietarios.Open();
   qryPropietarios.First;
   while not qryPropietarios.Eof do
   begin

          for I := 0 to lbPropietarios.Items.Count-1 do
          begin
             if AnsiPos(qryListaClientes.Fields[2].AsString,lbPropietarios.Items[I])=1 then
             //if lbPropietarios.Items[I] = qryListaClientes.Fields[1].AsString then
              begin
                bandera:=True;
              end;
          end;
         if bandera <> True then lbPropietarios.Items.Add(qryPropietarios.FieldByName('NOMBRE').AsString);
         qryListaClientes.First;
         while not qryListaClientes.Eof do
          begin
            if qryPropietarios.FieldByName('CLIENTE_ID').Value=qryListaClientes.FieldByName('CLIENTE_ID').Value then
            begin
              qryListaClientes.Edit;
              qryListaClientes.FieldByName('SELECCIONADO').Value:='S';
              valor:=True;
            end;

            qryListaClientes.Next;
          end;


    qryPropietarios.Next;
   end;
   if valor then
     begin
      qryListaClientes.IndexFieldNames:='SELECCIONADO:D';
     //qryListaClientes.Filtered := True;
     end;
     qryListaClientes.First;
 end
 else
 begin
  qryPropietarios.ClearBlobs;
 end;
end;

procedure TPropietariosTraspasos.NuevoCliente1Click(Sender: TObject);
var
FormCliente:TClienteForm;
begin
  FormCliente:= TClienteForm.Create(nil);
  FormCliente.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormCliente.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormCliente.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormCliente.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormCliente.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  FormCliente.ShowModal;
  qryListaClientes.Refresh;
end;

procedure TPropietariosTraspasos.txtBucarChange(Sender: TObject);
begin
   if txtBucar.Text = '' then
    begin
      qryListaClientes.Filtered := False;
    end
    else
    begin
      qryListaClientes.Filter := 'upper(NOMBRE) like ''%'+AnsiUpperCase(txtBucar.Text)+'%'' AND NOT(CLIENTE_ID='+cliente_id.ToString+')';
      qryListaClientes.Filtered := True;
    end;
end;


end.
