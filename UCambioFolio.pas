unit UCambioFolio;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, Vcl.ComCtrls, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.DBCtrls;

type
  TCambioFolio = class(TForm)
    cbCliente: TDBLookupComboBox;
    Label1: TLabel;
    txtFolio: TEdit;
    Label2: TLabel;
    btnAceptar: TButton;
    btnCancelar: TButton;
    Conexion: TFDConnection;
    qryClientes: TFDQuery;
    dsClientes: TDataSource;
    Label3: TLabel;
    Label4: TLabel;
    StatusBar1: TStatusBar;
    txtFolioNuevo: TEdit;
    procedure FormShow(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
  public
  cliente_id:integer;
  folio,tipo:string;
    { Public declarations }
  end;

var
  CambioFolio: TCambioFolio;

implementation

{$R *.dfm}

procedure TCambioFolio.btnAceptarClick(Sender: TObject);
begin
 if txtFolio.Text<>'' then
  begin
   if Conexion.ExecSQLScalar('select count(folio) from sic_predial_terrenos where folio=:folio and cliente_id=:cid and tipo=:tipo',[txtFolioNuevo.Text,cbCliente.KeyValue,tipo])=0 then
     begin
      Conexion.ExecSQL('update sic_predial_terrenos set folio=:fn where folio=:folio and cliente_id=:cid and tipo=:tipo',[txtFolioNuevo.Text,txtFolio.Text,cbCliente.KeyValue,tipo]);
      Conexion.ExecSQL('insert into sic_predial_bitacora values(-1,:f,:cli,''CAMBIO FOLIO'',:f1,:f2,null,current_user,current_date)',[txtFolio.Text,cbCliente.KeyValue,txtFolio.Text,txtFolioNuevo.Text]);
      Conexion.Commit;
      ShowMessage('Se actualizo el folio correctamente.');
      ModalResult:=MrOk;
     end else ShowMessage('El folio nuevo ingresado ya existe.');
  end else ShowMessage('El folio nuevo no puede estar vacio.');
end;

procedure TCambioFolio.btnCancelarClick(Sender: TObject);
begin
Close;
end;

procedure TCambioFolio.FormShow(Sender: TObject);
begin
txtFolio.Text:=folio;
qryClientes.Open();
end;

end.
