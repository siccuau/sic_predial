unit UMedidas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  Vcl.StdCtrls;

type
  TMedidas = class(TForm)
    lblNombre: TLabel;
    txtNombre: TEdit;
    btnGuardar: TButton;
    Conexion: TFDConnection;
    txtSat: TEdit;
    Label1: TLabel;
    procedure btnGuardarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Medidas: TMedidas;

implementation

{$R *.dfm}

procedure TMedidas.btnGuardarClick(Sender: TObject);
begin
if (txtNombre.Text = '') then  ShowMessage('Faltan datos.')
else
  begin
    try
    Conexion.ExecSQL('INSERT INTO UNIDADES_VENTA(UNIDAD_VENTA_ID,UNIDAD_VENTA,CLAVE_SAT) VALUES(-1,'''+txtNombre.Text+''','''+txtSat.Text+''')') ;
    ShowMessage('Medida agregada');
    Close;
    Except
    ShowMessage('Hubo un error al agregar la medida');
    end;
  end;
end;

end.
