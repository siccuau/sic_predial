unit UImportarTerrenos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,ComObj,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  Vcl.StdCtrls, Vcl.ComCtrls;

type
  TImportarTerrenos = class(TForm)
    lbl_archivo: TLabel;
    Label1: TLabel;
    lbl_progress: TLabel;
    pb_articulos: TProgressBar;
    btn_archivo: TButton;
    btn_importar: TButton;
    Conexion: TFDConnection;
    OpenDialog1: TOpenDialog;
    procedure btn_archivoClick(Sender: TObject);
    procedure btn_importarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ImportarTerrenos: TImportarTerrenos;

implementation

{$R *.dfm}

procedure TImportarTerrenos.btn_archivoClick(Sender: TObject);
begin
  if OpenDialog1.Execute then
  begin
    btn_importar.Enabled := True;
    lbl_archivo.Caption := OpenDialog1.FileName;
  end
  else
  begin
    btn_importar.Enabled := False;
    lbl_archivo.Caption := '';
  end;
end;

procedure TImportarTerrenos.btn_importarClick(Sender: TObject);
var
  XL: OleVariant;
  j,i,num_rows,conteo,cond_pago_id,id_cliente,consecutivo:integer;
  tipo,folio,zona,mts_este,mts_norte,numero,metros,acres,campo,calle,avenida,
  calle_completo,fraccionamiento,colonia,direccion,municipio,traspaso,nuevo_folio:string;
begin
   //INICIALIZAR CONTEO
  conteo:=0;
   // SE CREA LA INSTANCIA DEL ARCHIVO DE EXCEL
  XL := CreateOleObject('Excel.Application');
  XL.WorkBooks.Open(OpenDialog1.FileName);
  num_rows := XL.Activesheet.UsedRange.Rows.Count;
  pb_articulos.Position := 0;
  pb_articulos.Max := num_rows;

  //SE RECORRE DOCUMENTO EXCEL
  for i := 2 to num_rows do
  begin
  id_cliente:=0;
    //SE OBTIENEN LOS DATOS POR CADA FILA
    tipo:=XL.ActiveSheet.Cells[i,1].Value;
    id_cliente:=Conexion.ExecSQLScalar('select cliente_id from dirs_clientes where upper(rfc_curp)=upper(:rfc)',[XL.ActiveSheet.Cells[i,2].Value]);
    folio:=XL.ActiveSheet.Cells[i,13].Value;
    zona:=XL.ActiveSheet.Cells[i,21].Value;
    mts_este:=XL.ActiveSheet.Cells[i,17].Value;
    mts_norte:=XL.ActiveSheet.Cells[i,18].Value;
    numero:=XL.ActiveSheet.Cells[i,8].Value;
    metros:=XL.ActiveSheet.Cells[i,19].Value;
    acres:=XL.ActiveSheet.Cells[i,20].Value;
    campo:=XL.ActiveSheet.Cells[i,14].Value;
    direccion:=XL.ActiveSheet.Cells[i,15].Value;
    municipio:=XL.ActiveSheet.Cells[i,16].Value;
    traspaso:='N';

    //VERIFICAR QUE NO ESTE EN VERDE
    if XL.ActiveSheet.Cells[i,1].Interior.ColorIndex <> 4 then
    begin
    //INSERTAR TERRENO A BD
    if id_cliente<>0 then
    begin
    //VERIFICA QUE NO EXISTA EL TERRENO EN ESE CLIENTE
      if Conexion.ExecSQLScalar('select count(*) from sic_predial_terrenos where cliente_id=:cid and folio=:f',[id_cliente,folio])=0 then
       begin
        // try
        //EN CASO DE SER FOLIO 0 GENERAR CONSECUTIVO
        if folio='0' then
        begin
        consecutivo:=Conexion.ExecSQLScalar('select consecutivo from folios_conceptos where sistema=''IN'' and serie=''SN''');
        nuevo_folio:='SN'+Format('%.*d',[7, consecutivo]);
        //ACTUALIZAR FOLIO
        Conexion.ExecSQL('update folios_conceptos set consecutivo=:consec where sistema=''IN'' and Serie=''SN''',[consecutivo+1]);
        folio:=nuevo_folio;
        end;
         Conexion.ExecSQL('INSERT INTO SIC_PREDIAL_TERRENOS(ID_TERRENO,TIPO,CLIENTE_ID,FOLIO,ZONA,MTS_ESTE,MTS_NORTE,NUMERO,METROS,ACRES,CAMPO,DIRECCION,MUNICIPIO,TRASPASO) VALUES(-1,upper(:p1),:p2,upper(:p3),upper(:p4),:p5,:p6,upper(:p7),:p8,:p9,upper(:p10),:p11,upper(:p12),:p13)',[tipo,id_cliente,folio,zona,mts_este,mts_norte,numero,metros,acres,campo,direccion,municipio,traspaso]);
         //pintar de verde
         for j := 1 to 21 do
         begin
         XL.ActiveSheet.Cells[i,j].Interior.ColorIndex:=4;
         end;

         conteo:=conteo+1;
         Conexion.Commit;
       {  except
         Conexion.Rollback;
         end; }
       end;
    end;
   end;

          //INFORMACION VISUAL
      lbl_progress.Caption := inttostr(i-1)+'/'+inttostr(num_rows-1);
      pb_articulos.Position := pb_articulos.Position + 1;

  end;
  XL.DisplayAlerts := False;
  XL.ActiveWorkbook.SaveAs(OpenDialog1.FileName);
  XL.Workbooks.close;
  XL.Quit;
  XL := Unassigned;
  showmessage('Proceso Terminado, se agregaron '+inttostr(conteo)+' terrenos nuevos.');
  close;
end;

end.
