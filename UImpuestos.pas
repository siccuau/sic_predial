unit UImpuestos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Vcl.StdCtrls, Vcl.Grids, Data.DB,
  FireDAC.Comp.Client, Vcl.ComCtrls, FireDAC.Comp.DataSet, Vcl.DBCtrls,UCatalogoImpuestos,
  Vcl.Menus;

type
  TFormImpuestos = class(TForm)
    cbCliente: TDBLookupComboBox;
    Label1: TLabel;
    qryClientes: TFDQuery;
    dsClientes: TDataSource;
    StatusBar1: TStatusBar;
    Conexion: TFDConnection;
    sgImpuestos: TStringGrid;
    Label2: TLabel;
    btnGuardar: TButton;
    PopupMenu1: TPopupMenu;
    NuevoImpuesto: TMenuItem;
    dsImpuestos: TDataSource;
    qryImpuestos: TFDQuery;
    ComboBox1: TComboBox;
    qryImpCli: TFDQuery;
    dsImpCli: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure NuevoImpuestoClick(Sender: TObject);
    procedure sgImpuestosSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure ComboBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ComboBox1Change(Sender: TObject);
    procedure sgImpuestosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnGuardarClick(Sender: TObject);
    procedure DeleteRow(Grid: TStringGrid; ARow: Integer);
    procedure CargarImpuestos();
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormImpuestos: TFormImpuestos;

implementation

{$R *.dfm}

uses Zona;

procedure TFormImpuestos.btnGuardarClick(Sender: TObject);
var texto,nombre,a�o:string;
impuesto_id,i:integer;
unidades:integer;
bandera:boolean;
pago:double;
begin
   bandera:=false;
for i := 1 to sgImpuestos.RowCount-2 do
 begin
  texto:=sgImpuestos.cells[0,i];
  nombre:=copy(texto,0,ansipos(' - ',texto)-1);
  a�o:=copy(texto,ansipos(' - ',texto)+3,Length(texto));
  unidades:=strtoint(sgImpuestos.cells[2,i]);
   pago:=strtoint(sgImpuestos.cells[4,i]);
  impuesto_id:=Conexion.ExecSQLScalar('select impuesto_id from sic_predial_imp where nombre=:nom and ano=:ano',[nombre,a�o]);
  if Conexion.ExecSQLScalar('select count(impuesto_id) from sic_predial_imp_cli where cliente_id=:cli and impuesto_id=:imp',[cbCliente.KeyValue,impuesto_id])>0 then
   begin    //ACTUALIZAR
     Conexion.ExecSQL('update sic_predial_imp_cli set '+
    'UNIDADES=:U,PAGO=:PAGO,FECHA_PAGO=current_date where cliente_id=:cli and impuesto_id=:imp',[unidades,pago,cbCliente.KeyValue,impuesto_id]);
       bandera:=true;
   end
   else
   begin
     //INSERTAR
     Conexion.ExecSQL('Insert into sic_predial_imp_cli(CLIENTE_ID,IMPUESTO_ID,UNIDADES,PAGO,FECHA_CREACION,FECHA_PAGO) '+
     'VALUES(:cliente_id,:imp,:unid,:pago,current_date,null)',[cbCliente.KeyValue,impuesto_id,unidades,pago]);
     Conexion.ExecSQL('INSERT INTO sic_predial_bitacora VALUES(gen_id(SIC_ID_BITACORA,1),:folio,:cli,''IMPUESTO NUEVO'',''0'',:metros,null,current_user, CURRENT_TIMESTAMP)',[texto,cbCliente.KeyValue,unidades]);
        bandera:=true;
   end;
 end;
 if bandera=true then
  begin
    ShowMessage('Se guardo correctamente.');
    close;
  end else ShowMessage('ERROR no se pudo guardar.');
end;

procedure TFormImpuestos.ComboBox1Change(Sender: TObject);
begin
   sgImpuestos.Cells[sgImpuestos.Col,sgImpuestos.Row]:=ComboBox1.Text;
end;

procedure TFormImpuestos.ComboBox1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var texto:string;
   nombre,a�o:string;
  impuesto_id:integer;
  buttonSelected,unidades : Integer;
begin
 if Key=VK_RETURN then
  begin
   sgImpuestos.Cells[sgImpuestos.Col,sgImpuestos.Row]:=ComboBox1.Text;
   texto:=ComboBox1.Text;
   texto:=copy(texto,0,ansipos(' - ',texto)-1);
   sgImpuestos.Cells[sgImpuestos.Col+1,sgImpuestos.Row]:=Conexion.ExecSQLScalar('Select medida from sic_predial_imp where nombre=:nom',[texto]);
   sgImpuestos.Col:=sgImpuestos.Col+2;
   sgImpuestos.SetFocus;
  end;

   If (Shift = [ssctrl]) and (key = VK_DELETE) then
  begin
     texto:=sgImpuestos.cells[0,sgImpuestos.Row];
   nombre:=copy(texto,0,ansipos(' - ',texto)-1);
   a�o:=copy(texto,ansipos(' - ',texto)+3,Length(texto));
   impuesto_id:=Conexion.ExecSQLScalar('select impuesto_id from sic_predial_imp where nombre=:nom and ano=:ano',[nombre,a�o]);
    unidades:=strtoint(sgImpuestos.cells[2,sgImpuestos.Row]);
   buttonSelected := messagedlg('Desea eliminar este impuesto "'+texto+'"?',mtCustom,[mbYes,mbCancel], 0);
    if buttonSelected = mrYes    then
    begin
    //ELIMINAR DE BD
    Conexion.ExecSQL('delete from sic_predial_imp_cli where cliente_id=:cli and impuesto_id=:imp',[cbCliente.KeyValue,impuesto_id]);
    Conexion.ExecSQL('INSERT INTO sic_predial_bitacora VALUES(gen_id(SIC_ID_BITACORA,1),:folio,:cli,''ELIMINAR IMPUESTO'',:unid,''0'',null,current_user, CURRENT_TIMESTAMP)',[texto,cbCliente.KeyValue,unidades]);
    //ELIMINAR LINEA
     DeleteRow(sgImpuestos, sgImpuestos.Row);
          Combobox1.Visible:=false;
     //CargarImpuestos();
    end;
  end;
end;

procedure TFormImpuestos.FormShow(Sender: TObject);
var i:integer;
nombre,medida:string;
unidades:double;
begin
sgImpuestos.Cols[0].Text:='Impuesto';
sgImpuestos.ColWidths[0]:=280;
sgImpuestos.Cols[1].Text:='Unidades';
sgImpuestos.ColWidths[1]:=170;
sgImpuestos.Cols[2].Text:='Cantidad';
sgImpuestos.ColWidths[2]:=70;
sgImpuestos.Cols[3].Text:='Total';
sgImpuestos.ColWidths[3]:=100;
sgImpuestos.Cols[4].Text:='Pago';
sgImpuestos.ColWidths[4]:=100;
qryImpuestos.Open();
qryClientes.Open();


//Llenar combobox impuestos
qryImpuestos.First;
while not qryImpuestos.Eof do
  begin
  ComboBox1.Items.Add(qryImpuestos.FieldByName('NOMBRE').Value+' - '+qryImpuestos.FieldByName('ANO').Value);
  qryImpuestos.Next;
  end;
  ComboBox1.ItemIndex:=0;

cargarimpuestos();
end;

procedure TFormImpuestos.NuevoImpuestoClick(Sender: TObject);
var
FormCatalogoImpuestos:TCatalogoImpuestos;
begin
  FormCatalogoImpuestos:= TCatalogoImpuestos.Create(nil);
  FormCatalogoImpuestos.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormCatalogoImpuestos.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormCatalogoImpuestos.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormCatalogoImpuestos.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormCatalogoImpuestos.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  FormCatalogoImpuestos.ShowModal;
  qryImpuestos.Refresh;
  ComboBox1.Items.Clear;
  qryImpuestos.First;
  while not qryImpuestos.Eof do
  begin
  ComboBox1.Items.Add(qryImpuestos.FieldByName('nombre').Value+' - '+qryImpuestos.FieldByName('ano').Value);
    qryImpuestos.Next;
  end;
end;

procedure TFormImpuestos.sgImpuestosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var nombre,a�o,texto:string;
  impuesto_id:integer;
  buttonSelected : Integer;
  unidades:integer;
begin
  if Key=VK_RETURN then
  begin
     if sgImpuestos.Col=4 then
       begin
        if sgImpuestos.Cells[4,sgImpuestos.Row]>sgImpuestos.Cells[3,sgImpuestos.Row] then
        begin
          ShowMessage('La cantidad a pagar no debe de ser mayor al total.');
          sgImpuestos.Cells[4,sgImpuestos.Row]:='0';
        end
        else
          begin
          sgImpuestos.Col:=0;
          if sgImpuestos.Row=sgImpuestos.RowCount-1 then
          begin
          sgImpuestos.RowCount:=sgImpuestos.RowCount+1;
          end;
          sgImpuestos.Row:=sgImpuestos.Row+1;
         end;
       end;

  if sgImpuestos.Col=2 then
       begin
       texto:=sgImpuestos.cells[0,sgImpuestos.Row];
       nombre:=copy(texto,0,ansipos(' - ',texto)-1);
       a�o:=copy(texto,ansipos(' - ',texto)+3,Length(texto));
       impuesto_id:=Conexion.ExecSQLScalar('select impuesto_id from sic_predial_imp where nombre=:nom and ano=:ano',[nombre,a�o]);
       sgImpuestos.cells[sgImpuestos.Col+1,sgImpuestos.Row]:=floattostr(strtofloat(sgImpuestos.cells[sgImpuestos.Col,sgImpuestos.Row])*Conexion.ExecSQLScalar('select valor from sic_predial_imp where impuesto_id=:id',[impuesto_id]));
        sgImpuestos.Col:=sgImpuestos.Col+2;
        sgImpuestos.Cells[4,sgImpuestos.Row]:='0';
      end;
  end;

  If (Shift = [ssctrl]) and (key = VK_DELETE) then
  begin
   texto:=sgImpuestos.cells[0,sgImpuestos.Row];
   nombre:=copy(texto,0,ansipos(' - ',texto)-1);
   a�o:=copy(texto,ansipos(' - ',texto)+3,Length(texto));
   impuesto_id:=Conexion.ExecSQLScalar('select impuesto_id from sic_predial_imp where nombre=:nom and ano=:ano',[nombre,a�o]);
     unidades:=strtoint(sgImpuestos.cells[2,sgImpuestos.Row]);
   buttonSelected := messagedlg('Desea eliminar este impuesto "'+texto+'"?',mtCustom,[mbYes,mbCancel], 0);
    if buttonSelected = mrYes    then
    begin
    //ELIMINAR DE BD
    Conexion.ExecSQL('delete from sic_predial_imp_cli where cliente_id=:cli and impuesto_id=:imp',[cbCliente.KeyValue,impuesto_id]);
    Conexion.ExecSQL('INSERT INTO sic_predial_bitacora VALUES(gen_id(SIC_ID_BITACORA,1),:folio,:cli,''ELIMINAR IMPUESTO'',:unid,''0'',null,current_user, CURRENT_TIMESTAMP)',[texto,cbCliente.KeyValue,unidades]);
    //ELIMINAR LINEA
     DeleteRow(sgImpuestos, sgImpuestos.Row);
     Conexion.Commit;
    // ComboBox1.ItemIndex:=0;
     Combobox1.Visible:=false;
     //CargarImpuestos();
    end;
  end;
end;

procedure TFormImpuestos.sgImpuestosSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
R: TRect;
begin
if (ACol = 0) and (ARow <> 0) then
 begin
  R := sgImpuestos.CellRect(ACol, ARow);
  R.Left := R.Left + sgImpuestos.Left;
  R.Right := R.Right + sgImpuestos.Left;
  R.Top := R.Top + sgImpuestos.Top;
  R.Bottom := R.Bottom + sgImpuestos.Top;
    with ComboBox1 do
    begin
    Left := R.Left + 1;
    Top := R.Top + 2;
    Width := (R.Right + 1) - R.Left;
    Height := (R.Bottom + 1) - R.Top;
    Visible := True;
    SetFocus;
    end;
 end else Combobox1.Visible:=false;
end;

procedure TFormImpuestos.DeleteRow(Grid: TStringGrid; ARow: Integer);
var
  i: Integer;
begin
  for i := ARow to Grid.RowCount - 2 do
    Grid.Rows[i].Assign(Grid.Rows[i + 1]);
  Grid.RowCount := Grid.RowCount - 1;
end;

procedure TFormImpuestos.CargarImpuestos();
var i:integer;
nombre,medida:string;
unidades,pago:double;
begin
//Llenar Stringgrid
sgImpuestos.RowCount:=2;
sgImpuestos.Cols[0].Text:='Impuesto';
sgImpuestos.ColWidths[0]:=280;
sgImpuestos.Cols[1].Text:='Unidades';
sgImpuestos.ColWidths[1]:=170;
sgImpuestos.Cols[2].Text:='Cantidad';
sgImpuestos.ColWidths[2]:=70;
sgImpuestos.Cols[3].Text:='Total';
sgImpuestos.ColWidths[3]:=100;
sgImpuestos.Cols[4].Text:='Pago';
sgImpuestos.ColWidths[4]:=100;
 i:=1;
         qryImpCli.ParamByName('cli').Value:=cbCliente.KeyValue;
         qryImpCli.Open();
         qryImpCli.Refresh;
         qryImpCli.First;
         while not qryImpCli.Eof do
         begin
         nombre:=qryImpCli.FieldByName('nombre').Value+' - '+qryImpCli.FieldByName('ano').Value;
         medida:=qryImpCli.FieldByName('medida').Value;
         unidades:=qryImpCli.FieldByName('unidades').Value;
         pago:=qryImpCli.FieldByName('pago').Value;
          sgImpuestos.Cells[0,i]:=nombre;
          sgImpuestos.Cells[1,i]:=medida;
          sgImpuestos.Cells[2,i]:=floattostr(unidades);
          sgImpuestos.Cells[3,i]:=floattostr(unidades*qryImpCli.FieldByName('valor').Value);
          sgImpuestos.Cells[4,i]:=floattostr(pago);
          i:=i+1;
          sgImpuestos.RowCount:=sgImpuestos.RowCount+1;
          qryImpCli.Next;
         end;

qryImpCli.ParamByName('cli').Value:=cbCliente.KeyValue;
qryImpCli.Open();

if qryImpCli.RecordCount=0 then ComboBox1.Visible:=true else ComboBox1.Visible:=false;
end;

end.
