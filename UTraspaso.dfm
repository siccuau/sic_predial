object Traspaso: TTraspaso
  Left = 0
  Top = 0
  Caption = 'Traspaso'
  ClientHeight = 273
  ClientWidth = 974
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 32
    Width = 71
    Height = 25
    Caption = 'Cliente:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 37
    Top = 79
    Width = 50
    Height = 25
    Caption = 'Folio:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 208
    Top = 1
    Width = 25
    Height = 25
    Caption = 'De'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 432
    Top = 56
    Width = 116
    Height = 25
    Caption = 'Traspasar a:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 568
    Top = 32
    Width = 71
    Height = 25
    Caption = 'Cliente:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 568
    Top = 82
    Width = 90
    Height = 25
    Caption = 'Cantidad:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 9
    Top = 128
    Width = 102
    Height = 25
    Caption = 'Metros M'#178':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object cbCliente: TDBLookupComboBox
    Left = 93
    Top = 32
    Width = 316
    Height = 27
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyField = 'CLIENTE_ID'
    ListField = 'NOMBRE'
    ListSource = dsClientes
    ParentFont = False
    TabOrder = 0
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 254
    Width = 974
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object txtFolio: TEdit
    Left = 93
    Top = 81
    Width = 188
    Height = 27
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Text = 'Edit1'
  end
  object cbCliente2: TDBLookupComboBox
    Left = 645
    Top = 32
    Width = 316
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyField = 'CLIENTE_ID'
    ListField = 'NOMBRE'
    ListSource = dsClientes2
    ParentFont = False
    TabOrder = 3
  end
  object btnAceptar: TButton
    Left = 360
    Top = 177
    Width = 113
    Height = 45
    Caption = 'Aceptar'
    TabOrder = 4
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 488
    Top = 177
    Width = 113
    Height = 45
    Caption = 'Cancelar'
    TabOrder = 5
    OnClick = btnCancelarClick
  end
  object cmbSuperficie: TComboBox
    Left = 809
    Top = 81
    Width = 96
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemIndex = 0
    ParentFont = False
    TabOrder = 6
    Text = 'Metros M'#178
    Items.Strings = (
      'Metros M'#178
      'Acres')
  end
  object txtCantidad: TEdit
    Left = 664
    Top = 81
    Width = 145
    Height = 27
    Alignment = taRightJustify
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
  end
  object txtMetros: TEdit
    Left = 117
    Top = 130
    Width = 164
    Height = 27
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    Text = 'Edit1'
  end
  object btnTraspasoCompleto: TButton
    Left = 664
    Top = 131
    Width = 113
    Height = 30
    Caption = 'Traspaso Completo'
    TabOrder = 9
    OnClick = btnTraspasoCompletoClick
  end
  object qryClientes: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from clientes')
    Left = 776
    Top = 176
  end
  object dsClientes: TDataSource
    DataSet = qryClientes
    Left = 704
    Top = 184
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'Protocol=TCPIP'
      'Server=localhost'
      'CharacterSet=ISO8859_1'
      'RoleName=USUARIO_MICROSIP'
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    UpdateOptions.AssignedValues = [uvAutoCommitUpdates]
    UpdateOptions.AutoCommitUpdates = True
    LoginPrompt = False
    Left = 845
    Top = 128
  end
  object qryClientes2: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select c.*,dc.rfc_curp,dc.calle,dc.num_exterior,dc.colonia,(ci.n' +
        'ombre||'#39','#39'||edo.nombre_abrev)'
      
        'from clientes c join dirs_clientes dc on c.cliente_id=dc.cliente' +
        '_id'
      'join ciudades ci on ci.ciudad_id=dc.ciudad_id'
      'join estados edo on ci.estado_id=edo.estado_id')
    Left = 912
    Top = 128
  end
  object dsClientes2: TDataSource
    DataSet = qryClientes2
    Left = 936
    Top = 72
  end
  object rptConstancia: TfrxReport
    Version = '6.7.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44092.374019178240000000
    ReportOptions.LastChange = 44092.452575358800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 352
    Top = 160
    Datasets = <
      item
        DataSet = dssTerreno
        DataSetName = 'frxDBDataset1'
      end
      item
        DataSet = dssClientes
        DataSetName = 'frxDBDataset1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 279.400000000000000000
      PaperSize = 1
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object Memo1: TfrxMemoView
        AllowVectorExport = True
        Left = 1.000000000000000000
        Top = 113.385900000000000000
        Width = 737.008350000000000000
        Height = 26.456710000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Rockwell'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'CONSTANCIA DE POSESION')
        ParentFont = False
      end
      object Memo2: TfrxMemoView
        AllowVectorExport = True
        Left = 1.000000000000000000
        Top = 141.622140000000000000
        Width = 737.008350000000000000
        Height = 26.456710000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Rockwell'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'CD. CUAUHT'#201'MOC, CHIH.')
        ParentFont = False
      end
      object SysMemo1: TfrxSysMemoView
        AllowVectorExport = True
        Left = 506.457020000000000000
        Top = 139.842610000000000000
        Width = 222.992270000000000000
        Height = 18.897650000000000000
        DisplayFormat.FormatStr = 'mmmm dd, yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Rockwell'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[DATE]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo3: TfrxMemoView
        AllowVectorExport = True
        Top = 173.858380000000000000
        Width = 245.669450000000000000
        Height = 49.133890000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Frame.Typ = []
        Memo.UTF8W = (
          'UNION DE CREDITO AGRICULTORES DE'
          'CUAUHT'#201'MOC, S.A. DE C.V.'
          'KM.5 CARR. COL. A. OBREGON')
        ParentFont = False
      end
      object Memo4: TfrxMemoView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 249.448980000000000000
        Width = 510.236550000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          
            '                                        POR MEDIO DE ESTE ESCRIT' +
            'O COMUNICO A USTED QUE EL SR.')
        ParentFont = False
      end
      object frxDBDataset1NOMBRE: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Top = 268.346630000000000000
        Width = 321.260050000000000000
        Height = 15.118120000000000000
        DataField = 'NOMBRE'
        DataSet = dssClientes
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Frame.Typ = []
        Memo.UTF8W = (
          '[frxDBDataset1."NOMBRE"]')
        ParentFont = False
      end
      object Memo5: TfrxMemoView
        AllowVectorExport = True
        Left = 325.039580000000000000
        Top = 268.346630000000000000
        Width = 34.015770000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'RFC.')
        ParentFont = False
      end
      object frxDBDataset1RFC_CURP: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 362.834880000000000000
        Top = 268.346630000000000000
        Width = 222.992270000000000000
        Height = 15.118120000000000000
        DataField = 'RFC_CURP'
        DataSet = dssClientes
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Frame.Typ = []
        Memo.UTF8W = (
          '[frxDBDataset1."RFC_CURP"]')
        ParentFont = False
      end
      object Memo6: TfrxMemoView
        AllowVectorExport = True
        Left = 593.386210000000000000
        Top = 268.346630000000000000
        Width = 136.063080000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'CON DOMICILIO EN EL')
        ParentFont = False
      end
      object Memo7: TfrxMemoView
        AllowVectorExport = True
        Top = 291.023810000000000000
        Width = 45.354360000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'CAMPO')
        ParentFont = False
      end
      object frxDBDataset1CALLE: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 45.354360000000000000
        Top = 291.023810000000000000
        Width = 313.700990000000000000
        Height = 15.118120000000000000
        DataField = 'CALLE'
        DataSet = dssClientes
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[frxDBDataset1."CALLE"]')
        ParentFont = False
      end
      object Memo8: TfrxMemoView
        AllowVectorExport = True
        Left = 362.834880000000000000
        Top = 291.023810000000000000
        Width = 60.472480000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'CASA NO. ')
        ParentFont = False
      end
      object frxDBDataset1NUM_EXTERIOR: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 427.086890000000000000
        Top = 291.023810000000000000
        Width = 37.795300000000000000
        Height = 15.118120000000000000
        DataField = 'NUM_EXTERIOR'
        DataSet = dssClientes
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[frxDBDataset1."NUM_EXTERIOR"]')
        ParentFont = False
      end
      object Memo9: TfrxMemoView
        AllowVectorExport = True
        Left = 468.661720000000000000
        Top = 291.023810000000000000
        Width = 94.488250000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'DE LA COLONIA ')
        ParentFont = False
      end
      object frxDBDataset1COLONIA: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 566.929500000000000000
        Top = 291.023810000000000000
        Width = 170.078850000000000000
        Height = 15.118120000000000000
        DataField = 'COLONIA'
        DataSet = dssClientes
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[frxDBDataset1."COLONIA"]')
        ParentFont = False
      end
      object frxDBDataset1CONCATENATION: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 90.708720000000000000
        Top = 309.921460000000000000
        Width = 211.653680000000000000
        Height = 15.118120000000000000
        DataField = 'CONCATENATION'
        DataSet = dssClientes
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[frxDBDataset1."CONCATENATION"]')
        ParentFont = False
      end
      object Memo10: TfrxMemoView
        AllowVectorExport = True
        Top = 309.921460000000000000
        Width = 86.929190000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'MUNICIPIO DE ')
        ParentFont = False
      end
      object Memo11: TfrxMemoView
        AllowVectorExport = True
        Left = 306.141930000000000000
        Top = 309.921460000000000000
        Width = 427.086890000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          
            'ES POSEEDOR DEL TERRENO CON LOS SIGUIENTE'#39'S SUPERFICIE'#39'S FOLIO Y' +
            ' ')
        ParentFont = False
      end
      object Memo12: TfrxMemoView
        AllowVectorExport = True
        Top = 328.819110000000000000
        Width = 90.708720000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'COORDENADAS')
        ParentFont = False
      end
      object Shape1: TfrxShapeView
        AllowVectorExport = True
        Top = 355.275820000000000000
        Width = 733.228820000000000000
        Height = 75.590600000000000000
        Frame.Typ = []
      end
      object Line1: TfrxLineView
        AllowVectorExport = True
        Top = 404.409710000000000000
        Width = 733.228820000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
      end
      object Line2: TfrxLineView
        AllowVectorExport = True
        Left = 181.417440000000000000
        Top = 355.275820000000000000
        Height = 75.590600000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
      end
      object Line3: TfrxLineView
        AllowVectorExport = True
        Left = 287.244280000000000000
        Top = 355.275820000000000000
        Height = 75.590600000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
      end
      object Line4: TfrxLineView
        AllowVectorExport = True
        Left = 483.779840000000000000
        Top = 355.275820000000000000
        Height = 75.590600000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
      end
      object Line5: TfrxLineView
        AllowVectorExport = True
        Left = 589.606680000000000000
        Top = 355.275820000000000000
        Height = 75.590600000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
      end
      object Memo13: TfrxMemoView
        AllowVectorExport = True
        Top = 370.393940000000000000
        Width = 181.417440000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Folio No.')
        ParentFont = False
      end
      object Memo14: TfrxMemoView
        AllowVectorExport = True
        Left = 181.417440000000000000
        Top = 370.393940000000000000
        Width = 105.826840000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Zona')
        ParentFont = False
      end
      object Memo15: TfrxMemoView
        AllowVectorExport = True
        Left = 287.244280000000000000
        Top = 359.055350000000000000
        Width = 196.535560000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'UTM Coordenadas')
        ParentFont = False
      end
      object Memo16: TfrxMemoView
        AllowVectorExport = True
        Left = 483.779840000000000000
        Top = 370.393940000000000000
        Width = 105.826840000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Hectareas')
        ParentFont = False
      end
      object Memo17: TfrxMemoView
        AllowVectorExport = True
        Left = 589.606680000000000000
        Top = 370.393940000000000000
        Width = 143.622140000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Casa/Lote/Agricola')
        ParentFont = False
      end
      object Memo18: TfrxMemoView
        AllowVectorExport = True
        Left = 287.244280000000000000
        Top = 385.512060000000000000
        Width = 98.267780000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Este Mts.')
        ParentFont = False
      end
      object Memo19: TfrxMemoView
        AllowVectorExport = True
        Left = 389.291590000000000000
        Top = 385.512060000000000000
        Width = 94.488250000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'Norte Mts.')
        ParentFont = False
      end
      object Line6: TfrxLineView
        AllowVectorExport = True
        Left = 385.512060000000000000
        Top = 404.409710000000000000
        Height = 26.456710000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
      end
      object frxDBDataset1FOLIO: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 11.338590000000000000
        Top = 408.189240000000000000
        Width = 158.740260000000000000
        Height = 18.897650000000000000
        DataField = 'FOLIO'
        DataSet = dssTerreno
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDBDataset1."FOLIO"]')
        ParentFont = False
      end
      object frxDBDataset1ZONA: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 188.976500000000000000
        Top = 408.189240000000000000
        Width = 90.708720000000000000
        Height = 18.897650000000000000
        DataField = 'ZONA'
        DataSet = dssTerreno
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDBDataset1."ZONA"]')
        ParentFont = False
      end
      object frxDBDataset1MTS_ESTE: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 291.023810000000000000
        Top = 408.189240000000000000
        Width = 86.929190000000000000
        Height = 18.897650000000000000
        DataField = 'MTS_ESTE'
        DataSet = dssTerreno
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDBDataset1."MTS_ESTE"]')
        ParentFont = False
      end
      object frxDBDataset1MTS_NORTE: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 389.291590000000000000
        Top = 408.189240000000000000
        Width = 90.708720000000000000
        Height = 18.897650000000000000
        DataField = 'MTS_NORTE'
        DataSet = dssTerreno
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDBDataset1."MTS_NORTE"]')
        ParentFont = False
      end
      object frxDBDataset1TIPO: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 593.386210000000000000
        Top = 408.189240000000000000
        Width = 136.063080000000000000
        Height = 18.897650000000000000
        DataField = 'TIPO'
        DataSet = dssTerreno
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDBDataset1."TIPO"]')
        ParentFont = False
      end
      object frxDBDataset1METROS: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 487.559370000000000000
        Top = 408.189240000000000000
        Width = 98.267780000000000000
        Height = 18.897650000000000000
        DataSet = dssTerreno
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[(<frxDBDataset1."METROS">/10000)]')
        ParentFont = False
      end
      object Memo20: TfrxMemoView
        AllowVectorExport = True
        Top = 442.205010000000000000
        Width = 733.228820000000000000
        Height = 34.015770000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          
            'HACIENDO DE SU CONOCIMIENTO QUE LA SUPERFICIE DE TERRENO QUE AMP' +
            'ARA TODA LA COLONIA SE ENCUENTRA REGISTRADO EN ESCRITURAS PUBLIC' +
            'AS NUMEROS')
        ParentFont = False
      end
      object Memo21: TfrxMemoView
        AllowVectorExport = True
        Left = 207.874150000000000000
        Top = 457.323130000000000000
        Width = 68.031540000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Frame.Typ = []
        Memo.UTF8W = (
          '165 Y 166')
        ParentFont = False
      end
      object Memo22: TfrxMemoView
        AllowVectorExport = True
        Left = 275.905690000000000000
        Top = 457.323130000000000000
        Width = 733.228820000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'EXPEDIDAS ANTE EL NOTARIO PUBLICO NUMERO 2 DEL DISTRITO BRAVOS,')
        ParentFont = False
      end
      object Memo23: TfrxMemoView
        AllowVectorExport = True
        Top = 472.441250000000000000
        Width = 733.228820000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'LIC. RAFAEL D. MARTINEZ,')
        ParentFont = False
      end
      object Memo24: TfrxMemoView
        AllowVectorExport = True
        Top = 498.897960000000000000
        Width = 733.228820000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'ATENTAMENTE')
        ParentFont = False
      end
      object Memo25: TfrxMemoView
        AllowVectorExport = True
        Top = 514.016080000000000000
        Width = 733.228820000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'EL JEFE DE LA COLONIA MANITOBA')
        ParentFont = False
      end
      object Line7: TfrxLineView
        AllowVectorExport = True
        Left = 241.889920000000000000
        Top = 600.945270000000000000
        Width = 268.346630000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
      end
      object Memo26: TfrxMemoView
        AllowVectorExport = True
        Top = 608.504330000000000000
        Width = 733.228820000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'SR. XXXXXXXXXX XXXXXXXXXX FIRMA Y SELLO')
        ParentFont = False
      end
      object Memo27: TfrxMemoView
        AllowVectorExport = True
        Top = 672.756340000000000000
        Width = 733.228820000000000000
        Height = 15.118120000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'VERIFICADO POR EL JEFE Y SECRETARIO DE CAMPO')
        ParentFont = False
      end
      object Line8: TfrxLineView
        AllowVectorExport = True
        Left = 70.914892780000000000
        Top = 759.685530000000000000
        Width = 216.387833560000000000
        Color = clBlack
        Frame.Typ = [ftTop]
      end
      object Memo28: TfrxMemoView
        AllowVectorExport = True
        Left = 49.133890000000000000
        Top = 769.764276670000000000
        Width = 260.787570000000000000
        Height = 20.157493330000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'SRS. ENRIQUE FROESE REMPEL')
        ParentFont = False
      end
      object Memo29: TfrxMemoView
        AllowVectorExport = True
        Left = 430.866420000000000000
        Top = 769.764276670000000000
        Width = 260.787570000000000000
        Height = 20.157493330000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'JACOB WALL NEUFELD')
        ParentFont = False
      end
      object Line9: TfrxLineView
        AllowVectorExport = True
        Left = 457.323130000000000000
        Top = 759.685530000000000000
        Width = 216.387833560000000000
        Color = clBlack
        Frame.Typ = [ftTop]
      end
      object Memo30: TfrxMemoView
        AllowVectorExport = True
        Left = 325.039580000000000000
        Top = 770.024120000000000000
        Width = 94.488250000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'y')
        ParentFont = False
      end
      object Memo31: TfrxMemoView
        AllowVectorExport = True
        Left = 45.354360000000000000
        Top = 914.646260000000000000
        Width = 37.795300000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Frame.Typ = []
        Memo.UTF8W = (
          'Nota:')
        ParentFont = False
      end
      object Memo32: TfrxMemoView
        AllowVectorExport = True
        Left = 86.929190000000000000
        Top = 914.646260000000000000
        Width = 582.047620000000000000
        Height = 49.133890000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          
            'Esta Constancia de Posesi'#243'n ser'#225' valida '#250'nicamente con la Firma ' +
            'de Jefe de la Colonia, Jefe de Campo y Secretario donde se Ubica' +
            ' la Garant'#237'a. Deber'#225' presentar una Constancia de Posesi'#243'n por ca' +
            'da Lote otorgado en Garant'#237'a. Tiene vigencia por Tiempo indefini' +
            'do, hasta que UCACSA. Emita una constancia de Liberaci'#243'n.')
        ParentFont = False
      end
      object Memo33: TfrxMemoView
        IndexTag = 1
        AllowVectorExport = True
        Left = 264.567100000000000000
        Top = 873.071430000000000000
        Width = 321.260050000000000000
        Height = 15.118120000000000000
        DataField = 'NOMBRE'
        DataSet = dssClientes
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Frame.Typ = []
        Memo.UTF8W = (
          '[frxDBDataset1."NOMBRE"]')
        ParentFont = False
      end
      object Line10: TfrxLineView
        AllowVectorExport = True
        Left = 272.126160000000000000
        Top = 861.732840000000000000
        Width = 227.726423560000000000
        Color = clBlack
        Frame.Typ = [ftTop]
      end
      object Memo34: TfrxMemoView
        AllowVectorExport = True
        Left = 230.551330000000000000
        Top = 873.071430000000000000
        Width = 34.015770000000000000
        Height = 16.377963330000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'SR.')
        ParentFont = False
      end
      object Memo35: TfrxMemoView
        AllowVectorExport = True
        Left = 332.598640000000000000
        Top = 888.189550000000000000
        Width = 90.708720000000000000
        Height = 16.377963330000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          'POSEEDOR')
        ParentFont = False
      end
    end
  end
  object dssClientes: TfrxDBDataset
    UserName = 'frxDBDataset1'
    CloseDataSource = False
    DataSource = dsClientes2
    BCDToCurrency = False
    Left = 272
    Top = 168
  end
  object qryTerreno: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select * from sic_predial_terrenos where folio=:folio and client' +
        'e_id=:cliente_id')
    Left = 96
    Top = 160
    ParamData = <
      item
        Name = 'FOLIO'
        DataType = ftString
        ParamType = ptInput
        Size = 20
        Value = Null
      end
      item
        Name = 'CLIENTE_ID'
        ParamType = ptInput
      end>
  end
  object dsTerreno: TDataSource
    DataSet = qryTerreno
    Left = 152
    Top = 168
  end
  object dssTerreno: TfrxDBDataset
    UserName = 'frxDBDataset1'
    CloseDataSource = False
    DataSource = dsTerreno
    BCDToCurrency = False
    Left = 216
    Top = 160
  end
end
